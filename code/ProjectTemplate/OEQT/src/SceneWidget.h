#pragma once
#include "BaseSceneWidget.h"
#include "osgEarth/MapNode"

class SceneWidget : public BaseSceneWidget
{
public:
	SceneWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
	osgEarth::Map* getMap();
	osgEarth::MapNode* getMapNode();
protected:
	osgEarth::Map* _map;
	osgEarth::MapNode* _mapNode;
};