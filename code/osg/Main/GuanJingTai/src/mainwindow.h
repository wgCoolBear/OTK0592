#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QtEvents>
#include <osg/Group>
#include <osgViewer/Viewer>

#define ACTION(func) void on_action_##func##_triggered(bool b);
#define FUNC(name) void MainWindow::on_action_##name##_triggered(bool b)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
	//退出,on_action_*_triggered语法中，"quit"和mainwindow.ui中的action对应，这样就不用显示的声明connect了
	void on_action_quit_triggered();
	//基本图形
	ACTION(BasicShape)
	//键鼠事件
	ACTION(KeyMouseEventHandler)
	//鼠标拾取
	ACTION(MousePick)
	//回调
	ACTION(NodeCallback)
	//StateSet
	ACTION(StateSet)
	//访问器
	ACTION(NodeVisitor)
	//顶点更新
	ACTION(UpdateGeometry)
	//RTT
	ACTION(RTT)
	//动画路径
	ACTION(AnimationPath)
	//骨骼动画fbx
	ACTION(FBX)
	//样条曲线
	ACTION(SplineCurve)
	//特效
	ACTION(Effect)
	//有向包围盒OBB
	ACTION(OBB)
	//提取模型边界
	ACTION(ExtractModelEdge)
	//延迟渲染
	ACTION(DeferredShading)
	//Shader
	ACTION(Shader)
	//多面体与模型求交
	ACTION(CutModel)
	//HUD
	ACTION(HUD)
	//FBOScreen
	ACTION(FBOScreen)
	//半透明的球
	ACTION(TransparentBall)
	//布尔运算使用CGAL
	ACTION(BooleanOperation1)
	//布尔运算使用osgModeling
	ACTION(BooleanOperation2)
	//投影纹理
	ACTION(ProjectiveTexture)
	//视域分析
	ACTION(Viewshed)
	//拖拽器
	ACTION(Dragger)
	//发光效果高斯模糊
	ACTION(GlowWithGaussianBlur)
	//ShaderToy
	ACTION(ShaderToy)
	//箭头
	ACTION(Arrow)
	//EDL点云阴影染色算法
	ACTION(EDL)
	//雷达特效
	ACTION(Radar)
	//简化模型
	ACTION(MeshSimplify)
protected:
	//virtual void keyPressEvent(QKeyEvent* event) override;
private:
    Ui::MainWindow* _ui;

	osg::ref_ptr<osgViewer::Viewer> _viewer;
	osg::ref_ptr<osg::Camera> _mainCamera;
	osg::ref_ptr<osg::Group> _root;
	osg::ref_ptr<osg::Group> _functions;
	//一些测试模型
	osg::ref_ptr<osg::Node> _cessna;
	osg::ref_ptr<osg::Node> _glider;
	osg::ref_ptr<osg::Node> _cow;
	osg::ref_ptr<osg::Node> _clock;
	osg::ref_ptr<osg::Node> _lz;
	osg::ref_ptr<osg::Node> _tank;
	osg::ref_ptr<osg::Node> _axes;
	osg::ref_ptr<osg::Node> _redflag;
};

#endif // MAINWINDOW_H
