#pragma once
#include<windows.h>
/*
作者：segen_jaa
来源：CSDN
原文：https ://blog.csdn.net/segen_jaa/article/details/28634877?utm_source=copy
*/
#include <string>
class JarAppCall
{
public:
	JarAppCall();
	~JarAppCall();
	/*
	@brief 执行Jar包调用
	@param[in] strJarDir jar包目录
	@param[in] strJarPath jar包绝对路径
  @return 返回句柄
	*/
  HANDLE Run(const std::string& strJarDir, const std::string& strJarPath);
private:
  HANDLE CallApp(const std::string& strJarDir, const std::string& strJarPath);
};

/*
//使用方式
#include <windows.h>
#include <shellapi.h>//ShellExecute函数所在头文件
#include <string>
#include <iostream>
#include "JarAppCall.h"

void main()
{
	//路径中要使用'/'，不用'\'
	std::string exePath = "E:/A B啊/Main Simple.exe";//路径可以包括空格和汉字
	std::string exeDir = "";//exe所在目录
	std::string exeName = "";
	int delay = 1000;//毫秒
	if (exePath.find('/') != std::string::npos)
	{
		exeDir = exePath.substr(0, exePath.rfind('/') + 1);
		exeName = exePath.substr(exePath.rfind('/') + 1);
	}
	//启动exe。据说要启动需要管理员权限的exe，要用到ShellExecuteEx函数。。。。。
	ShellExecute(NULL, "open", exePath.data(), NULL, exeDir.data(), SW_SHOWNORMAL);
	std::cout << "启动：" << exePath << std::endl;
	Sleep(delay);

	//终止
	std::string cmd = std::string("TASKKILL /F /IM \"") + exeName + "\"";//exe名字前后用引号引起来，因为有的exe名字有空格
	system(cmd.data());

	// 启动、终止一个jar
	JarAppCall jarCall;
	std::string jarPath = "E:\\ExecuteOtherExe\\LR(1).jar";
	std::string jarDir = "";//exe所在目录
	// 将路径中的'\'替换成'/'
	size_t pos = 0;
	while ((pos = jarPath.find('\\')) != std::string::npos)
	{
		jarPath.replace(pos, 1, "/");
	}
	if (jarPath.find('/') != std::string::npos)
	{
		jarDir = jarPath.substr(0, jarPath.rfind('/') + 1);
	}
	HANDLE h = jarCall.Run(jarDir, jarPath);
	std::cout << "启动：" << jarPath << std::endl;
	Sleep(delay);
	//终止
	TerminateProcess(h, 0);
	std::cout << "终止：" << jarPath << std::endl;

	system("pause");
}
*/