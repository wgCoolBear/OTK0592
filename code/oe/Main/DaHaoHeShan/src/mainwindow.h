#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <osgEarth/MapNode>
#include <osgViewer/Viewer>
#include <osgEarthUtil/EarthManipulator>

#define ACTION(func) void on_action_##func##_triggered(bool b);
#define FUNC(name) void MainWindow::on_action_##name##_triggered(bool b)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
	void init();
	//打开项目,on_action_*_triggered语法中，"open"和mainwindow.ui中的action对应，这样就不用显示的声明connect了
	bool on_action_open_triggered();
	//退出
	void on_action_quit_triggered();	
	//京沪飞行
	void on_pushButton_jingHuFly_clicked();
	//太湖
	void on_pushButton_taiHu_clicked();
private:
    Ui::MainWindow* _ui;

	osg::observer_ptr<osgViewer::Viewer> _viewer;
	osg::observer_ptr<osg::Group> _root;
	osg::ref_ptr<osgEarth::Util::EarthManipulator> _earthManipulator;
	//一些osg的东西可以加在这里
	osg::ref_ptr<osg::Group> _functions;
	osg::observer_ptr<osgEarth::Map> _map;
	//添加在地球上的例如Annotation需要添加到mapNode，否则看不见
	osg::observer_ptr<osgEarth::MapNode> _mapNode;
	//一些测试模型
	osg::ref_ptr<osg::Node> _cessna;
	osg::ref_ptr<osg::Node> _glider;
	osg::ref_ptr<osg::Node> _cow;
	osg::ref_ptr<osg::Node> _satellite;
};

#endif // MAINWINDOW_H
