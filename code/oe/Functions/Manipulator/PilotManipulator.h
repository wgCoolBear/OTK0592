#pragma once
#include "Export.h"
#include <osgGA/FirstPersonManipulator>
#include <osgEarth/SpatialReference>
#include <osg/MatrixTransform>
//飞行员视角操纵器
class DLL_EXPORT PilotManipulator:public osgGA::FirstPersonManipulator
{
	// 指示操纵器的上方，右方，前方
	class PilotCoordinateFrameCallback : public osgGA::CameraManipulator::CoordinateFrameCallback
	{
	public:
		PilotCoordinateFrameCallback();
		  // 此函数被FirstPersonManipulator中的performMovementLeftMouseButton函数调用
		  // 传入参数是eye，计算eye所在的位姿矩阵
		  virtual osg::CoordinateFrame getCoordinateFrame(const osg::Vec3d& position) const;
	protected:
		virtual ~PilotCoordinateFrameCallback();
	};

	//根据飞机节点更新操纵器矩阵
	class UpdateMatrixCallback : public osg::NodeCallback
	{
	public:
		UpdateMatrixCallback(PilotManipulator* pm);
		// node节点是当前回调所挂载的节点
		virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);
	private:
		PilotManipulator* _pm;
	};
public:
	//视角旋转模式
	enum ROTATE_MODE
	{
		AUTO,//自动，完全跟随飞机模型
		HAND//手动控制旋转
	};
public:
	PilotManipulator(osg::ref_ptr<osg::MatrixTransform> planeMT, ROTATE_MODE mode=AUTO);

	//通过经纬度方位角设置位置和姿态(单位是角度)
	void setLLAHPR(float lon, float lat, float alt, float heading, float pitch, float roll);

	//通过矩阵设置操纵器
	void setMatrix(osg::Matrix matrix);

	//设置操纵器的上方，右方，前方
	void setPilotCoordinateFrameCallback();

	osg::ref_ptr<osg::MatrixTransform> _planeMT;

	ROTATE_MODE _mode;
};