#pragma once
#include <QLabel>
#include <QMovie>
/*
播放gif动画图片
支持APNG格式，不过需要手动编译qapng.dll插件，详细情况见OTK\code\qt\plugins\APNG
使用方式
auto gif = new GIFPlayer(":/a.gif");
auto apnggif = new GIFPlayer(":/apng.png");
gif->play();
*/
class GIFPlayer :public QLabel
{
	Q_OBJECT
public:
	GIFPlayer(QWidget* parent=NULL);
	GIFPlayer(QString gifPath, QWidget* parent, bool play = false);
	GIFPlayer(QString gifPath, QWidget* parent, QSize targetSize, bool play = false);

	void init(QString gifPath, bool play);
	void play();
	void stop();

	void setPos(int x, int y);
	void setGeom(QRect rect);
	void setGeom(int x, int y, int width, int height);
public slots:
void onFinished();
private:
	QMovie* _movie;
	int _width;
	int _height;
};

