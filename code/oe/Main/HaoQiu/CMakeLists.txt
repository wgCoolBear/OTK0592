cmake_minimum_required(VERSION 3.0)

SET(PRODUCT_NAME HaoQiu)

SET(CPP_HDRS
    #src/EarthWidget.h
)

# Header files that need moc'd
SET(MOC_HDRS
    src/mainwindow.h
	src/SceneWidget.h
)

# Qt UI files
SET(UI_FILES
    ui/mainwindow.ui
)

#Windows的资源文件
set(RC_FILES
    #my.rc
)

set(RES_FILES
    ui/main.qrc
	#resource/icons/action_icons.qrc
)

QT5_ADD_RESOURCES( RES_SRCS ${RES_FILES} )

QT5_WRAP_UI( UI_HDRS ${UI_FILES} )

QT5_WRAP_CPP(MOC_SRCS ${MOC_HDRS})

#文件夹筛选器文件分类
SOURCE_GROUP("auto generated files" FILES ${MOC_SRCS} ${UI_HDRS} ${RES_SRCS})
SOURCE_GROUP("ui" FILES ${UI_FILES} ${RES_FILES})

SET(PRODUCT_FILES
	${RC_FILES}
	${RES_SRCS}
    ${MOC_SRCS}
    ${UI_HDRS}
    src/main.cpp
    src/mainwindow.cpp
    src/SceneWidget.cpp
	#${CMAKE_SOURCE_DIR}/GraphicsWindowQt.cpp
	#${CMAKE_SOURCE_DIR}/GraphicsWindowQt.h
    ${CPP_HDRS}
	${MOC_HDRS}
)

SET(PRODUCT_DATA_FILES
	empty
    #data/axes.osgt
    #data/arial.ttf
)

# 使用Qt中的QOpenGLWidget还是使用QGLWidget
IF(USE_QOpenGLWidget)
	ADD_DEFINITIONS(-DUSE_QOpenGLWidget)
ENDIF()

# 将当前项目路径添加到include，否则fatal error C1083: 无法打开包括文件: “ui_mainwindow.h”
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})
#子功能目录
INCLUDE_DIRECTORIES(../../Functions)

#是否显示控制台
SET(WITH_CONSOLE 1)
IF(${WITH_CONSOLE})
    ADD_EXECUTABLE(${PRODUCT_NAME} ${PRODUCT_FILES})
ELSE(${WITH_CONSOLE})
    ADD_EXECUTABLE(${PRODUCT_NAME} WIN32 ${PRODUCT_FILES})
ENDIF(${WITH_CONSOLE})

#需要的Qt库
qt5_use_modules(${PRODUCT_NAME} Gui Core OpenGL)

#osgQt库
IF(USE_QOpenGLWidget)
	TARGET_LINK_LIBRARIES(${PRODUCT_NAME} osgQOpenGL)
ELSE()
	TARGET_LINK_LIBRARIES(${PRODUCT_NAME} osgQGL)
ENDIF()
#功能模块
TARGET_LINK_LIBRARIES(${PRODUCT_NAME} 
	BasicAnnotation
	#FastCutShp
	Manipulator
	#CircleArea
	#DataLinkLine
	#Wave
	OEShader
	#OEMousePick
	#Hole
	#ObliquePhotography
	#VerticalScale
	#Contour
	#DyeEarthInPolygonArea
	#EarthHeatMap
	#Analysis
	#FlightPath
	#FlyPath
	#StarSky
	#AnimationPathOnEarth
)
#以下模块需要C++11支持
IF(COMPILER_SUPPORTS_CXX11)
	TARGET_LINK_LIBRARIES(${PRODUCT_NAME} 
		#TLE-Satellite
		#SolarSystem
	)
ENDIF()


#设置链接库、输出目录、安装目录等
SETUP_LINK_OUTPUT_INSTALL(${PRODUCT_NAME} ${SOLUTION_FOLDER_NAME} ${PRODUCT_DATA_FILES})