#include "Export.h"
#include <osg/NodeVisitor>

//使用访问器获取节点中几何信息
class DLL_EXPORT GetGeometryDataVisitor : public osg::NodeVisitor
{
public:
	GetGeometryDataVisitor();
	virtual void apply(osg::Geode& geode);
private:
	//顶点数组
	std::vector<osg::Vec3> vertex;
};