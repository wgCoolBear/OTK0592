
SET(3rd_DIR "" CACHE PATH "Set to 3rd party path same as osg and osgearth 3rd party path")

FIND_PATH(3rd_BIN_DIR zlib.dll
    PATHS
	${3rd_DIR}/bin
    /usr/bin
    /usr/local/bin
)
SET( 3rd_FOUND "NO" )
IF( 3rd_BIN_DIR )
    SET( 3rd_FOUND "YES" )
ENDIF( 3rd_BIN_DIR )