#pragma once
#include "Export.h"
#include <osg/MatrixTransform>
#include "osg/NodeVisitor"
//控制节点在屏幕上的大小固定。大小通过size调整，它不是像素大小也不是空间大小。。。
class DLL_EXPORT AutoScaleGroup : public osg::Group
{
public:
	AutoScaleGroup(osg::MatrixTransform* mt, float size=240.0f);

	void setSize(float size);

	void traverse(osg::NodeVisitor& nv);

protected:
	osg::MatrixTransform* _mt;
	float _size;
	bool _active;
};


