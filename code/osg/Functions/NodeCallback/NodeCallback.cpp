#include "NodeCallback.h"
#include <osg/MatrixTransform>

RotCallback::RotCallback()
{
	startTime = 0;
}

void RotCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
{
	double t = nv->getFrameStamp()->getReferenceTime();
	osg::notify(osg::ALWAYS) <<"ReferenceTime"<< t<<std::endl;
	osg::ref_ptr<osg::MatrixTransform> mt = dynamic_cast<osg::MatrixTransform*>(node);
	if (mt)
	{
		//经过1秒旋转0.1度
		if (t - startTime > 1)
		{
			mt->postMult( osg::Matrix::rotate(0.1, osg::Z_AXIS) );
			startTime = t;
		}
	}
	traverse(node, nv);
}

ReadFrameBufferCameraDrawCallback::ReadFrameBufferCameraDrawCallback(osg::Image* img) :_img(img)
{

}

void ReadFrameBufferCameraDrawCallback::operator()(const osg::Camera& camera) const
{
	auto viewport = camera.getViewport();
	//直接从帧缓存读取数据，这里测试只读取部分
	_img->readPixels(viewport->x()/4, viewport->y()/4, viewport->width()/2, viewport->height()/2, GL_RGBA, GL_UNSIGNED_BYTE);
	//osgDB::writeImageFile(*_img, "rtt.png");
}
