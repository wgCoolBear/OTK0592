#pragma once
#include "Export.h"
#include <osgEarth/MapNode>
class DLL_EXPORT Shader
{
public:
	//给地球添加一层迷雾，根据视点高程逐渐稀释浓度
	static void AddShaderOnEarth(osgEarth::MapNode* mapNode);
	static void RemoveShader(osgEarth::MapNode* mapNode);
};

