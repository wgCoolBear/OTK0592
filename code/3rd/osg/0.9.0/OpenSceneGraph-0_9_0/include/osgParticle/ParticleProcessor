//C++ header - Open Scene Graph - Copyright (C) 1998-2002 Robert Osfield
//Distributed under the terms of the GNU Library General Public License (LGPL)
//as published by the Free Software Foundation.
//osgParticle - Copyright (C) 2002 Marco Jez

#ifndef OSGPARTICLE_PARTICLEPROCESSOR_
#define OSGPARTICLE_PARTICLEPROCESSOR_ 1

#include <osgParticle/Export>
#include <osgParticle/ParticleSystem>

#include <osg/ref_ptr>
#include <osg/Object>
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osg/CopyOp>
#include <osg/Vec3>
#include <osg/Matrix>

namespace osgParticle
{

    /**    A common base interface for those classes which need to do something on particles. Such classes
        are, for example, <CODE>Emitter</CODE> (particle generation) and <CODE>Program</CODE> (particle animation).
        This class holds some properties, like a <I>reference frame</I> and a reference to a <CODE>ParticleSystem<CODE>;
        descendant classes should process the particles taking into account the reference frame, computing the right
        transformations when needed.
    */
    class OSGPARTICLE_EXPORT ParticleProcessor: public osg::Node {
    public:
    
        enum ReferenceFrame {
            RELATIVE_TO_PARENTS,
            RELATIVE_TO_ABSOLUTE
        };
        
        ParticleProcessor();
        ParticleProcessor(const ParticleProcessor &copy, const osg::CopyOp &copyop = osg::CopyOp::SHALLOW_COPY);

        virtual const char *className() const { return "ParticleProcessor"; }
        virtual bool isSameKindAs(const osg::Object *obj) const { return dynamic_cast<const ParticleProcessor*>(obj) != 0; }
        virtual void accept(osg::NodeVisitor& nv) { if (nv.validNodeMask(*this)) { nv.pushOntoNodePath(this); nv.apply(*this); nv.popFromNodePath(); } }        
        
        /// Get the reference frame.
        inline ReferenceFrame getReferenceFrame() const;
        
        /// Set the reference frame.
        inline void setReferenceFrame(ReferenceFrame rf);
        
        /// Get whether this processor is enabled or not.
        inline bool isEnabled() const;
        
        /// Set whether this processor is enabled or not.
        inline void setEnabled(bool v);
        
        /// Get a pointer to the destination particle system.
        inline ParticleSystem *getParticleSystem();
        
        /// Get a const pointer to the destination particle system.
        inline const ParticleSystem *getParticleSystem() const;
        
        /// Set the destination particle system.
        inline void setParticleSystem(ParticleSystem *ps);
        
        void traverse(osg::NodeVisitor &nv);

        /// Get the current local-to-world transformation matrix (valid only during cull traversal).
        inline const osg::Matrix &getLocalToWorldMatrix();
        
        /// Get the current world-to-local transformation matrix (valid only during cull traversal).
        inline const osg::Matrix &getWorldToLocalMatrix();
        
        /// Transform a point from local to world coordinates (valid only during cull traversal).
        inline osg::Vec3 transformLocalToWorld(const osg::Vec3 &P);
        
        /// Transform a vector from local to world coordinates, discarding translation (valid only during cull traversal).
        inline osg::Vec3 rotateLocalToWorld(const osg::Vec3 &P);
        
        /// Transform a point from world to local coordinates (valid only during cull traversal).
        inline osg::Vec3 transformWorldToLocal(const osg::Vec3 &P);
        
        /// Transform a vector from world to local coordinates, discarding translation (valid only during cull traversal).
        inline osg::Vec3 rotateWorldToLocal(const osg::Vec3 &P);

    protected:
        virtual ~ParticleProcessor() {}
        ParticleProcessor &operator=(const ParticleProcessor &) { return *this; }
        
        inline const bool computeBound() const;    
        
        virtual void process(double dt) = 0;
        
    private:
        ReferenceFrame rf_;
        bool enabled_;
        double t0_;
        osg::ref_ptr<ParticleSystem> ps_;
        bool need_ltw_matrix_;
        bool need_wtl_matrix_;
        osg::Matrix ltw_matrix_;
        osg::Matrix wtl_matrix_;
        osg::NodeVisitor *current_nodevisitor_;
    };
    
    // INLINE FUNCTIONS
    
    inline ParticleProcessor::ReferenceFrame ParticleProcessor::getReferenceFrame() const
    {
        return rf_;
    }
    
    inline void ParticleProcessor::setReferenceFrame(ReferenceFrame rf)
    {
        rf_ = rf;
    }
    
    inline bool ParticleProcessor::isEnabled() const
    {
        return enabled_;
    }
    
    inline void ParticleProcessor::setEnabled(bool v)
    {
        enabled_ = v;
        if (enabled_) t0_ = -1;
    }
    
    inline ParticleSystem *ParticleProcessor::getParticleSystem()
    {
        return ps_.get();
    }

    inline const ParticleSystem *ParticleProcessor::getParticleSystem() const
    {
        return ps_.get();
    }
    
    inline void ParticleProcessor::setParticleSystem(ParticleSystem *ps)
    {
        ps_ = ps;
    }

    inline const bool ParticleProcessor::computeBound() const
    {
        _bsphere.init();
        _bsphere_computed = true;
        return true;
    }
    
    inline const osg::Matrix &ParticleProcessor::getLocalToWorldMatrix()    
    {
        if (need_ltw_matrix_) {
            ltw_matrix_ = osg::Matrix::identity();
            current_nodevisitor_->getLocalToWorldMatrix(ltw_matrix_, this);
            need_ltw_matrix_ = false;
        }
        return ltw_matrix_;
    }

    inline const osg::Matrix &ParticleProcessor::getWorldToLocalMatrix()    
    {
        if (need_wtl_matrix_) {
            wtl_matrix_ = osg::Matrix::identity();
            current_nodevisitor_->getWorldToLocalMatrix(wtl_matrix_, this);
            need_wtl_matrix_ = false;
        }
        return wtl_matrix_;
    }
    
    inline osg::Vec3 ParticleProcessor::transformLocalToWorld(const osg::Vec3 &P)
    {
        return getLocalToWorldMatrix().preMult(P);
    }
    
    inline osg::Vec3 ParticleProcessor::transformWorldToLocal(const osg::Vec3 &P)
    {
        return getWorldToLocalMatrix().preMult(P);
    }

    inline osg::Vec3 ParticleProcessor::rotateLocalToWorld(const osg::Vec3 &P)
    {
        return getLocalToWorldMatrix().preMult(P) - 
            getLocalToWorldMatrix().preMult(osg::Vec3(0, 0, 0));
    }
    
    inline osg::Vec3 ParticleProcessor::rotateWorldToLocal(const osg::Vec3 &P)
    {
        return getWorldToLocalMatrix().preMult(P) -
            getWorldToLocalMatrix().preMult(osg::Vec3(0, 0, 0));
    }

}


#endif
