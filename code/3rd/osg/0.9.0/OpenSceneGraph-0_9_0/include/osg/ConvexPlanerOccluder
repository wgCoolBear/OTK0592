//C++ header - Open Scene Graph - Copyright (C) 1998-2002 Robert Osfield
//Distributed under the terms of the GNU Library General Public License (LGPL)
//as published by the Free Software Foundation.

#ifndef OSG_CONVEXPLANEROCCLUDER
#define OSG_CONVEXPLANEROCCLUDER 1

#include <osg/ConvexPlanerPolygon>
#include <osg/Object>

namespace osg {

class OccluderVolume;

/** A class for representing convex clipping volumes made up.
  * When adding planes, their normals should point inwards (into the volume) */
class SG_EXPORT ConvexPlanerOccluder : public Object
{

    public:

        ConvexPlanerOccluder():Object() {}
        ConvexPlanerOccluder(const ConvexPlanerOccluder& cpo,const CopyOp& copyop=CopyOp::SHALLOW_COPY):
            Object(cpo,copyop),
            _occluder(cpo._occluder),
            _holeList(cpo._holeList) {}
        
        META_Object(osg,ConvexPlanerOccluder)

        void setOccluder(const ConvexPlanerPolygon& cpp) { _occluder = cpp; }
        
        ConvexPlanerPolygon& getOccluder() { return _occluder; }

        const ConvexPlanerPolygon& getOccluder() const { return _occluder; }
        
        

        typedef std::vector<ConvexPlanerPolygon> HoleList;

        void addHole(const ConvexPlanerPolygon& cpp) { _holeList.push_back(cpp); }

        HoleList& getHoleList() { return _holeList; }
        
        const HoleList& getHoleList() const { return _holeList; }

        void computeAttributes();
        
        void computeBound(BoundingBox& bb) const;

        void computeBound(BoundingSphere& bs) const;

    protected:

        ~ConvexPlanerOccluder() {}

        ConvexPlanerPolygon _occluder;
        HoleList _holeList;

};

}	// end of namespace

#endif 
