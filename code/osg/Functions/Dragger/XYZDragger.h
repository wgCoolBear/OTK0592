#pragma once

#include <osgManipulator/Translate1DDragger>
#include <osgManipulator/Projector>
#include <osg/LineWidth>

using namespace osgManipulator;

//////////////////////////////////////////////////////////////////////////
/// 原生OSG拖拽器存在问题，需要重写handle(ea,aa)和handle(pi,ea,aa)
/// 主要问题是其在判断拖拽器是否被鼠标拾取时，只使用了第一个intersection
/// 其他的intersection没有判断
//////////////////////////////////////////////////////////////////////////
class  OneDDragger : public Translate1DDragger
{
    public:

		OneDDragger();

		OneDDragger(const osg::Vec3d& s, const osg::Vec3d& e);

		//virtual void traverse(osg::NodeVisitor& nv);

		virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

        /** Handle pick events on dragger and generate TranslateInLine commands. */
        virtual bool handle(const PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);

        /** Setup default geometry for dragger. */
        void setupDefaultGeometry();

    protected:

        virtual ~OneDDragger();
};

class XYZDragger : public CompositeDragger
{
public:

	XYZDragger();

	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

	/** Handle pick events on dragger and generate TranslateInLine commands. */
	virtual bool handle(const PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);

	/** Setup default geometry for dragger. */
	void setupDefaultGeometry();

	/** Sets the width of the axis lines in pixels. */
	void setAxisLineWidth(float linePixelWidth);

	/** Retrieves the width of the axis lines in pixels. */
	float getAxisLineWidth() const { return _axisLineWidth; }

	/** Sets the radius of the cylinders representing the axis lines for picking. */
	void setPickCylinderRadius(float pickCylinderRadius);

	/** Retrieves the radius of the cylinders representing the axis lines for picking. */
	float getPickCylinderRadius() const { return _pickCylinderRadius; }

	/** Sets the height of the cones. */
	void setConeHeight(float radius);

	/** Retrieves the height of the cones. */
	float getConeHeight() const { return _coneHeight; }

protected:

	virtual ~XYZDragger();

	osg::ref_ptr< OneDDragger >  _xDragger;
	osg::ref_ptr< OneDDragger >  _yDragger;
	osg::ref_ptr< OneDDragger >  _zDragger;

	float _coneHeight;
	float _axisLineWidth;
	float _pickCylinderRadius;

	//osg::ref_ptr<osg::Geode> _lineGeode;
	osg::ref_ptr<osg::Cylinder> _lineCylinder;
	osg::ref_ptr<osg::Cylinder> _cylinder;
	osg::ref_ptr<osg::LineWidth> _lineWidth;
	osg::ref_ptr<osg::Cone> _cone;
};
