#include "BasicAnnotation.h"
#include <osgEarthAnnotation/ImageOverlay>
#include <osgEarthAnnotation/CircleNode>
#include <osgEarthAnnotation/RectangleNode>
#include <osgEarthAnnotation/EllipseNode>
#include <osgEarthAnnotation/PlaceNode>
#include <osgEarthAnnotation/LabelNode>
#include <osgEarthAnnotation/LocalGeometryNode>
#include <osgEarthAnnotation/FeatureNode>
#include <osgEarthAnnotation/ModelNode>
#include "3rd/ToolKitFunctionClass/ToolKitFunctionClass.h"

using namespace osgEarth;
using namespace osgEarth::Annotation;
using namespace osgEarth::Features;

#define SRS osgEarth::SpatialReference::get("wgs84")

BasicAnnotation::BasicAnnotation(osgEarth::MapNode* mapNode)
{
	//图文标签PlaceNode
	{
		Style style;
		style.getOrCreate<IconSymbol>()->url()->setLiteral( "data/image/placemark.png" );
		//一定要指定一种中文字体，否则即使w2s成功了也无法显示中文
		style.getOrCreate<osgEarth::Symbology::TextSymbol>()->font() = "simsun.ttc";
		//指定编码
		style.getOrCreate<osgEarth::Symbology::TextSymbol>()->encoding() = osgEarth::Symbology::TextSymbol::ENCODING_UTF8;
		style.getOrCreate<osgEarth::Symbology::TextSymbol>()->size() = 30;
		style.getOrCreate<osgEarth::Symbology::TextSymbol>()->alignment() = osgEarth::Symbology::TextSymbol::ALIGN_LEFT_CENTER;
		style.getOrCreate<osgEarth::Symbology::TextSymbol>()->fill()->color() = osgEarth::Symbology::Color::White;
		addChild( new PlaceNode(GeoPoint(SRS, 116.408, 39.904), ToolKitFunctionClass::s2utf8("北京Beijing"), style));
		// test with an LOD:
		osg::LOD* lod = new osg::LOD();
		lod->addChild( new PlaceNode(GeoPoint(SRS, 121.445, 31.213), ToolKitFunctionClass::s2utf8("LOD上海Shanghai"), style), 0.0, 2e6);
		addChild( lod );
		// absolute altitude:
		addChild( new PlaceNode(GeoPoint(SRS, 120.343, 36.088, 1000, ALTMODE_ABSOLUTE), ToolKitFunctionClass::s2utf8("青岛Qingdao"), style));
	}
	//闪烁的线
	{
		struct C : public osg::NodeCallback {
			void operator()(osg::Node* n, osg::NodeVisitor* nv) {
				static int i=0;
				i++;
				if (i % 100 < 50)
					traverse(n, nv);
			}
		};
		Geometry* geom = new Polygon();
		geom->push_back( osg::Vec3d(116.408, 39.904, 0) );
		geom->push_back( osg::Vec3d(121.445, 31.213, 0) );

		Feature* feature = new Feature(geom, SRS);
		feature->geoInterp() = GEOINTERP_RHUMB_LINE;
		Style geomStyle;
		geomStyle.getOrCreate<LineSymbol>()->stroke()->color() = Color::Cyan;
		geomStyle.getOrCreate<LineSymbol>()->stroke()->width() = 5.0f;
		geomStyle.getOrCreate<LineSymbol>()->tessellationSize() = 75000;
		geomStyle.getOrCreate<AltitudeSymbol>()->clamping() = AltitudeSymbol::CLAMP_TO_TERRAIN;
		geomStyle.getOrCreate<AltitudeSymbol>()->technique() = AltitudeSymbol::TECHNIQUE_DRAPE;

		FeatureNode* fnode = new FeatureNode(feature, geomStyle);
		fnode->addCullCallback(new C());
		addChild( fnode );
		Style style;
		style.getOrCreate<IconSymbol>()->url()->setLiteral( "data/image/placemark.png" );
		style.getOrCreate<osgEarth::Symbology::TextSymbol>()->font() = "simsun.ttc";
		style.getOrCreate<osgEarth::Symbology::TextSymbol>()->encoding() = osgEarth::Symbology::TextSymbol::ENCODING_UTF8;
		LabelNode* label = new LabelNode(GeoPoint(SRS, 118, 36),ToolKitFunctionClass::s2utf8("京沪线"),style);
		addChild(label);
	}
	//圆圈
	{
		Style circleStyle;
		circleStyle.getOrCreate<PolygonSymbol>()->fill()->color() = Color(Color::Cyan, 0.5);
		circleStyle.getOrCreate<AltitudeSymbol>()->clamping() = AltitudeSymbol::CLAMP_TO_TERRAIN;
		circleStyle.getOrCreate<AltitudeSymbol>()->technique() = AltitudeSymbol::TECHNIQUE_DRAPE;

		CircleNode* circle = new CircleNode();
		circle->set(
			GeoPoint(SRS, 118.336, 32.057, 1000., ALTMODE_RELATIVE),
			Distance(300, Units::KILOMETERS),
			circleStyle, 
			Angle(-45.0, Units::DEGREES),
			Angle(45.0, Units::DEGREES),
			true);
		addChild( circle );
	}
	//拔高椭圆
	{
		Style ellipseStyle;
		ellipseStyle.getOrCreate<PolygonSymbol>()->fill()->color() = Color(Color::Red, 0.9);
		ellipseStyle.getOrCreate<ExtrusionSymbol>()->height() = 200000.0; // meters MSL
		EllipseNode* ellipse = new EllipseNode();
		ellipse->set(
			GeoPoint(SRS, 110.009, 30.663, 0.0, ALTMODE_RELATIVE),
			Distance(250, Units::MILES),
			Distance(200, Units::MILES),
			Angle   (0, Units::DEGREES),
			ellipseStyle,
			Angle(45.0, Units::DEGREES),
			Angle(360.0 - 45.0, Units::DEGREES), 
			true);
		addChild( ellipse );
	}
	//矩形
	{
		// A rectangle around San Diego
		Style rectStyle;
		rectStyle.getOrCreate<PolygonSymbol>()->fill()->color() = Color(Color::Gray, 0.9);
		rectStyle.getOrCreate<AltitudeSymbol>()->clamping() = AltitudeSymbol::CLAMP_TO_TERRAIN;
		rectStyle.getOrCreate<AltitudeSymbol>()->technique() = AltitudeSymbol::TECHNIQUE_DRAPE;
		RectangleNode* rect = new RectangleNode(
			GeoPoint(SRS, 117.246, 29.117),
			Distance(300, Units::KILOMETERS ),
			Distance(300, Units::KILOMETERS ),
			rectStyle);
		addChild( rect );
	}
	//多边形
	{
		Geometry* poly = new Polygon();
		poly->push_back( 114.052, 37.0   );
		poly->push_back( 109.054, 37.0   );
		poly->push_back( 109.054, 41.0   );
		poly->push_back( 111.040, 41.0   );
		poly->push_back( 111.080, 42.059 );
		poly->push_back( 114.080, 42.024 );

		Style style;
		style.getOrCreate<ExtrusionSymbol>()->height() = 250000.0; // meters MSL
		style.getOrCreate<PolygonSymbol>()->fill()->color() = Color(Color::White, 0.8);

		Feature*     feature = new Feature(poly, SRS);
		FeatureNode* featureNode = new FeatureNode(feature, style);

		addChild( featureNode );
	}
	// an image overlay.
	{
		ImageOverlay* imageOverlay = 0L;
		osg::ref_ptr<osg::Image> image = osgDB::readRefImageFile( "data/image/miky.png" );
		if (image.valid())
		{
			imageOverlay = new ImageOverlay(mapNode, image.get());
			imageOverlay->setBounds( Bounds( 120, 30.0, 121, 31.0) );
			addChild( imageOverlay );
		}
	}
	//模型
	{
		Style style;
		style.getOrCreate<ModelSymbol>()->autoScale() = true;
		//style.getOrCreate<ModelSymbol>()->setModel(modelNode);
		style.getOrCreate<ModelSymbol>()->url()->setLiteral("data/model/redflag.osgb.50.scale");
		osgEarth::Annotation::ModelNode* modelNode = new osgEarth::Annotation::ModelNode(mapNode, style); 
		modelNode->setPosition(GeoPoint(SRS, 120.53, 37.82));
		addChild(modelNode);
	}
}


BasicAnnotation::~BasicAnnotation()
{
}
