#include <osgDB/Version>

const char* osgDBGetVersion()
{
    return "0.9.0";
}


const char* osgDBGetLibraryName()
{
    return "Open Scene Graph DB (data base) Library";
}
