#include "MoveableWidget.h"

MoveableWidget::MoveableWidget(QWidget* parent /*= NULL*/)
	:_dragFlag(false)
{
	_parentWidget = parent;
}

void MoveableWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button()==Qt::LeftButton)
	{
		_dragFlag = true;
		_dragStartPos = event->globalPos() - _parentWidget->pos();
		event->accept();
	}
}

void MoveableWidget::mouseReleaseEvent(QMouseEvent *event)
{
	_dragFlag = false;
}

void MoveableWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (_dragFlag&&(event->buttons()&&Qt::LeftButton))
	{
		//QWidget* parent = this->parentWidget();
		_parentWidget->move(event->globalPos() - _dragStartPos);
		event->accept();
	}
}
