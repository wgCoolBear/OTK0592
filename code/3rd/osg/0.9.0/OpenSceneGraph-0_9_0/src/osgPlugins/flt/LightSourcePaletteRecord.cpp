// LightSourcePaletteRecord.cpp

#include "flt.h"
#include "Registry.h"
#include "LightSourcePaletteRecord.h"

using namespace flt;

////////////////////////////////////////////////////////////////////
//
//                       LightSourcePaletteRecord
//
////////////////////////////////////////////////////////////////////

RegisterRecordProxy<LightSourcePaletteRecord> g_LightSourcePaletteProxy;

LightSourcePaletteRecord::LightSourcePaletteRecord()
{
}


// virtual
LightSourcePaletteRecord::~LightSourcePaletteRecord()
{
}


// virtual
void LightSourcePaletteRecord::endian()
{
}
