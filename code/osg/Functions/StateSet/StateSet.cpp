#include "StateSet.h"
#include <osg/PolygonMode>
#include <osg/LineWidth>
#include <osg/Point>
#include <osg/PolygonOffset>
#include <osg/Texture2D>
#include <osg/TextureRectangle>
#include "osgDB/ReadFile"

StateSet::StateSet(osg::Node* model)
{
	addChild(model);
	auto ss = this->getOrCreateStateSet();

	//关闭相机的细节裁剪。有些时候例如只绘制一个点的时候会看不见，是因为细节裁剪给裁剪掉了。switch off small feature culling as this can cull out geometry that will still be large enough once perspective correction takes effect.
	//camera->setCullingMode(camera->getCullingMode() & ~osg::CullSettings::SMALL_FEATURE_CULLING);

	//光照
	//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::OVERRIDE);
	
	//背面剔除
	//ss->setAttributeAndModes(new osg::CullFace(osg::CullFace::BACK));
	//ss->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);

	//线框模式
	ss->setAttributeAndModes(new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE), osg::StateAttribute::ON);
	
	//PolygonOffset可用于消除重叠三件面片引起的Z-Fighting现象。
	//一个大于0的offset会把模型推到离你（摄像机）更远一点的位置，相应地一个小于0的offset会把模型拉近
	ss->setAttributeAndModes(new osg::PolygonOffset(-1, -1));

	//线宽
	osg::LineWidth* lw = new osg::LineWidth(3);
	ss->setAttributeAndModes(lw, osg::StateAttribute::ON);
	
	//点大小
	osg::Point* p = new osg::Point(5);
	ss->setAttributeAndModes(p);
	
	//透明
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	// set up the texture state.
	/*osg::Texture2D* texture = new osg::Texture2D;
	texture->setDataVariance(osg::Object::DYNAMIC); // protect from being optimized away as static state.
	texture->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
	texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
	texture->setWrap(osg::Texture2D::WRAP_S, osg::Texture2D::REPEAT);
	texture->setWrap(osg::Texture2D::WRAP_T, osg::Texture2D::REPEAT);
	texture->setImage(osgDB::readImageFile("Images/road.png"));
	osg::StateSet* stateset = pyramidGeometry->getOrCreateStateSet();
	stateset->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	stateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//stateset->setMode(GL_BLEND, osg::StateAttribute::ON);
	//stateset->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	*/

	/************************************************************************/
	/* 纹理对象osg::Texture2D更新
	/* 直接替换一张图片好弄，FBO更新也好弄，但是当更新的数据是一个char*数组，就不好弄了
	/************************************************************************/
	osg::Texture2D* tex = dynamic_cast<osg::Texture2D*>(ss->getTextureAttribute(0, osg::StateAttribute::TEXTURE));
	if (tex)
	{
		osg::Image* img = tex->getImage(0);
		if (img==NULL)
		{
			img = new osg::Image();
			img->setDataVariance(osg::Object::DYNAMIC);
			tex->setImage(img);
		}
		//新数据是一个图片文件
		osg::Image* newImg = osgDB::readImageFile("C:/new.bmp");
		img->setImage(1024, 768, 1,newImg->getInternalTextureFormat(),newImg->getPixelFormat(),newImg->getDataType(), newImg->data(), osg::Image::USE_MALLOC_FREE);
		img->dirty();
		//新数据是数据流传过来的一个char数组,data
		float* data = new float[1024*867];
		img->setImage(1024, 768, 1, GL_RED, GL_RED, GL_FLOAT, (unsigned char*)data, osg::Image::USE_NEW_DELETE);
		//image->setPixelFormat(GL_RGBA);  //设置像素的格式(内存中图像数据的物理布局)
		//image->setInternalTextureFormat(GL_RGBA32F_ARB);
		//image->setDataType(GL_FLOAT);
		
		/*
		float* data = new float[128 * 128];
		for (int i = 0; i < 128 * 128; i ++)
		{
			data[i] = cos(i*0.001);
		}
		img->setImage(128, 128, 1, GL_RED, GL_RED, GL_FLOAT,//GL_LUMINANCE, GL_LUMINANCE
			(unsigned char*)data, osg::Image::USE_NEW_DELETE);
		*/
		/*
		float* data = new float[128 * 128];
		for (int i = 0; i < 128 * 128; i++)
		{
			data[i] = cos(i*0.001);
		}
		img->setImage(128, 128, 1, GL_LUMINANCE, GL_LUMINANCE, GL_FLOAT,
			(unsigned char*)data, osg::Image::USE_NEW_DELETE);
		*/
		/*
		float* data = new float[128 * 128*2];
		for (int i = 0; i < 128 * 128; i++)
		{
			data[i*2] = cos(i*0.001);
			data[i * 2+1] = 1.0;
		}
		img->setImage(128, 128, 1, GL_LUMINANCE_ALPHA32F_ARB, GL_LUMINANCE_ALPHA, GL_FLOAT,//GL_LUMINANCE_ALPHA32F_ARB可以替换成2，表示2个分量
			(unsigned char*)data, osg::Image::USE_NEW_DELETE);
		*/
		/*
		float* data = new float[128 * 128 * 4];
		for (int i = 0; i < 128 * 128; i++)
		{
			data[i*4] = 0.5;
			data[i * 4 + 1] = 0.1;
			data[i * 4 + 2] = 0.6;
			data[i * 4 + 3] = 0.2;
		}
		img->setImage(128, 128, 1, GL_RGBA32F, GL_RGBA, GL_FLOAT,//GL_RGBA32F可以替换成4，表示4个分量
			(unsigned char*)data, osg::Image::USE_NEW_DELETE);
		*/
	}

	//纹理对象设置，用于向shader传递数组参数
	{
		/************************************************************************/
		/* InternalFormat内部格式的一些解释
		/* The difference between the three single-component texture formats, GL_ALPHA, GL_LUMINANCE and GL_INTENSITY, is in the way the four-component RGBA color vector is generated. If the value for a given texel is X, then the RGBA color vector generated is:
		* GL_ALPHA: RGBA = (0, 0, 0, X)
		* GL_LUMINANCE: RGBA = (X, X, X, 1)
		* GL_INTENSITY: RGBA = (X, X, X, X)
		In other words, if we interpret the alpha as transparency, GL_ALPHA would represent a completely black texture with varying transparency, GL_LUMINANCE is an opaque texture with varying color (a grayscale image), and GL_INTENSITY is a combination where both the color and alpha channel is varying.                                                                     */
		/************************************************************************/
		osg::Texture2D* tex2D = new osg::Texture2D();
		tex2D->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR);
		tex2D->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
		tex2D->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
		tex2D->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
		tex2D->setInternalFormat(GL_RED);
		tex2D->setSourceType(GL_FLOAT);
		tex2D->setSourceFormat(GL_RED);
		tex2D->setDataVariance(osg::Object::DYNAMIC);

		//矩形纹理
		osg::TextureRectangle* texRect = new osg::TextureRectangle();
		texRect->setFilter(osg::Texture::MIN_FILTER, osg::Texture::NEAREST);
		texRect->setFilter(osg::Texture::MAG_FILTER, osg::Texture::NEAREST);
		texRect->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
		texRect->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
		texRect->setInternalFormat(GL_LUMINANCE_ALPHA32F_ARB);
		texRect->setSourceType(GL_FLOAT);
		texRect->setSourceFormat(GL_LUMINANCE_ALPHA);
		texRect->setDataVariance(osg::Object::DYNAMIC);
	}
}

StateSet::~StateSet()
{

}
