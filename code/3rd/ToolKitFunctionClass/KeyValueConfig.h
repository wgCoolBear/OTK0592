#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>

// 最简单的key-value,key唯一
/************************************************************************/
/* 最简单的key-value，key唯一。例如：
ds_uri=ws://127.0.0.1:6020/deepstream

StartCmd=Start

StopCmd=Stop

ExePath=E:\OESimple.exe
/************************************************************************/
void readConfigFile(const char * cfgfilepath, std::map<std::string,std::string>& key_value);

/************************************************************************/
/*多个key列表，例如多个可执行文件路径
ds_uri=ws://127.0.0.1:6020/deepstream

StartCmd=Start

StopCmd=Stop

ExePath=E:\bin\A B啊\MainSimple.exe|5000
ExePath=E:\bin\Node Callback.exe
ExePath=E:\bin\OESimple.exe
/************************************************************************/
void readConfigFile(const char * cfgfilepath, std::map<std::string, std::vector<std::string>>& key_value);
struct EXE
{
	std::string exePath;//绝对路径，例如D:/a/ss/abc.exe
	std::string exeDir;//exe所在目录，例如D:/a/ss
	std::string exeName;//exe名字，例如abc.exe
	int delay;//延迟时间，毫秒
};
/*
//使用方式
std::vector<EXE> exes;
void main()
{
	std::map<std::string, std::string> key_value;
	readConfigFile("data/test.txt",key_value);

	// 读取分析配置文件中的exe信息
	std::map<std::string, std::vector<std::string>> key_value2;
	readConfigFile("data/test2.txt", key_value2);
	// 配置文件里面的exe路径后面跟着延时，用|分割
	for each (std::string s in key_value2["ExePath"])
	{
		EXE exe;
		size_t pos = s.rfind('|');
		std::string path = s;
		std::string delay = "0";
		if (pos != std::string::npos)
		{
			path = s.substr(0, pos);
			delay = s.substr(pos + 1);
		}
		// 将路径中的'\'替换成'/'
		while ((pos = path.find('\\')) != std::string::npos)
		{
			path.replace(pos, 1, "/");
		}
		std::cout << path << delay << std::endl;
		exe.exePath = path;
		if (path.find('/') != std::string::npos)
		{
			exe.exeDir = path.substr(0, path.rfind('/') + 1);
			exe.exeName = path.substr(path.rfind('/') + 1);
		}
		exe.delay = atoi(delay.data());
		exes.push_back(exe);
	}
	system("pause");
}*/