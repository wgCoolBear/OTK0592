#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <osgEarth/MapNode>
#include <osgViewer/Viewer>
#include <osgEarthUtil/EarthManipulator>

#define ACTION(func) void on_action_##func##_triggered(bool b);
#define FUNC(name) void MainWindow::on_action_##name##_triggered(bool b)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
	void init();
	//打开项目,on_action_*_triggered语法中，"open"和mainwindow.ui中的action对应，这样就不用显示的声明connect了
	bool on_action_open_triggered();
	//退出
	void on_action_quit_triggered();	
	//快速切片Shp
	ACTION(FastCutShp)
	//操纵器
	ACTION(Manipulator)
	//圆形区域
	ACTION(CircleArea)
	//数据通信链路
	ACTION(DataLinkLine)
	//动态波
	ACTION(Wave)
	//基础图形
	ACTION(BasicAnnotation)
	//测量
	ACTION(Analysis)
	//Shader
	ACTION(Shader)
	//Hole
	ACTION(Hole)
	//倾斜摄影数据
	ACTION(ObliquePhotography)
	//修改高程
	ACTION(VerticalScale)
	//高程分层染色
	ACTION(Contour)
	//多边形区域染色
	ACTION(DyeEarthInPolygonArea)
	//地球热力图
	ACTION(EarthHeatMap)
	//飞行航线
	ACTION(FlightPath)
	//飞行路径
	ACTION(FlyPath)
	//星空
	ACTION(StarSky)
	//TLE卫星轨道
	ACTION(TLESatellite)
	//太阳系
	ACTION(SolarSystem)
	//地表飞行
	ACTION(AnimationPathOnEarth)
private:
    Ui::MainWindow* _ui;

	osg::observer_ptr<osgViewer::Viewer> _viewer;
	osg::observer_ptr<osg::Group> _root;
	osg::ref_ptr<osgEarth::Util::EarthManipulator> _earthManipulator;
	//一些osg的东西可以加在这里
	osg::ref_ptr<osg::Group> _functions;
	osg::observer_ptr<osgEarth::Map> _map;
	//添加在地球上的例如Annotation需要添加到mapNode，否则看不见
	osg::observer_ptr<osgEarth::MapNode> _mapNode;
	//一些测试模型
	osg::ref_ptr<osg::Node> _cessna;
	osg::ref_ptr<osg::Node> _glider;
	osg::ref_ptr<osg::Node> _cow;
	osg::ref_ptr<osg::Node> _satellite;
};

#endif // MAINWINDOW_H
