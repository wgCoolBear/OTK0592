#include "TableHeadViewWithCheckBox.h"


TableHeadViewWithCheckBox::TableHeadViewWithCheckBox(int checkboxCol, Qt::Orientation orientation, QWidget *parent/*=Q_NULLPTR*/)
	:QHeaderView(orientation, parent),
	_checkboxCol(checkboxCol),
	_isChecked(true)
{
	//支持点击以实现排序
	setSectionsClickable(true);
	setSortIndicatorShown(true);
}

TableHeadViewWithCheckBox::~TableHeadViewWithCheckBox()
{

}

// 绘制复选框
void TableHeadViewWithCheckBox::paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const
{
	painter->save();
	QHeaderView::paintSection(painter, rect, logicalIndex);
	painter->restore();

	if (logicalIndex == _checkboxCol)
	{
		QStyleOptionButton option;
		option.rect=QRect(3,5,21,21);
		option.state = _isChecked ? QStyle::State_On : QStyle::State_Off;

		//style()->drawItemPixmap(painter, rect, Qt::AlignCenter, QPixmap(":/images/checkBoxChecked"));
		style()->drawControl(QStyle::CE_CheckBox, &option, painter, this);
		
		/*QCheckBox checkBox;
		option.iconSize = QSize(20, 20);
		option.rect = rect;
		style()->drawPrimitive(QStyle::PE_IndicatorCheckBox, &option, painter, &checkBox);*/
	}
}

// 鼠标按下表头
void TableHeadViewWithCheckBox::mousePressEvent(QMouseEvent *event)
{
	int nColumn = logicalIndexAt(event->pos());
	if ((event->buttons() & Qt::LeftButton) && (nColumn == _checkboxCol))
	{
		_isChecked=!_isChecked;
		updateSection(_checkboxCol);
		//发送checkbox状态改变信号
		emit checkStatusChanged(_isChecked);
	}
	else
	{
		QHeaderView::mousePressEvent(event);
	}
}