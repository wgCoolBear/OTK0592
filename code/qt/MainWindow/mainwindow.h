#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
class MoveableWidgetContainer;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
	void on_tableWidget_cellDoubleClicked(int row, int col);
	void on_tableWidget_currentCellChanged(int newRow, int newCol, int oldRow, int oldCol);
	void slotOnComboBox(int idx);
	void slotPopMenu(QPoint p);
	void on_pushButton_color_clicked();
private:
    Ui::MainWindow *ui;
	Ui::MoveableWidgetContainer* uiMoveableWidget;
};

#endif // MAINWINDOW_H
