#include "NodeVisitor.h"
#include <osg/Geometry>
#include <osg/Geode>

GetGeometryDataVisitor::GetGeometryDataVisitor()
	: osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN)
{

}

void GetGeometryDataVisitor::apply(osg::Geode& geode)
{
	//获取当前的节点路径
	auto nodePath = getNodePath();
	unsigned int count = geode.getNumDrawables();
	osg::notify(osg::ALWAYS) << "NodeVisitor查询到 "<< count <<" 个Drawables\n";
	for (unsigned int i = 0; i < count; i++)
	{
		osg::Geometry *geometry = geode.getDrawable(i)->asGeometry();
		if (!geometry)
			continue;
		// 顶点数据
		osg::Vec3Array* vertices = dynamic_cast<osg::Vec3Array*>(geometry->getVertexArray());
		int vertexlNum = vertices->size();
		for (int i = 0; i < vertexlNum; i++) {
			vertex.push_back(vertices->at(i));
		}
	}
}
