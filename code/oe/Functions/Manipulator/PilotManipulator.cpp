#include "PilotManipulator.h"
#include <osgEarth/GeoTransform>
#include "osg/AnimationPath"

#define SRS osgEarth::SpatialReference::get("wgs84")

PilotManipulator::PilotManipulator(osg::ref_ptr<osg::MatrixTransform> planeMT, ROTATE_MODE mode/*=AUTO*/)
	:_planeMT(planeMT),
	_mode(mode)
{
	osg::AnimationPath* animationPath = new osg::AnimationPath;
	animationPath->setLoopMode(osg::AnimationPath::SWING);

	float yaw = 0.0f;
	float roll = osg::inDegrees(30.0f);
	float pitch=0.0f;
	//构造测试数据
	for (int i=0;i<10;i++)
	{
		osg::Vec3 lla(-i*10, i*5, 4000000);
		osgEarth::GeoPoint gp(SRS, lla);
		osg::Matrixd worldMat;
		gp.createLocalToWorld(worldMat);
		osg::Vec3d position=worldMat.getTrans();
		osg::Quat rotation=worldMat.getRotate();
		animationPath->insert(i*10,osg::AnimationPath::ControlPoint(position,rotation));
	}
	//使用AnimationPathCallback使模型沿着测试路径飞行
	planeMT->addUpdateCallback(new osg::AnimationPathCallback(animationPath, 0.0, 1.0));
	//根据模型位置更新操纵器
	planeMT->addUpdateCallback(new UpdateMatrixCallback(this));
}

void PilotManipulator::setLLAHPR(float lon, float lat, float alt, float heading, float pitch, float roll)
{
	//位置
	osg::Vec3 lla(lon, lat, alt);
	osgEarth::GeoPoint gp(SRS, lla);
	//LLA对应的世界矩阵
	osg::Matrix posMatrix;
	gp.createLocalToWorld(posMatrix);

	//姿态
	osg::Matrix rotMatrix;
	rotMatrix.makeRotate(
		osg::inDegrees(roll), osg::Vec3(0, 1, 0),
		osg::inDegrees(pitch), osg::Vec3(1, 0, 0),
		osg::inDegrees(-heading), osg::Vec3(0, 0, 1));

	//设置操纵器矩阵
	setByMatrix(osg::Matrix::rotate(osg::PI_2, osg::X_AXIS)*
		rotMatrix*posMatrix);
}
//通过飞机模型的矩阵来设置操纵器
void PilotManipulator::setMatrix(osg::Matrix matrix)
{
	if (_mode==AUTO)
	{
		setByMatrix(matrix);
	}
	else//手动模式可以用鼠标控制旋转
	{
		osg::Matrixd oldMatrix = getMatrix();
		setByMatrix(osg::Matrix::rotate(oldMatrix.getRotate())*
			osg::Matrix::translate(matrix.getTrans()));
	}
}

void PilotManipulator::setPilotCoordinateFrameCallback()
{
	//如果想使得视角可以翻滚roll，不要锁定Up方向，即设为false
	setVerticalAxisFixed(false);

	//注意setCoordinateFrameCallback一定要在setCameraManipulator之后执行
	//因为setCameraManipulator里面执行了setCoordinateFrameCallback
	//setCoordinateFrameCallback(new PilotCoordinateFrameCallback());
}

PilotManipulator::PilotCoordinateFrameCallback::PilotCoordinateFrameCallback()
{

}
//设置操纵器的上方，右方，前方
osg::CoordinateFrame PilotManipulator::PilotCoordinateFrameCallback::getCoordinateFrame(const osg::Vec3d& position) const
{
	//这里取相机所在位置对应的矩阵，即上方是地心指向相机，前方指向北极
	osg::Matrixd coordinateFrame;
	osgEarth::GeoPoint gp;
	gp.fromWorld(SRS, position);
	gp.createLocalToWorld(coordinateFrame);
	return coordinateFrame;
}

PilotManipulator::PilotCoordinateFrameCallback::~PilotCoordinateFrameCallback()
{

}

PilotManipulator::UpdateMatrixCallback::UpdateMatrixCallback(PilotManipulator* pm)
	:_pm(pm)
{

}

void PilotManipulator::UpdateMatrixCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
{
	osg::ref_ptr<osg::MatrixTransform> mt = dynamic_cast<osg::MatrixTransform*>(node);
	if (mt)
	{
		_pm->setMatrix(mt->getMatrix());
	}
	traverse(node, nv);
}
