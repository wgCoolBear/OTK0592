#pragma once
#include "ui_AutoHideMessageBox.h"
class AutoHideMessageBox :public QDialog, Ui::Dialog
{
	Q_OBJECT
public:
	AutoHideMessageBox(QWidget* parent, const QString &title, const QString &text, bool autoHide=true, int showTime=2000, QDialogButtonBox::StandardButtons buttons=QDialogButtonBox::Ok);
	~AutoHideMessageBox();
public slots:
};

