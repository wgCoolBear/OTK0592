//C++ header - Open Scene Graph - Copyright (C) 1998-2002 Robert Osfield
//Distributed under the terms of the GNU Library General Public License (LGPL)
//as published by the Free Software Foundation.

#ifndef OSG_STATESET
#define OSG_STATESET 1

#include <osg/Object>
#include <osg/StateAttribute>
#include <osg/ref_ptr>

#include <map>
#include <vector>
#include <string>

/**
Encapsulates OpenGL state modes and attributes. 
Used to specific textures etc of osg::Drawable's which hold references
to a single osg::StateSet.  StateSet can be shared between Drawable's
and is recommend if possible as it minimize expensive state changes
in the graphics pipeline.
*/
class SG_EXPORT StateSet : public Object
{
    public :
    
            
        StateSet();
        StateSet(const StateSet&,const CopyOp& copyop=CopyOp::SHALLOW_COPY);

        virtual Object* cloneType() const { return osgNew StateSet(); }
        virtual Object* clone(const CopyOp& copyop) const { return osgNew StateSet(*this,copyop); }
        virtual bool isSameKindAs(const Object* obj) const { return dynamic_cast<const StateSet*>(obj)!=NULL; }
        virtual const char* libraryName() const { return "osg"; }
        virtual const char* className() const { return "StateSet"; }

        /** return -1 if *this < *rhs, 0 if *this==*rhs, 1 if *this>*rhs.*/
        int compare(const StateSet& rhs,bool compareAttributeContents=false) const;
        
        bool operator <  (const StateSet& rhs) const { return compare(rhs)<0; }
        bool operator == (const StateSet& rhs) const { return compare(rhs)==0; }
        bool operator != (const StateSet& rhs) const { return compare(rhs)!=0; }

        /** set all the modes to on or off so that it defines a 
            complete state, typically used for a default global state.*/
        void setGlobalDefaults();

        /** set all the modes to inherit, typically used to signify
            nodes which inherit all of their modes for the global state.*/
        void setAllToInherit();
        
       /** merge this stateset with stateset rhs, this overrides
          * the rhs if OVERRIDE is specified, otherwise rhs takes precedence.*/
        void merge(const StateSet& rhs);

        /** a container to map GLModes to their respective GLModeValues.*/
        typedef std::map<StateAttribute::GLMode,StateAttribute::GLModeValue>  ModeList;

        /** set this StateSet to contain specified GLMode and value.*/
        void setMode(const StateAttribute::GLMode mode, const StateAttribute::GLModeValue value);
        /** set this StateSet to inherit specified GLMode type from parents.
          * has the effect of deleting any GlMode of specified type from StateSet.*/
        void setModeToInherit(const StateAttribute::GLMode mode);

        /** get specified GLModeValue for specified GLMode.
          * returns INHERIT if no GLModeValue is contained within StateSet.*/
        const StateAttribute::GLModeValue getMode(const StateAttribute::GLMode mode) const;

        /** return the list of all GLModes contained in this StateSet.*/
        inline ModeList& getModeList() { return _modeList; }

        /** return the const list of all GLModes contained in this const StateSet.*/
        inline const ModeList& getModeList() const { return _modeList; }



        /** simple pairing between an attribute and its override flag.*/
        typedef std::pair<ref_ptr<StateAttribute>,StateAttribute::OverrideValue> RefAttributePair;
        /** a container to map StateAttribyte::Types to their respective RefAttributePair.*/
        typedef std::map<StateAttribute::Type,RefAttributePair>                  AttributeList;

        /** set this StateSet to contain specified attribute and override flag.*/
        void setAttribute(StateAttribute *attribute, const StateAttribute::OverrideValue value=StateAttribute::OFF);
        /** set this StateSet to contain specified attribute and set the associated GLMode's to specified value.*/
        void setAttributeAndModes(StateAttribute *attribute, const StateAttribute::GLModeValue value=StateAttribute::ON);
        /** set this StateSet to inherit specified attribute type from parents.
          * has the effect of deleting any state attributes of specified type from StateSet.*/
        void setAttributeToInherit(const StateAttribute::Type type);

        /** get specified StateAttribute for specified type.
          * returns NULL if no type is contained within StateSet.*/
        StateAttribute* getAttribute(const StateAttribute::Type type);

        /** get specified const StateAttribute for specified type.
          * returns NULL if no type is contained within const StateSet.*/
        const StateAttribute* getAttribute(const StateAttribute::Type type) const;

        /** get specified RefAttributePair for specified type.
          * returns NULL if no type is contained within StateSet.*/
        const RefAttributePair* getAttributePair(const StateAttribute::Type type) const;
        
        /** return the list of all StateAttributes contained in this StateSet.*/
        inline AttributeList& getAttributeList() { return _attributeList; }

        /** return the const list of all StateAttributes contained in this const StateSet.*/
        inline const AttributeList& getAttributeList() const { return _attributeList; }



        typedef std::vector<ModeList>       TextureModeList;

        /** set this StateSet to contain specified GLMode and value.*/
        void setTextureMode(unsigned int unit,const StateAttribute::GLMode mode, const StateAttribute::GLModeValue value);
        /** set this StateSet to inherit specified GLMode type from parents.
          * has the effect of deleting any GlMode of specified type from StateSet.*/
        void setTextureModeToInherit(unsigned int unit,const StateAttribute::GLMode mode);

        /** get specified GLModeValue for specified GLMode.
          * returns INHERIT if no GLModeValue is contained within StateSet.*/
        const StateAttribute::GLModeValue getTextureMode(unsigned int unit,const StateAttribute::GLMode mode) const;

        /** return the list of all Texture related GLModes contained in this StateSet.*/
        inline TextureModeList& getTextureModeList() { return _textureModeList; }

        /** return the const list of all Texture related GLModes contained in this const StateSet.*/
        inline const TextureModeList& getTextureModeList() const  { return _textureModeList; }


        typedef std::vector<AttributeList>  TextureAttributeList;

        /** set this StateSet to contain specified attribute and override flag.*/
        void setTextureAttribute(unsigned int unit,StateAttribute *attribute, const StateAttribute::OverrideValue value=StateAttribute::OFF);
        /** set this StateSet to contain specified attribute and set the associated GLMode's to specified value.*/
        void setTextureAttributeAndModes(unsigned int unit,StateAttribute *attribute, const StateAttribute::GLModeValue value=StateAttribute::ON);
        /** set this StateSet to inherit specified attribute type from parents.
          * has the effect of deleting any state attributes of specified type from StateSet.*/
        void setTextureAttributeToInherit(unsigned int unit,const StateAttribute::Type type);

        /** get specified Texture related StateAttribute for specified type.
          * returns NULL if no type is contained within StateSet.*/
        StateAttribute* getTextureAttribute(unsigned int unit,const StateAttribute::Type type);

        /** get specified Texture related const StateAttribute for specified type.
          * returns NULL if no type is contained within const StateSet.*/
        const StateAttribute* getTextureAttribute(unsigned int unit,const StateAttribute::Type type) const;

        /** get specified Texture related RefAttributePair for specified type.
          * returns NULL if no type is contained within StateSet.*/
        const RefAttributePair* getTextureAttributePair(unsigned int unit,const StateAttribute::Type type) const;
        
        /** return the list of all Texture related StateAttributes contained in this StateSet.*/
        inline TextureAttributeList& getTextureAttributeList() { return _textureAttributeList; }

        /** return the const list of all Texture related StateAttributes contained in this const StateSet.*/
        inline const TextureAttributeList& getTextureAttributeList() const { return _textureAttributeList; }


        void setAssociatedModes(const StateAttribute* attribute, const StateAttribute::GLModeValue value);
        
        void setAssociatedTextureModes(unsigned int unit,const StateAttribute* attribute, const StateAttribute::GLModeValue value);

        enum RenderingHint
        {
            DEFAULT_BIN = 0,
            OPAQUE_BIN = 1,
            TRANSPARENT_BIN = 2
        };
        
        /** set the RenderingHint of the StateSet.
          * RenderingHint is used by osgUtil::Renderer to determine which
          * draw bin to drop associated osg::Drawables in. For opaque
          * objects OPAQUE_BIN would typical used, which TRANSPARENT_BIN
          * should be used for objects which need to be depth sorted.*/
        void setRenderingHint(const int hint);

        /** get the RenderingHint of the StateSet.*/
        inline const int getRenderingHint() const      { return _renderingHint; }

        enum RenderBinMode
        {
            INHERIT_RENDERBIN_DETAILS,
            USE_RENDERBIN_DETAILS,
            OVERRIDE_RENDERBIN_DETAILS,
            ENCLOSE_RENDERBIN_DETAILS
        };

        /** set the render bin details.*/
        void setRenderBinDetails(const int binNum,const std::string& binName,const RenderBinMode mode=USE_RENDERBIN_DETAILS);
                
        /** set the render bin details to inherit.*/
        void setRendingBinToInherit();
        
        /** get the render bin mode.*/
        inline const RenderBinMode getRenderBinMode() const { return _binMode; }

        /** get whether the render bin details are set and should be used.*/
        inline const bool useRenderBinDetails() const { return _binMode!=INHERIT_RENDERBIN_DETAILS; }

        /** get the render bin number.*/
        inline const int getBinNumber() const { return _binNum; }

        /** get the render bin name.*/
        inline const std::string& getBinName() const { return _binName; }

        
        /** call compile on all StateAttributes contained within this StateSet.*/
        void compile(State& state) const;

    protected :


        virtual ~StateSet();

        StateSet& operator = (const StateSet&) { return *this; }
        
        ModeList                            _modeList;
        AttributeList                       _attributeList;

        TextureModeList                     _textureModeList;
        TextureAttributeList                _textureAttributeList;
        
        inline ModeList& getOrCreateTextureModeList(unsigned int unit)
        {
            if (unit>=_textureModeList.size()) _textureModeList.resize(unit+1);
            return _textureModeList[unit];
        }
        
        inline AttributeList& getOrCreateTextureAttributeList(unsigned int unit)
        {
            if (unit>=_textureAttributeList.size()) _textureAttributeList.resize(unit+1);
            return _textureAttributeList[unit];
        }
        
        int compareModes(const ModeList& lhs,const ModeList& rhs);
        int compareAttributePtrs(const AttributeList& lhs,const AttributeList& rhs);
        int compareAttributeContents(const AttributeList& lhs,const AttributeList& rhs);

        void setMode(ModeList& modeList,const StateAttribute::GLMode mode, const StateAttribute::GLModeValue value);
        void setModeToInherit(ModeList& modeList,const StateAttribute::GLMode mode);
        const StateAttribute::GLModeValue getMode(const ModeList& modeList,const StateAttribute::GLMode mode) const;
        void setAssociatedModes(ModeList& modeList,const StateAttribute* attribute, const StateAttribute::GLModeValue value);

        void setAttribute(AttributeList& attributeList,StateAttribute *attribute, const StateAttribute::OverrideValue value=StateAttribute::OFF);

        StateAttribute* getAttribute(AttributeList& attributeList,const StateAttribute::Type type);
        const StateAttribute* getAttribute(const AttributeList& attributeList,const StateAttribute::Type type) const;
        const RefAttributePair* getAttributePair(const AttributeList& attributeList,const StateAttribute::Type type) const;

        int                                 _renderingHint;

        RenderBinMode                       _binMode;
        int                                 _binNum;
        std::string                         _binName;

};


#endif
