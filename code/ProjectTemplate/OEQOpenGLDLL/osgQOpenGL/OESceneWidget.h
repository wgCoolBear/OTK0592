#pragma once
#include "../../../3rd/osgQOpenGL/BaseSceneWidget.h"
#include "osgEarth/MapNode"

class DLL_EXPORT OESceneWidget : public BaseSceneWidget
{
public:
	OESceneWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
	osgEarth::Map* getMap();
	osgEarth::MapNode* getMapNode();
	virtual void initializedFromOsgQOpenGLWidget() override;
protected:
	osgEarth::Map* _map;
	osgEarth::MapNode* _mapNode;
};