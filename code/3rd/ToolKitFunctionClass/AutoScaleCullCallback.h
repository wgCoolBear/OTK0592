#include "Export.h"
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osg/Camera>

/************************************************************************/
/* 实现一个自动缩放以保证在屏幕上大小保持不变的节点cull回调
/* osg::AutoTransform控制旋转还可以，控制大小就不好用了
/* 相关代码参照osgEarthAnnotation::GeoPositionNodeAutoScaler.cpp
/* osg::PositionAttitudeTransform* pat = new osg::PositionAttitudeTransform();
/* pat->addNode(model);
/* pat->addCullCallback(new AutoScaleCullCallback());
/* 功能尚未正确实现，不能使用
/************************************************************************/
class DLL_EXPORT AutoScaleCullCallback : public osg::NodeCallback
{
public:
	AutoScaleCullCallback(const osg::Vec3d& baseScale = osg::Vec3d(1,1,1), double minScale = 0.0, double maxScale = DBL_MAX);
	// node节点是当前回调所挂载的节点
	virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);
private:
	osg::Vec3d _baseScale;
	double _minScale;
	double _maxScale;
};