#include "STKManipulator.h"
#include "osgEarth\SpatialReference"
#include "osgEarth\GeoData"

STKManipulator::STKManipulator()
{
}

STKManipulator::~STKManipulator()
{

}

void STKManipulator::setByMatrix(const osg::Matrixd& matrix)
{
	matrix.getLookAt(_eye, _center, _up);
}

void STKManipulator::setByInverseMatrix(const osg::Matrixd& matrix)
{
	setByInverseMatrix(osg::Matrixd::inverse(matrix));
}

osg::Matrixd STKManipulator::getMatrix() const 
{
	return osg::Matrixd::inverse(getInverseMatrix());
}

osg::Matrixd STKManipulator::getInverseMatrix() const 
{
	return osg::Matrixd::lookAt(_eye,_center,_up);
}

void STKManipulator::home(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us)
{
	osgEarth::SpatialReference* srs = osgEarth::SpatialReference::get("wgs84");
	double earthRadius = srs->getEllipsoid()->getRadiusEquator();
	//经纬度(0,0)正上空
	osg::Vec3 lla(0, 0, earthRadius*2);
	osgEarth::GeoPoint gp(srs, lla);
	gp.toWorld(_eye);
	_center = osg::Vec3d();
	_up = osg::Z_AXIS;
	_lon=0;
	_lat=0;

	us.requestRedraw();
	us.requestContinuousUpdate( false );
	flushMouseEventStack();
}

void STKManipulator::init(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& us)
{
	 flushMouseEventStack();
	 us.requestContinuousUpdate(false);
}

bool STKManipulator::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us)
{
	switch(ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::FRAME):
		addMouseEvent(ea);
		return false;
	case(osgGA::GUIEventAdapter::RESIZE):
		init(ea,us);
		us.requestRedraw();
		return true;
	default:
		break;
	}

	if (ea.getHandled()) return false;

	switch(ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::PUSH):
		{
			addMouseEvent(ea);
			us.requestContinuousUpdate(true);
			return true;
		}

	case(osgGA::GUIEventAdapter::RELEASE):
		{
			addMouseEvent(ea);
			us.requestContinuousUpdate(true);
			return true;
		}

	case(osgGA::GUIEventAdapter::DRAG):
		{
			addMouseEvent(ea);
			us.requestContinuousUpdate(true);

			double mx0 = _ga_t0->getX(); double mx1 = _ga_t1->getX();
			double my0 = _ga_t0->getY(); double my1 = _ga_t1->getY();
			double dx = mx0-mx1;
			double dy = my0-my1;

			//相机上方与Z轴夹角是否小于180，用来处理越过南北极之后水平旋转的方向
			int dir=osg::sign(_up*osg::Z_AXIS);

			double scale=0.1;
			dx*=(scale*dir);
			dy*=scale;

			//当前经线与赤道交点(_lon,0)
			osg::Vec3d p(cos(_lon), sin(_lon), 0);
			//经线圈所在平面的法向量
			osg::Vec3d N=osg::Z_AXIS^p;

			int modKey = _ga_t0->getModKeyMask();
			//以当前视点位置为球心旋转
			if (modKey&osgGA::GUIEventAdapter::MODKEY_SHIFT)
			{
				osg::Vec3d lookDir = _center-_eye;
				double distance = lookDir.length();
				//水平方向旋转角度
				double rotLon = -dir*osg::inDegrees(dx);
				//垂直方向旋转角度
				double rotLat = osg::inDegrees(dy);
				//水平方向旋转矩阵
				osg::Matrixd lonMatrix = osg::Matrixd::rotate(rotLon, osg::Z_AXIS*dir);
				//垂直方向旋转矩阵
				osg::Matrixd latMatrix = osg::Matrixd::rotate(rotLat, N);
				//旋转之后的视线方向
				osg::Vec3d newLookDir = lookDir*lonMatrix*latMatrix;
				//旋转之后的中心点
				_center = _eye + newLookDir;
				//新视线与球面交点
				_lon+=rotLon;
				_lat-=rotLat;
				//更新后经线与赤道交点(_lon,0)
				osg::Vec3d newP(cos(_lon), sin(_lon), 0);
				//更新后经线圈所在平面的法向量
				osg::Vec3d newN=osg::Z_AXIS^newP;
				//更新后(_lon,_lat)所在位置
				osg::Vec3d p2(cos(_lat)*cos(_lon),cos(_lat)*sin(_lon),sin(_lat));
				//更新视点上方
				_up = p2^newN;
			}
			else
			{
				//限制在垂直方向旋转
				if (modKey&osgGA::GUIEventAdapter::MODKEY_CTRL)
				{
					dx=0;
				}
				//限制在水平方向旋转
				else if (modKey&osgGA::GUIEventAdapter::MODKEY_ALT)
				{
					dy=0;
				}
				//视线与球面交点
				_lon-=osg::inDegrees(dx);
				_lat-=osg::inDegrees(dy);
				//视线方向
				osg::Vec3d lookDir;
				lookDir.x()=cos(_lat)*cos(_lon);
				lookDir.y()=cos(_lat)*sin(_lon);
				lookDir.z()=sin(_lat);
				//中心点距视点距离
				double distance = (_eye-_center).length();
				//更新视点位置
				_eye = _center+lookDir*distance;
				//计算视点上方
				_up = lookDir^N;
			}
			return true;
		}

	case(osgGA::GUIEventAdapter::MOVE):
		{
			addMouseEvent(ea);
			us.requestContinuousUpdate(true);
			return true;
		}

	case(osgGA::GUIEventAdapter::KEYDOWN):
		{
			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Space)
			{
				flushMouseEventStack();
				home(ea,us);
				return true;
			}
			return false;
		}

	case(osgGA::GUIEventAdapter::KEYUP):
		{
			return false;
		}
	case(osgGA::GUIEventAdapter::SCROLL):
		{
			osg::Vec3d lookDir = _eye-_center;
			double distance = lookDir.normalize();
			osgGA::GUIEventAdapter::ScrollingMotion sm = ea.getScrollingMotion();
			if( sm== osgGA::GUIEventAdapter::SCROLL_UP)
			{
				_eye = _center+lookDir*distance*0.9;
				us.requestRedraw();
				return true;
			}else
			{
				_eye = _center+lookDir*distance*1.1;
				us.requestRedraw();
				return true;
			}
		}
	default:
		return false;
	}
}

void STKManipulator::addMouseEvent(const osgGA::GUIEventAdapter& ea)
{
	_ga_t1 = _ga_t0;
	_ga_t0 = &ea;
}

void STKManipulator::flushMouseEventStack()
{
	_ga_t1 = NULL;
	_ga_t0 = NULL;
}
