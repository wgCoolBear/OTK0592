#include "Export.h"
#include <osg/Group>
class DLL_EXPORT UpdateGeometry :public osg::Group
{
public:
	UpdateGeometry();
	~UpdateGeometry();

	void changeColor(osg::Vec4 newColor);
};