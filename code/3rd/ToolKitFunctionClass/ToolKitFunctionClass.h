#pragma once
#include "Export.h"
#include "osg\Node"
#include "osg\MatrixTransform"

#include "osgEarth\StringUtils"//此头文件中含有一系列字符操作函数

//包含osgearth工具开关
#define INCLUDE_OE 1
#if INCLUDE_OE
#include <osgEarth/SpatialReference>
#include <osgEarth/ElevationQuery>
#include <osgEarth/MapNode>
#include <osgEarth/GeoData>
#endif

class DLL_EXPORT ToolKitFunctionClass
{
public:
	//获取一个节点全部三角面片顶点的世界坐标
	static void getAllTrianglesVertex(osg::Node* node, std::set<osg::Vec3>& pos);

	//创建一个长方体Box，顶点用球显示，线框模式
	static osg::Geode* createBoxWithLinePoint(const std::vector<osg::Vec3>& corners);

	//创建一个带有纹理的四边形
	static osg::Geometry* createTexturedQuad(const std::string& imgFile, const osg::Vec3& corner, const osg::Vec3& widthVec, const osg::Vec3& heightVec);
	
	//创建一个简单的矩形，位于XOY平面
	static osg::Geode* createQuad(float w, float h);

	//创建一个简单的点
	static osg::Geode* createPoint();

	//根据mvp矩阵绘制平截体
	static osg::MatrixTransform* makeFrustumGeometry(osg::Matrixd proj, osg::Matrixd mv);

	//字符串与基本类型之间的转换
	template<typename T>
	static T as( const std::string& str, const T& default_value );
	template<typename T>
	static std::string toString(const T& value);

	//wstring与string转换
	static std::string w2s(const std::wstring &src);
	static std::wstring s2w(const std::string &src);
	//主要用于将含有中文的字符串转utf8
	static std::string s2utf8(const std::string &src);

	//创建经纬线网格
	static osg::Geode* createGraticule(float R, float spanLat=15, float spanLon=15, float lenPerSegment=1, osg::Vec4 color=osg::Vec4(1,1,1,1) );

	//创建球面，使用QUAD_STRIP，spanLat:两条纬线间隔度数，spanLon:两条经线间隔度数
	static osg::Group* createSphere(float R, bool withWire = true, float spanLat = 15, float spanLon = 15, osg::Vec4 color = osg::Vec4(0, 0.5, 1, 0.3));

	//创建扇面
	static osg::Geode* createFan(float R, float angle, osg::Vec4 color = osg::Vec4(0.3, 0.5, 0, 0.5));

	//创建一个自动缩放以保证在屏幕上大小保持不变的节点
	//osg::AutoTransform控制旋转还可以，控制大小就不好用了。
	//相关代码参照osgEarthAnnotation::GeoPositionNodeAutoScaler.cpp
	//实现代码见osg/Functions/NodeCallback/AutoScaleCullCallback.h
	static osg::Group* createAutoScaleModel(osg::Node* model);

#if INCLUDE_OE
	//根据经纬度获取对应的高程
	static double getElevation(osgEarth::MapNode* mapNode, osgEarth::GeoPoint lonlat);
	//经纬度高程转XYZ
	static osg::Vec3d LLA2XYZ(osgEarth::GeoPoint lla);
	//XYZ转经纬度高程，默认wgs84
	static osgEarth::GeoPoint XYZ2LLA(osg::Vec3d xyz, osgEarth::SpatialReference* srs = osgEarth::SpatialReference::get("wgs84"));
#endif

	//抗锯齿，在程序入口处添加
	//osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
	//ds->setNumMultiSamples(8);

	//osg指定向量旋转指定角度
	//向量AB，沿着n旋转10度
	//osg::Vec3 left = AB*osg::Matrix::rotate(osg::inDegrees(10.0), n);
	//osg::Vec3 right = AB*osg::Matrix::rotate(osg::inDegrees(-10.0), n);//right=-left
	//特殊情况下，可以使用向量叉乘实现，例如旋转90度
	//osg::Vec3 left = n^AB;
	//osg::Vec3 right = AB^n;

};

