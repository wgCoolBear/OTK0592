//C++ header - Open Scene Graph - Copyright (C) 1998-2002 Robert Osfield
//Distributed under the terms of the GNU Library General Public License (LGPL)
//as published by the Free Software Foundation.

#ifndef OSGUTIL_RENDERSTAGELIGHTING
#define OSGUTIL_RENDERSTAGELIGHTING 1

#include <osg/Object>
#include <osg/Light>
#include <osg/State>

#include <osgUtil/RenderLeaf>
#include <osgUtil/RenderGraph>

namespace osgUtil {

/**
 * RenderBin base class.
 */
class OSGUTIL_EXPORT RenderStageLighting : public osg::Object
{
    public:
    

        RenderStageLighting();
        virtual osg::Object* cloneType() const { return new RenderStageLighting(); }
        virtual osg::Object* clone(const osg::CopyOp&) const { return new RenderStageLighting(); } // note only implements a clone of type.
        virtual bool isSameKindAs(const osg::Object* obj) const { return dynamic_cast<const RenderStageLighting*>(obj)!=0L; }
        virtual const char* libraryName() const { return "osgUtil"; }
        virtual const char* className() const { return "RenderStageLighting"; }

        virtual void reset();
        
        typedef std::pair< const osg::StateAttribute*, osg::ref_ptr<osg::Matrix> >    AttrMatrixPair;
        typedef std::vector< AttrMatrixPair >                                   AttrMatrixList;

        virtual void addPositionedAttribute(osg::Matrix* matrix,const osg::StateAttribute* attr)
        {
            _attrList.push_back(AttrMatrixPair(attr,matrix));
        }

        virtual void draw(osg::State& state,RenderLeaf*& previous);
        
    public:
        
        AttrMatrixList _attrList;
        
    protected:
    
        virtual ~RenderStageLighting();

};

}

#endif

