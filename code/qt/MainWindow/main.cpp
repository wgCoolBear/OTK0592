#include "mainwindow.h"
#include "DarkStyleUI/DarkStyle.h"
#include "DarkStyleUI/framelesswindow/framelesswindow.h"
#include <QApplication>

#define USE_DARKSTYLE 0

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#if USE_DARKSTYLE
	// style our application with custom dark style
	QApplication::setStyle(new DarkStyle);
	QApplication::setPalette(QApplication::style()->standardPalette());

	// create frameless window (and set windowState or title)
	FramelessWindow framelessWindow;
	//framelessWindow.setWindowState(Qt::WindowFullScreen);
	//framelessWindow.setWindowTitle("test title");
	framelessWindow.setWindowIcon(a.style()->standardIcon(QStyle::SP_DesktopIcon));

	// create our mainwindow instance
	MainWindow *mainWindow = new MainWindow;

	// add the mainwindow to our custom frameless window
	framelessWindow.setContent(mainWindow);
	framelessWindow.show();
#else
	MainWindow w;
	w.show();
#endif

    return a.exec();
}
