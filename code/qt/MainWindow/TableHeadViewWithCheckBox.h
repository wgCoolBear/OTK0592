#pragma once
#include <QtGui>
#include <QPainter>
#include <QHeaderView>
#include <QStyleOptionButton>
#include <QStyle>
class TableHeadViewWithCheckBox:public QHeaderView
{
	Q_OBJECT
public:
	TableHeadViewWithCheckBox(int checkboxCol, Qt::Orientation orientation, QWidget *parent=Q_NULLPTR);
	~TableHeadViewWithCheckBox();
signals:
	void checkStatusChanged(bool);
protected:
	void paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const override;
	void mousePressEvent(QMouseEvent *event) override;
private:
	bool _isChecked;
	int _checkboxCol;
};