#pragma once
#include "Export.h"
#include <osgEarth/SpatialReference>
#include <osg/MatrixTransform>
#include <osgEarthUtil/EarthManipulator>
#include "osgViewer/Viewer"

class DLL_EXPORT TrackNode:public osg::MatrixTransform
{
public:
	TrackNode(osgViewer::Viewer* viewer, osg::ref_ptr<osg::Node> trackedNode);
};