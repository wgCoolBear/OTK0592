#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QLineEdit>
#include <QComboBox>
#include <QInputDialog>

#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>
#include <osgGA/TrackballManipulator>
#include "osgDB/ReadFile"
#include "osg/MatrixTransform"

#define ADDFUNC _functions->addChild(obj);_viewer->home();
#define REMOVEFUNC _functions->removeChild(obj);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow)
{
	_ui->setupUi(this);

	_viewer = _ui->sceneWidget->getViewer();
	_mainCamera = _viewer->getCamera();
	_root = _ui->sceneWidget->getRoot();

	_functions = new osg::Group();
	_root->addChild(_functions);
	
	//有纹理
	_clock = osgDB::readNodeFile("data/model/clock.osgb");
	_tank = osgDB::readNodeFile("data/model/tank.osgb");
	_lz = osgDB::readNodeFile("data/model/lz.osgb");//没有设置法向量
	_cow = osgDB::readNodeFile("data/model/cow.osgb");//TexGen是SPHERE_MAP
	
	//没有纹理
	_cessna = osgDB::readNodeFile("data/model/cessna.osgb");
	_glider = osgDB::readNodeFile("data/model/glider.osgb");//部分geometry的BLEND设置为False了
	_axes = osgDB::readNodeFile("data/model/axes.osgb");
	_redflag = osgDB::readNodeFile("data/model/redflag.osgb");
}

MainWindow::~MainWindow()
{
    delete _ui;
}

void MainWindow::on_action_quit_triggered()
{
	exit(0);
}
/************************************************************************/
/* 基础图元
/* 点、线、面、长方体、圆等
/************************************************************************/
#include "BasicShape/BasicShape.h"
FUNC(BasicShape)
{
	static BasicShape* obj = NULL;
	if (b)
	{
		obj = new BasicShape();
		_functions->addChild(obj);
	}
	else
	{
		REMOVEFUNC
	}
}
/************************************************************************/
/* 键盘和鼠标响应事件
/* 
/************************************************************************/
#include "KeyMouseEventHandler/KeyMouseEventHandler.h"
FUNC(KeyMouseEventHandler)
{
	static KeyMouseEventHandler* obj = NULL;
	if (b)
	{
		obj = new KeyMouseEventHandler();
		_viewer->addEventHandler(obj);
	}
	else
	{
		_viewer->removeEventHandler(obj);
	}
}
/************************************************************************/
/* 鼠标拾取模型
/* 示例代码中例举了多种通过鼠标位置与场景求交的结果
/************************************************************************/
#include "MousePick/MousePick.h"
FUNC(MousePick)
{
	static MousePick* obj = NULL;
	if (b)
	{
		obj = new MousePick();
		_viewer->addEventHandler(obj);
	}
	else
	{
		_viewer->removeEventHandler(obj);
	}
}
/************************************************************************/
/* 回调(包含相机绘制回调)
/* MT节点添加了一个回调，固定时间间隔模型旋转一个角度
/* 相机绘制回调从帧缓存中读取一部分数据，绘制在一个矩形中
/************************************************************************/
#include "NodeCallback/NodeCallback.h"
FUNC(NodeCallback)
{
	static osg::MatrixTransform* rotMT = NULL;
	static osg::MatrixTransform* fbMT = NULL;
	static osg::Geode* rectFrameBuffer = NULL;
	static ReadFrameBufferCameraDrawCallback* cdc = NULL;
	if (b)
	{
		rotMT = new osg::MatrixTransform();
		//添加一个模型和旋转回调
		rotMT->addChild(_glider);
		rotMT->addUpdateCallback(new RotCallback());

		osg::ref_ptr<osg::Image> image = new osg::Image();
		image->allocateImage(512, 512, 1, GL_RGBA, GL_UNSIGNED_BYTE);
		//设置主相机的绘制回调，回调中image不断从帧缓存中读取数据
		cdc = new ReadFrameBufferCameraDrawCallback(image);
		_mainCamera->setPostDrawCallback(cdc);
		//新建一个显示Image的矩形
		auto rectFrameBuffer = osg::createGeodeForImage(image);
		rectFrameBuffer->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
		rectFrameBuffer->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
		fbMT = new osg::MatrixTransform();
		//矩形偏移一下，否则就出现画中画了
		fbMT->setMatrix(osg::Matrix::translate(osg::Vec3(2,0,0)));
		fbMT->addChild(rectFrameBuffer);

		_functions->addChild(rotMT);
		_functions->addChild(fbMT);
	}
	else
	{
		_mainCamera->removePostDrawCallback(cdc);
		_functions->removeChild(rotMT);
		_functions->removeChild(fbMT);
	}
}
/************************************************************************/
/* 状态集StateSet
/* 设置网格模式、点大小、线宽、混合透明、PolygonOffset等
/************************************************************************/
#include "StateSet/StateSet.h"
FUNC(StateSet)
{
	static StateSet* obj = NULL;
	if (b)
	{
		obj = new StateSet(_cessna);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}
}
/************************************************************************/
/* 访问器NodeVisitor
/* 展示如何使用访问器查询节点中的信息
/************************************************************************/
#include "NodeVisitor/NodeVisitor.h"
FUNC(NodeVisitor)
{
	GetGeometryDataVisitor nv;
	_root->accept(nv);
}
/************************************************************************/
/* 更新顶点(显示列表、VBO)
/* Geometry顶点位置、颜色等更新
/************************************************************************/
#include "UpdateGeometry/UpdateGeometry.h"
FUNC(UpdateGeometry)
{
	static UpdateGeometry* obj = NULL;
	if (b)
	{
		obj = new UpdateGeometry();
		obj->changeColor(osg::Vec4(1.0, 1.0, 0.0, 1));
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}
}
/************************************************************************/
/* 渲染模型颜色信息和深度信息到纹理或者Image
/* 使用setPostDrawCallback可以将Image输出到文件，便于调试
/************************************************************************/
//#include "RTT/RTT.h"
FUNC(RTT)
{
	/*static RTT* obj = NULL;
	if (b)
	{
		obj = new RTT(_cessna);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 动画路径
/* 快速通过关键点构建一条带有位置和旋转信息的插值路径
/* 通过mt->setUpdateCallback作用到MT节点
/* 示例中实现了飞机沿着圆圈飞行
/************************************************************************/
//#include "AnimationPath/AnimationPath.h"
FUNC(AnimationPath)
{
	/*static AnimationPath* obj = NULL;
	if (b)
	{
		obj = new AnimationPath(_glider);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 骨骼动画FBX
/* 读取fbx动画模型文件，获取其中的动画管理器，获取动画列表，播放动画
/* 使用3dmax做好动画后，不能直接导出.osgb格式，因为它不会导出动画
/* 应该先导出fbx格式，如果需要，可以使用osgconv工具将fbx转为osgb
/* osgdb_fbx插件的编译和具体转换细节见BoneAnimationFBX.h
/************************************************************************/
//#include "BoneAnimationFBX/BoneAnimationFBX.h"
FUNC(FBX)
{
	/*static BoneAnimationFBX* obj = NULL;
	if (b)
	{
		osg::Node* model = osgDB::readNodeFile("data/model/Sasuke.osgb.(0.01).scale");
		if (model)
		{
			obj = new BoneAnimationFBX(model);
			ADDFUNC
		}
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 样条曲线
/* 控制点在曲线上的B样条曲线
/************************************************************************/
//#include "SplineCurve/SplineCurve.h"
FUNC(SplineCurve)
{
	/*static SplineCurve* obj = NULL;
	if (b)
	{
		obj = new SplineCurve();
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 特效
/* 固定功能管线、高亮、调色、半透明、淡入淡出、表面网格
/************************************************************************/
//#include "Effect/Effect.h"
FUNC(Effect)
{
	//static Effect* obj = NULL;
	//static int clearMask = 0;
	//if (b)
	//{
	//	//模型轮廓特效需要模板缓存
	//	clearMask = _viewer->getCamera()->getClearMask();
	//	_viewer->getCamera()->setClearMask(clearMask | GL_STENCIL_BUFFER_BIT);
	//	_viewer->getCamera()->setClearStencil(0);
	//	// must have stencil buffer...
	//	osg::DisplaySettings::instance()->setMinimumNumStencilBits(1);

	//	obj = new Effect(_lz, _tank);
	//	ADDFUNC
	//}
	//else
	//{
	//	_viewer->getCamera()->setClearMask(clearMask);
	//	REMOVEFUNC
	//}
}
/************************************************************************/
/* 有向包围盒OBB
/************************************************************************/
//#include "OBB/OBB.h"
FUNC(OBB)
{
	/*static OBB* obj = NULL;
	if (b)
	{
		obj = new OBB(_glider);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 提取模型边界
/************************************************************************/
//#include "ExtractModelEdge/ExtractModelEdge.h"
FUNC(ExtractModelEdge)
{
	/*static ExtractModelEdge* obj = NULL;
	if (b)
	{
		obj = new ExtractModelEdge(_lz);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 延迟渲染
/************************************************************************/
//#include "DeferredShading/DeferredShading.h"
FUNC(DeferredShading)
{
	//static DeferredShading* obj = NULL;
	//if (b)
	//{
	//	//obj = new DeferredShading();
	//	ADDFUNC
	//}
	//else
	//{
	//	REMOVEFUNC
	//}
}
/************************************************************************/
/* Shader
/************************************************************************/
//#include "Shader/Shader.h"
FUNC(Shader)
{
	/*static ShaderTest1* obj1 = NULL;
	static ShaderTest2* obj2 = NULL;
	static ShaderTest3* obj3 = NULL;
	static ShaderTest4* obj4 = NULL;
	static ShaderTest6* obj6 = NULL;
	static ShaderTest7* obj7 = NULL;
	if (b)
	{
		obj1 = new ShaderTest1(_glider);
		obj2 = new ShaderTest2(_lz);
		obj3 = new ShaderTest3(_lz);
		obj4 = new ShaderTest4(_lz);
		obj6 = new ShaderTest6(_lz);
		obj7 = new ShaderTest7(_lz, _mainCamera);
		_functions->addChild(obj1);
		_functions->addChild(obj2);
		_functions->addChild(obj3);
		_functions->addChild(obj4);
		_functions->addChild(obj6);
		_functions->addChild(obj7);
	}
	else
	{
		_functions->removeChild(obj1);
		_functions->removeChild(obj2);
		_functions->removeChild(obj3);
		_functions->removeChild(obj4);
		_functions->removeChild(obj6);
		_functions->removeChild(obj7);
	}*/
}
/************************************************************************/
/* 多面体与模型求交
/* 
/************************************************************************/
//#include "CutModel/CutModel.h"
FUNC(CutModel)
{
	/*static CutModel* obj = NULL;
	if (b)
	{
		obj = new CutModel(_lz);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* HUD
/* 
/************************************************************************/
//#include "HUD/HUD.h"
FUNC(HUD)
{
	/*static HUD* obj = NULL;
	if (b)
	{
		obj = new HUD(_lz);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* FBOScreen
/* 
/************************************************************************/
//#include "FBOScreen/FBOScreen.h"
FUNC(FBOScreen)
{
	/*static FBOScreen* obj = NULL;
	if (b)
	{
		obj = new FBOScreen(_lz);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 半透明的球
/*
/************************************************************************/
//#include "TransparentBall/TransparentBall.h"
FUNC(TransparentBall)
{
	/*static TransparentBall* obj = NULL;
	if (b)
	{
		obj = new TransparentBall();
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 布尔运算
/* 使用CGAL库计算
/************************************************************************/
#ifdef USE_CGAL
#include "BooleanOperationCGAL/BooleanOperationCGAL.h"
#endif
FUNC(BooleanOperation1)
{
#ifdef USE_CGAL
	static BooleanOperationCGAL* obj = NULL;
	if (b)
	{
		obj = new BooleanOperationCGAL(_clock);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}
#else
	QMessageBox::information(this, "", QStringLiteral("没有设置CGAL库"));
#endif
}
/************************************************************************/
/* 布尔运算
/* 使用osgModeling库计算
/************************************************************************/
#ifdef USE_OSGMODELING
#include "BooleanOperationOsgModeling/BooleanOperationOsgModeling.h"
#endif
FUNC(BooleanOperation2)
{
#ifdef USE_OSGMODELING
	static BooleanOperationOsgModeling* obj = NULL;
	if (b)
	{
		obj = new BooleanOperationOsgModeling();
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}
#else
	QMessageBox::information(this, "", QStringLiteral("没有设置OSGMODELING库"));
#endif
}
/************************************************************************/
/* ProjectiveTexture
/* 投影纹理映射
/************************************************************************/
//#include "ProjectiveTexture/ProjectiveTexture.h"
FUNC(ProjectiveTexture)
{
	/*static ProjectiveTexture* obj = NULL;
	if (b)
	{
		obj = new ProjectiveTexture();
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* Viewshed
/* 视域分析
/************************************************************************/
//#include "Viewshed/Viewshed.h"
FUNC(Viewshed)
{
	/*static Viewshed* obj = NULL;
	if (b)
	{
		obj = new Viewshed();
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 简单拖拽器
/* 
/************************************************************************/
#include "Dragger/Dragger.h"
FUNC(Dragger)
{
	static SimpleDragger* obj = NULL;
	if (b)
	{
		obj = new SimpleDragger(_cessna);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}
}
/************************************************************************/
/* 发光效果高斯模糊
/*
/************************************************************************/
//#include "GlowWithGaussianBlur/GlowWithGaussianBlur.h"
FUNC(GlowWithGaussianBlur)
{
	/*static GlowWithGaussianBlur* obj = NULL;
	if (b)
	{
		obj = new GlowWithGaussianBlur();
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* ShaderToy
/*
/************************************************************************/
//#include "ShaderToy/ShaderToy.h"
FUNC(ShaderToy)
{
	/*static ShaderToy* obj = NULL;
	if (b)
	{
		QStringList items;
		items << QStringLiteral("夜视仪1")
			<< QStringLiteral("夜视仪2")
			<< QStringLiteral("夜视仪3")
			<< QStringLiteral("夜视仪4")
			<< QStringLiteral("夜视仪5");
		bool ok;
		QString item = QInputDialog::getItem(0, QStringLiteral("选择shadertoy着色器"), QStringLiteral("着色器:"), items, 0, false, &ok);
		if (ok == false)
		{
			return;
		}
		int id = items.indexOf(item);
		auto shaderFileName="";
		if (id == 0)shaderFileName = "nvg1.fs";
		else if (id == 1)shaderFileName = "nvg2.fs";
		else if (id == 2)shaderFileName = "nvg3.fs";
		else if (id == 3)shaderFileName = "nvg4.fs";
		else if (id == 4)shaderFileName = "nvg5.fs";
		int vpWidth = _mainCamera->getViewport()->width();
		int vpHeight = _mainCamera->getViewport()->height();
		obj = new ShaderToy(_lz, osg::Vec2i(vpWidth, vpHeight), shaderFileName);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 箭头
/* 通过拖拽关键点编辑箭头形状。三种箭头样式，普通、双头、贝塞尔
/************************************************************************/
//#include "Arrow/Arrow.h"
FUNC(Arrow)
{
	//static Arrow* obj = NULL;
	//if (b)
	//{
	//	obj = new Arrow();
	//	ADDFUNC
	//}
	//else
	//{
	//	REMOVEFUNC
	//}
}

/************************************************************************/
/* EDL点云阴影染色算法
/************************************************************************/
//#include "EDL/EDL.h"
FUNC(EDL)
{
	/*static EDL* obj = NULL;
	if (b)
	{
		obj = new EDL(_mainCamera);
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}

/************************************************************************/
/* 雷达特效
/************************************************************************/
//#include "Radar/Radar.h"
FUNC(Radar)
{
	/*static Radar* obj = NULL;
	if (b)
	{
		obj = new Radar();
		ADDFUNC
	}
	else
	{
		REMOVEFUNC
	}*/
}
/************************************************************************/
/* 简化模型
/************************************************************************/
#if _MSC_VER>=1900
//#include "MeshSimplify/MeshSimplify.h"
#endif
FUNC(MeshSimplify)
{
#if _MSC_VER>=1900//VS2015
	/*if (b)
	{
		std::string objIn = "data/model/Tile_+019_+025/Tile_+019_+025.obj";
		std::string objOut = "";
		float reduceFraction = 0.6f;
		MeshSimplify::simplify(objIn, objOut, reduceFraction);
	}*/
#endif
}