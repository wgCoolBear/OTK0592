#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QLineEdit>
#include <QComboBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow)
{
	_ui->setupUi(this);
#if USE_QOpenGLWidget
	connect(_ui->sceneWidget, &SceneWidget::initialized, this, &MainWindow::init);
#else
	init();
#endif
	//���ر�������״̬����������
	//setWindowFlags(Qt::FramelessWindowHint);
	//statusBar()->hide();
	//_ui->mainToolBar->hide();
}

MainWindow::~MainWindow()
{
    delete _ui;
}

void MainWindow::init()
{
	_viewer = _ui->sceneWidget->getViewer();
	_root = _ui->sceneWidget->getRoot();
	_earthManipulator = dynamic_cast<osgEarth::Util::EarthManipulator*>(_viewer->getCameraManipulator());
	_functions = new osg::Group();
	_root->addChild(_functions);
	_map = _ui->sceneWidget->getMap();
	_mapNode = _ui->sceneWidget->getMapNode();
	_cessna = osgDB::readNodeFile("data/model/cessna.osgb");
	_glider = osgDB::readNodeFile("data/model/glider.osgb");
	_cow = osgDB::readNodeFile("data/model/cow.osgb");
	_satellite = osgDB::readNodeFile("data/model/satellite.osgb.10000.scale");
}

bool MainWindow::on_action_open_triggered()
{
	//QMessageBox::warning(NULL, "warning", "Failed to load earth file");

	return false;
}

void MainWindow::on_action_quit_triggered()
{
	exit(0);
}

void MainWindow::on_pushButton_jingHuFly_clicked()
{
	_ui->sceneWidget->jingHuFly();
}

void MainWindow::on_pushButton_taiHu_clicked()
{
	_ui->sceneWidget->taiHu();
}
