#include "JarAppCall.h"

#include <iostream>


JarAppCall::JarAppCall()
{
}


JarAppCall::~JarAppCall()
{
}

HANDLE JarAppCall::Run(const std::string& strJarDir, const std::string& strJarPath)
{
	//缓存当前目录
	char sOldDir[MAX_PATH] = { 0 };
	GetCurrentDirectory(MAX_PATH, sOldDir);

	//设置当前目录为jar包目录
	SetCurrentDirectory(strJarDir.data());
  HANDLE h = CallApp(strJarDir,strJarPath);
  //std::cout << "strJarDir:" << strJarDir << std::endl;
	//还原当前目录
	SetCurrentDirectory(sOldDir);

	return h;
}

// strJarPath绝对路径
HANDLE JarAppCall::CallApp(const std::string& strJarDir, const std::string& strJarPath)
{
	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	/*
	char sSysDir[MAX_PATH] = { 0 };
	GetSystemDirectory(sSysDir, MAX_PATH);
	std::string strFullPath = sSysDir;
	strFullPath += "\\cmd.exe";
	std::string strCmdLine = " /c ";
	strCmdLine += "start \"AAAA\" java -jar ";
	//strCmdLine += "E:\\PITCProjects\\FZZRepository\\trunk\\SMSA\\dev\\node_DGX\\DSServer\\LR(1).jar";
	strCmdLine += strJarPath;
	//std::cout << "strCmdLine:" << strFullPath+strCmdLine << std::endl;
	BOOL bRunProgram = CreateProcess(strFullPath.data(),
		(LPSTR)strCmdLine.data(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	*/

	//strCmdLine = "java -jar E:\\PITCProjects\\FZZRepository\\trunk\\SMSA\\dev\\node_DGX\\DSServer\\LR(1).jar";
	std::string strCmdLine = "java -jar "+ strJarPath;
	si.lpTitle = (LPSTR)strJarPath.data();//控制台窗口标题
	BOOL bRunProgram = CreateProcess(NULL,
		(LPSTR)strCmdLine.data(), //命令行
		NULL, NULL, FALSE, 
		CREATE_NEW_CONSOLE, //为新进程创建一个新的控制台窗口
		NULL, 
		strJarDir.data(), //子进程的工作路径
		&si, &pi);
	if (!bRunProgram)
	{
		return NULL;
	}

	// 等待程序结束
	//WaitForSingleObject(pi.hProcess, 1000/*INFINITE*/);
	//BOOL b=TerminateProcess(pi.hProcess,0);
	//CloseHandle(pi.hProcess);
	//CloseHandle(pi.hThread);

	return pi.hProcess;
}
