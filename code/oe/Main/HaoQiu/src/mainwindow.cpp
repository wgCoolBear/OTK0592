#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QLineEdit>
#include <QComboBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow)
{
	_ui->setupUi(this);
#if USE_QOpenGLWidget
	connect(_ui->sceneWidget, &SceneWidget::initialized, this, &MainWindow::init);
#else
	init();
#endif
}

MainWindow::~MainWindow()
{
    delete _ui;
}

void MainWindow::init()
{
	_viewer = _ui->sceneWidget->getViewer();
	_root = _ui->sceneWidget->getRoot();
	_earthManipulator = dynamic_cast<osgEarth::Util::EarthManipulator*>(_viewer->getCameraManipulator());
	_functions = new osg::Group();
	_root->addChild(_functions);
	_map = _ui->sceneWidget->getMap();
	_mapNode = _ui->sceneWidget->getMapNode();
	_cessna = osgDB::readNodeFile("data/model/cessna.osgb");
	_glider = osgDB::readNodeFile("data/model/glider.osgb");
	_cow = osgDB::readNodeFile("data/model/cow.osgb");
	_satellite = osgDB::readNodeFile("data/model/satellite.osgb.10000.scale");
}

bool MainWindow::on_action_open_triggered()
{
	//QMessageBox::warning(NULL, "warning", "Failed to load earth file");

	return false;
}

void MainWindow::on_action_quit_triggered()
{
	exit(0);
}
/************************************************************************/
/* 快速切片矢量数据到tms瓦片服务
/* 比osgearth_package速度快。
/* 主要实现思路是不断的移动相机的位置，使用RTT将对应的瓦片写到Image
/************************************************************************/
//#include "FastCutShp/FastCutShp.h"
FUNC(FastCutShp)
{
// 	static FastCutShp* obj = NULL;
// 	if (b)
// 	{
// 		obj = new FastCutShp(4, 2);
// 		_functions->addChild(obj);
// 	}
// 	else
// 	{
// 		_functions->removeChild(obj);
// 	}
}
/************************************************************************/
/* 特殊场景操纵器
/* STK操纵器：模拟STK软件操作地球方式
/* 飞行员视角操纵器：模拟飞行员视角，视角旋转模式有自动和手动
/* 以当前鼠标位置为中心缩放操纵器:
/* 绑定鼠标按键响应：左键控制旋转，中键控制平移，右键控制缩放
/* 跟踪模型节点：视点跟随模型
/************************************************************************/
#include "Manipulator/Manipulator.h"
FUNC(Manipulator)
{
	static osg::MatrixTransform* mtPlane = NULL;
	if (b)
	{
		QStringList items;
		items << QStringLiteral("STK操纵器") 
			<< QStringLiteral("飞行员视角操纵器(自动旋转)")
			<< QStringLiteral("飞行员视角操纵器(手动旋转)")
			<< QStringLiteral("以当前鼠标位置为中心缩放操纵器")
			<< QStringLiteral("绑定鼠标按键响应")
			<< QStringLiteral("跟踪模型节点");
		bool ok;
		QString item = QInputDialog::getItem(0, QStringLiteral("选择一个操纵器"),QStringLiteral("操纵器类型:"), items, 1, false, &ok);
		if (ok==false)
		{
			return;
		}
		int id = items.indexOf(item);
		if (id==0)
		{
			_viewer->setCameraManipulator(new STKManipulator());
		}
		else if (id==1||id==2)
		{
			mtPlane = new osg::MatrixTransform();
			mtPlane->addChild(_glider);
			_functions->addChild(mtPlane);
			PilotManipulator* pilotManip = new PilotManipulator(mtPlane, id==1?(PilotManipulator::AUTO):(PilotManipulator::HAND));
			_viewer->setCameraManipulator(pilotManip);
			pilotManip->setPilotCoordinateFrameCallback();
		}
		else if (id==3)
		{
#if OSGEARTH_VERSION_LESS_OR_EQUAL(2,8,10)
			//以下代码在oe2.8版本可以使用
			_viewer->setCameraManipulator(new ZoomOnMouseEarthManipulator());
#elif OSGEARTH_VERSION_GREATER_OR_EQUAL(2,10,2)
			//oe2.10.2(GitHub上找2.10分支，不要去Tags里面找2.10.2，Tag不是最新的)的EarthManipulator提供了zoomToMouse功能
			osg::ref_ptr<osgEarth::Util::EarthManipulator::Settings> settings = new osgEarth::Util::EarthManipulator::Settings;
			*settings=*(_earthManipulator->getSettings());
			settings->setZoomToMouse(true);
			osg::ref_ptr<osgEarth::Util::EarthManipulator> em = new osgEarth::Util::EarthManipulator();
			em->applySettings(settings);
			_viewer->setCameraManipulator(em);
#else
			QMessageBox::information(this, "", QStringLiteral("当前OE版本不支持此功能"));
#endif
		}
		else if(id==4)
		{
			//左键控制旋转，中键控制平移，右键控制缩放
			osg::ref_ptr<osgEarth::Util::EarthManipulator::Settings> settings = new osgEarth::Util::EarthManipulator::Settings;
			*settings=*(_earthManipulator->getSettings());
			settings->bindMouse(osgEarth::Util::EarthManipulator::ACTION_ROTATE,
				osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON);
			settings->bindMouse(osgEarth::Util::EarthManipulator::ACTION_PAN,
				osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON);
			osg::ref_ptr<osgEarth::Util::EarthManipulator> em = new osgEarth::Util::EarthManipulator();
			em->applySettings(settings);
			_viewer->setCameraManipulator(em);
		}
		else if (id==5)
		{
			mtPlane = new TrackNode(_viewer.get(), _satellite);
			_functions->addChild(mtPlane);
		}
	}
	else
	{
		_functions->removeChild(mtPlane);
		_viewer->setCameraManipulator(_earthManipulator.get());
	}
}
/************************************************************************/
/* 绘制一个圆形区域，带有半径箭头
/************************************************************************/
//#include "CircleArea/CircleArea.h"
FUNC(CircleArea)
{
// 	static CircleArea* obj = NULL;
// 	if (b)
// 	{
// 		obj = new CircleArea(osg::Vec3(120, 20, 1000),//圆心
// 			1000,//半径
// 			osgEarth::Symbology::Color(osgEarth::Symbology::Color::Red, 0.7));//颜色
// 		//oe2.10Annotation必须加在mapNode下，否则看不见。oe2.8加在root就可以看见
// 		_mapNode->addChild(obj);
// 	}
// 	else
// 	{
// 		_mapNode->removeChild(obj);
// 	}
}
/************************************************************************/
/* 通信链路(//TODO着色器有问题)
/************************************************************************/
//#include "DataLinkLine/DataLinkLine.h"
FUNC(DataLinkLine)
{
// 	static DataLinkLine* obj = NULL;
// 	if (b)
// 	{
// 		obj=new DataLinkLine(osg::Vec3d(110.392714, 39.9174, 10000),
// 			osg::Vec3d(116.392714, 39.9174, 20000));
// 		//oe2.10osgEarth::Annotation::FeatureNode必须加在mapNode下，
// 		//否则看不见。oe2.8加在root就可以看见
// 		//osgEarth::Annotation::PlaceNode和LabelNode不必强制加在mapNode下
// 		_mapNode->addChild(obj);
// 	}
// 	else
// 	{
// 		_mapNode->removeChild(obj);
// 	}
}
/************************************************************************/
/* 动态波（锥形波、扇形波）
/************************************************************************/
//#include "Wave/ConeWave.h"
//#include "Wave/FanWave.h"
FUNC(Wave)
{
// 	static ConeWave* coneWave = NULL;
// 	static FanWave* fanWave = NULL;
// 	if (b)
// 	{
// 		osg::Vec3d basePoint, topPoint;
// 		osgEarth::GeoPoint(_mapNode->getMapSRS(), osg::Vec3(110, 30, 0)).toWorld(basePoint);
// 		osgEarth::GeoPoint(_mapNode->getMapSRS(), osg::Vec3(110, 30, 3000000)).toWorld(topPoint);
// 		coneWave = new ConeWave(topPoint, basePoint, 1000000);
// 
// 		fanWave = new FanWave(
// 			osg::Vec3(80, 25, 0),//起始点
// 			osg::Vec3(90, 50, 0), //目标点
// 			30,//角度范围
// 			osgEarth::Symbology::Color::Blue);//颜色
// 
// 		_functions->addChild(coneWave);
// 		//oe2.10Annotation必须加在mapNode下，否则看不见。oe2.8加在root就可以看见
// 		_mapNode->addChild(fanWave);
// 	}
// 	else
// 	{
// 		_functions->removeChild(coneWave);
// 		_mapNode->removeChild(fanWave);
// 	}
}
/************************************************************************/
/* 基础图形
/************************************************************************/
#include "BasicAnnotation/BasicAnnotation.h"
FUNC(BasicAnnotation)
{
	static BasicAnnotation* anno = NULL;
	if (b)
	{
		anno = new BasicAnnotation(_mapNode.get());
		//oe2.10Annotation必须加在mapNode下，否则看不见。oe2.8加在root就可以看见
		_mapNode->addChild(anno);
	}
	else
	{
		_mapNode->removeChild(anno);
	}
}
/************************************************************************/
/* 基础测量距离、面积等
/************************************************************************/
//#include "Analysis/Analysis.h"
FUNC(Analysis)
{
// 	static Analysis* anal = NULL;
// 	if (b)
// 	{
// 		anal = new Analysis(_viewer.get(), _mapNode.get());
// 		_mapNode->addChild(anal);
// 	}
// 	else
// 	{
// 		_mapNode->removeChild(anal);
// 	}
}
/************************************************************************/
/* Shader
/* 展示了如何在地球添加shader。示例展示了给地球添加一层迷雾，根据视点高程逐渐稀释浓度
/************************************************************************/
#include "OEShader/Shader.h"
FUNC(Shader)
{
	if (b)
	{
		Shader::AddShaderOnEarth(_mapNode.get());
	}
	else
	{
		Shader::RemoveShader(_mapNode.get());
	}
}
/************************************************************************/
/* 挖洞
/* 使用MaskLayer挖洞
/************************************************************************/
//#include "Hole/Hole.h"
FUNC(Hole)
{
// 	if (b)
// 	{
// 		Hole::AddHole(_mapNode.get());
// 	}
// 	else
// 	{
// 		Hole::RemoveHole(_mapNode.get());
// 	}
}
/************************************************************************/
/* 加载倾斜摄影数据
/* 附加文档doc目录中有关于如何将倾斜摄影数据嵌入到地球的代码
/* 包括计算器倾斜摄影数据的边界，然后使用MaskLayer挖洞或者使用shader挖洞
/************************************************************************/
//#include "ObliquePhotography/ObliquePhotography.h"
FUNC(ObliquePhotography)
{
// 	static ObliquePhotography* obj = NULL;
// 	if (b)
// 	{
// 		float lon = 120.53;
// 		float lat = 37.82;
// 		osgEarth::Viewpoint viewpoint("", lon, lat, 0, 0, -90.0, 1.0e3);
// 		_earthManipulator->setViewpoint(viewpoint);
// 		obj = new ObliquePhotography(_mapNode.get(), lon, lat);
// 		_mapNode->addChild(obj);
// 	}
// 	else
// 	{
// 		_mapNode->removeChild(obj);
// 	}
}
/************************************************************************/
/* 修改高程
/************************************************************************/
//#include "VerticalScale/VerticalScale.h"
FUNC(VerticalScale)
{
// 	if (b)
// 	{
// 		osgEarth::Viewpoint viewpoint("", 116.1, 40.0, 15, 0, -45.0, 1.0e4);
// 		_earthManipulator->setViewpoint(viewpoint);
// 		VerticalScale::AddShader(_mapNode.get(), _viewer.get());
// 	}
// 	else
// 	{
// 		VerticalScale::RemoveShader(_mapNode.get(), _viewer.get());
// 	}
}
/************************************************************************/
/* 高程分层染色
/************************************************************************/
//#include "Contour/Contour.h"
FUNC(Contour)
{
// 	if (b)
// 	{
// 		osgEarth::Viewpoint viewpoint("", 116.3937, 39.9184, 15, -2.50, -90.0, 1.0e5);
// 		_earthManipulator->setViewpoint(viewpoint);
// 		Contour::AddShader(_mapNode.get(), _viewer.get());
// 	}
// 	else
// 	{
// 		Contour::RemoveShader(_mapNode.get(), _viewer.get());
// 	}
}
/************************************************************************/
/* 多边形区域染色
/************************************************************************/
//#include "DyeEarthInPolygonArea/DyeEarthInPolygonArea.h"
FUNC(DyeEarthInPolygonArea)
{
// 	if (b)
// 	{
// 		DyeEarthInPolygonArea::AddShader(_mapNode.get());
// 	}
// 	else
// 	{
// 		DyeEarthInPolygonArea::RemoveShader(_mapNode.get());
// 	}
}
/************************************************************************/
/* 地球热力图
/* 
/************************************************************************/
//#include "EarthHeatMap/EarthHeatMap.h"
FUNC(EarthHeatMap)
{
// 	if (b)
// 	{
// 		QStringList items;
// 		items << QStringLiteral("平滑着色") 
// 			<< QStringLiteral("马赛克着色");
// 		bool ok;
// 		QString item = QInputDialog::getItem(0, QStringLiteral("选择着色模式"),QStringLiteral("着色方式:"), items, 0, false, &ok);
// 		if (ok==false)
// 		{
// 			return;
// 		}
// 		int id = items.indexOf(item);
// 		if (id==0)
// 		{
// 			EarthHeatMap::AddShaderSmooth(_mapNode.get());
// 		}
// 		else if(id==1)
// 		{
// 			EarthHeatMap::AddShaderMosaic(_mapNode.get(), 20);
// 		}
// 	}
// 	else
// 	{
// 		EarthHeatMap::RemoveShader(_mapNode.get());
// 	}
}
/************************************************************************/
/* 飞行航线
/*
/************************************************************************/
//#include "FlightPath/FlightPath.h"
FUNC(FlightPath)
{
// 	static FlightPath* fp = NULL;
// 	if (b)
// 	{
// 		fp = new FlightPath(_mapNode.get());
// 		_root->addChild(fp);
// 
// 		osgEarth::Viewpoint viewpoint("", 118.78875, 37.618, 15, 90, -45.0, 5.0e3);
// 		_earthManipulator->setViewpoint(viewpoint);
// 	}
// 	else
// 	{
// 		_root->removeChild(fp);
// 		_viewer->home();
// 	}
}
/************************************************************************/
/* 飞行路径
/************************************************************************/
//#include "FlyPath/FlyPath.h"
FUNC(FlyPath)
{
// 	static FlyPath* obj = NULL;
// 	if (b)
// 	{
// 		obj = new FlyPath(_viewer.get());
// 		_mapNode->addChild(obj);
// 	}
// 	else
// 	{
// 		_mapNode->removeChild(obj);
// 	}
}
/************************************************************************/
/* 星空
/* 将SimpleSkyNode独立出来，可以用于OSG场景
/* 为了便于修改shader代码，可以将相关的glsl代码替换StarSkyShaders.cpp相应位置，便于阅读修改
/* 如果在OSG场景中，可以这样使用
	//自动跟踪星群中心位置操纵器
	AutoLookAtStarSkyManipulator* manip = new AutoLookAtStarSkyManipulator();
	StarSkyOptions opt;
	opt.starSize() = 30;
	opt.starFile() = "data/stars.txt";
	auto starSky = new StarSkyNode(manip, opt);
	root->addChild(starSky);
	viewer.setSceneData(root);
	//注意setSceneData会调用操纵器home函数，所以设置操纵器放在之后
	viewer.setCameraManipulator(manip, false);
	//或者显示调用
	//starSky->updateAutoLookAtStarSkyManip();
/************************************************************************/
//#include "StarSky/StarSkyNode.h"
FUNC(StarSky)
{
// 	static StarSkyNode* obj = NULL;
// 	if (b)
// 	{
// 		StarSkyOptions opt;
// 		opt.starSize() = 30;
// 		//使用自定义的星星数据。如果使用oe默认的星空，注释掉下行即可
// 		opt.starFile() = "data/stars.txt";
// 		obj = new StarSkyNode(opt);
// 		//星空附加到view，可以设置setClearColor为黑色，当然也可以手动设置相机的clearColor，attach中有更多设置
// 		obj->attach(_viewer->asView(), 0);
// 		_root->addChild(obj);
// 
// 		//获取星群中心位置
// 		osg::Vec2d starsCenter = obj->getStarsCenter();
// 		double lon = osg::RadiansToDegrees(starsCenter.x());
// 		double lat = osg::RadiansToDegrees(starsCenter.y());
// 		//相机的位置应该为星群中心位置关于世界坐标原点的中心对称位置
// 		lon = lon - 180 * osg::sign(lon);
// 		lat = -lat;
// 		osgEarth::Viewpoint viewpoint("", lon, lat, 15, 0, -90.0, 1.0e8);
// 		_earthManipulator->setViewpoint(viewpoint);
// 	}
// 	else
// 	{
// 		_root->removeChild(obj);
// 	}
}
/************************************************************************/
/* 根据TLE数据生成卫星轨道
/************************************************************************/
#if _MSC_VER>=1900
//#include "TLE-Satellite/Satellite.h"
#endif
FUNC(TLESatellite)
{
#if _MSC_VER>=1900//VS2015
// 	static osg::Group* obj = NULL;
// 	if (b)
// 	{
// 		//卫星轨道TLE
// 		Satellite* satellite1 = new Satellite("UK-DMC 2",
// 			"1 35683U 09041C   12289.23158813  .00000484  00000-0  89219-4 0  5863",
// 			"2 35683  98.0221 185.3682 0001499 100.5295 259.6088 14.69819587172294");
// 		Satellite* satellite2 = new Satellite("negative around 1130 min",
// 			"1 28626U 05008A   06176.46683397 -.00000205  00000-0  10000-3 0  2190",
// 			"2 28626   0.0019 286.9433 0000335  13.7918  55.6504  1.00270176  4891");
// 		Satellite* satellite3 = new Satellite("Shows Lyddane choice at 1860 and 4700 min",
// 			"1 20413U 83020D   05363.79166667  .00000000  00000-0  00000+0 0  7041",
// 			"2 20413  12.3514 187.4253 7864447 196.3027 356.5478  0.24690082  7978");
// 		Satellite* satellite4 = new Satellite("BEIDOU-3 M8",
// 			"1 43246U 18029B   18194.72004803 -.00000064  00000-0  10000-3 0  9991",
// 			"2 43246  55.0642  39.6410 0004409 332.7876  76.6516  1.86232943  1964");
// 		Satellite* satellite5 = new Satellite("TIANLIAN 1 - 01",
// 			"1 32779U 08019A   20351.10840635 -.00000141  00000-0  00000-0 0  9991",
// 			"2 32779   4.5189  71.7819 0032076 257.7478 234.6613  1.00271096 46278");
// 		Satellite* satellite6 = new Satellite("TIANLIAN 1 - 02",
// 			"1 37737U 11032A   20351.77261799 -.00000019  00000-0  00000+0 0  9993",
// 			"2 37737   2.7338  75.1948 0015143 117.0676 342.6731  1.00272971 34583");
// 		Satellite* satellite7 = new Satellite("TIANLIAN 1 - 03",
// 			"1 38730U 12040A   20351.81440405  .00000125  00000-0  00000+0 0  9992",
// 			"2 38730   0.6606  99.7464 0004480 246.9705  52.7806  1.00272396 30853");
// 		Satellite* satellite8 = new Satellite("TIANLIAN 1 - 04",
// 			"1 41869U 16072A   20351.59278216  .00000097  00000-0  00000-0 0  9991",
// 			"2 41869   0.2182  94.0127 0010711   0.0140  18.7747  1.01323309 15171");
// 		Satellite* satellite9 = new Satellite("TIANLIAN 1 - 01",
// 			"1 44076U 19017A   20351.53909088 -.00000135  00000-0  00000-0 0  9992",
// 			"2 44076   1.5743 284.6749 0030671 222.1555 212.9147  1.00271758  6476");
// 		obj = new osg::Group();
// 		obj->addChild(satellite1);
// 		obj->addChild(satellite2);
// 		obj->addChild(satellite3);
// 		obj->addChild(satellite4);
// 		obj->addChild(satellite5);
// 		obj->addChild(satellite6);
// 		obj->addChild(satellite7);
// 		obj->addChild(satellite8);
// 		obj->addChild(satellite9);
// 		_root->addChild(obj);
// 	}
// 	else
// 	{
// 		_root->removeChild(obj);
// 	}
#endif
}
/************************************************************************/
/* 太阳系
/* 注意这里的太阳在世界坐标系的原点
/************************************************************************/
#if _MSC_VER>=1900
//#include "SolarSystem/SolarSystem.h"
#endif
FUNC(SolarSystem)
{
#if _MSC_VER>=1900//VS2015
// 	static SolarSystem* obj = NULL;
// 	if (b)
// 	{
// 		obj = new SolarSystem();
// 		obj->setMatrix(osg::Matrix::scale(10e7,10e7,10e7));
// 		_root->addChild(obj);
// 	}
// 	else
// 	{
// 		_root->removeChild(obj);
// 	}
#endif
}
/************************************************************************/
/* 地表飞行
/************************************************************************/
//#include "AnimationPathOnEarth/AnimationPathOnEarth.h"
FUNC(AnimationPathOnEarth)
{
// 	static AnimationPathOnEarth* obj = NULL;
// 	if (b)
// 	{
// 		osg::Vec3 startLLA(116.393714, 39.9184, 5000);
// 		osg::Vec3 endLLA(120.3, 31.2, 1000);
// 		obj = new AnimationPathOnEarth(_mapNode.get(), startLLA, endLLA, _cessna);
// 		
// 		_root->addChild(obj);
// 
// 		osgEarth::Viewpoint vp;
// 		vp.setNode(obj);
// 		vp.range()=10e5;
// 		//vp.setHeading(0);
// 		//vp.setPitch(-90);
// 		//设置绑定节点模式
// 		//_earthManipulator->getSettings()->setTetherMode(osgEarth::Util::EarthManipulator::TETHER_CENTER);
// 		_earthManipulator->setViewpoint(vp);
// 	}
// 	else
// 	{
// 		_root->removeChild(obj);
// 		//解除视点绑定
// 		_earthManipulator->setTetherCallback(NULL);
// 	}
}