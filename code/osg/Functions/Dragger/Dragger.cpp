#include "Dragger.h"
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/Point>
#include <osg/LineWidth>
#include <osg/Shape>
#include <osg/ShapeDrawable>
#include "osgText/Text"
#include "osg/AutoTransform"
#include "osg/PositionAttitudeTransform"
#include "3rd/ToolKitFunctionClass/ToolKitFunctionClass.h"
#include "osgDB/ReadFile"
#include "XYZDragger.h"

SimpleDragger::SimpleDragger(osg::Node* model)
{
	auto mt = new osg::MatrixTransform;
	mt->addChild(model);
	addChild(mt);

	auto xyzDragger = new XYZDragger;// osgManipulator::TranslateAxisDragger();
	xyzDragger->setupDefaultGeometry();
	xyzDragger->setAxisLineWidth(5.0f);
	xyzDragger->setPickCylinderRadius(0.05f);
	xyzDragger->setConeHeight(0.2f);
	// 保持dragger在屏幕上大小不变
	DraggerContainer* draggerContainer = new DraggerContainer;
	draggerContainer->setDragger(xyzDragger);
	draggerContainer->setDraggerSize(300);
	addChild(draggerContainer);

	// 设置拖拽器影响的变换节点
	xyzDragger->addTransformUpdating(mt);
	xyzDragger->addDraggerCallback(new UpdateCtrlPntDraggerCallback(this));

	// we want the dragger to handle it's own events automatically
	xyzDragger->setHandleEvents(true);

	// if we don't set an activation key or mod mask then any mouse click on
	// the dragger will activate it, however if do define either of ActivationModKeyMask or
	// and ActivationKeyEvent then you'll have to press either than mod key or the specified key to
	// be able to activate the dragger when you mouse click on it.  Please note the follow allows
	// activation if either the ctrl key or the 'a' key is pressed and held down.
	//xyzDragger->setActivationModKeyMask(osgGA::GUIEventAdapter::MODKEY_CTRL);
	//xyzDragger->setActivationKeyEvent('a');
}

SimpleDragger::DraggerContainer::DraggerContainer() : _draggerSize(240.0f), _active(true)
{

}

void SimpleDragger::DraggerContainer::setDragger(osgManipulator::Dragger* dragger)
{
	_dragger = dragger;
	if (!containsNode(dragger)) addChild(dragger);
}

void SimpleDragger::DraggerContainer::traverse(osg::NodeVisitor& nv)
{
	if (_dragger.valid())
	{
		if (_active && nv.getVisitorType() == osg::NodeVisitor::CULL_VISITOR)
		{
			osgUtil::CullVisitor* cv = static_cast<osgUtil::CullVisitor*>(&nv);

			float pixelSize = cv->pixelSize(_dragger->getBound().center(), 0.48f);
			if (pixelSize != _draggerSize)
			{
				float pixelScale = pixelSize > 0.0f ? _draggerSize / pixelSize : 1.0f;
				osg::Vec3d scaleFactor(pixelScale, pixelScale, pixelScale);

				osg::Vec3 trans = _dragger->getMatrix().getTrans();
				_dragger->setMatrix(osg::Matrix::scale(scaleFactor) * osg::Matrix::translate(trans));
			}
		}
	}
	osg::Group::traverse(nv);
}

SimpleDragger::UpdateCtrlPntDraggerCallback::UpdateCtrlPntDraggerCallback(SimpleDragger* dragger) :
	_dragger(dragger)
{

}

bool SimpleDragger::UpdateCtrlPntDraggerCallback::receive(const osgManipulator::MotionCommand& command)
{
	switch (command.getStage())
	{
	case osgManipulator::MotionCommand::START:
	{
		return true;
	}
	case osgManipulator::MotionCommand::MOVE:
	{
		//_dragger->doSth();
		return true;
	}
	case osgManipulator::MotionCommand::FINISH:
	{
		return true;
	}
	case osgManipulator::MotionCommand::NONE:
	default:
		return false;
	}
}
