#include <osg/Version>

const char* osgGetVersion()
{
    return "0.9.0";
}


const char* osgGetLibraryName()
{
    return "Open Scene Graph Library";
}
