#include <osgGA/Version>

const char* osgGAGetVersion()
{
    return "0.9.0";
}


const char* osgGAGetLibraryName()
{
    return "Open Scene Graph Gui Adapter Library";
}
