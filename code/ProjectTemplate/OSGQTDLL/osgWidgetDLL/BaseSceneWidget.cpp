#include "BaseSceneWidget.h"
#include "osgViewer\ViewerEventHandlers"
#include "osgGA\TrackballManipulator"
#include "osg\ShapeDrawable"
#include "osgGA\StateSetManipulator"

BaseSceneWidget::BaseSceneWidget(QWidget* parent /*= 0*/, Qt::WindowFlags f /*= 0*/, osgViewer::ViewerBase::ThreadingModel threadingModel /*= osgViewer::Viewer::SingleThreaded*/) : QWidget(parent, f)
{
	_viewer = new osgViewer::Viewer();
	// disable the default setting of viewer.done() by pressing Escape.
	_viewer->setKeyEventSetsDone(0);
	//如果不设置线程模型而让OSG自己选择的话，大多数情况下不会选择SingleThreaded单线程（现在单核CPU不多见了）
	//会出现“Cannot make QOpenGLContext current in a different thread”然后闪退
	_viewer->setThreadingModel(osgViewer::ViewerBase::SingleThreaded);

	initScene();

	GraphicsWindowQt* gw = createGraphicsWindow(0, 0, 100, 100);
	osg::Camera* camera = _viewer->getCamera();
	camera->setGraphicsContext(gw);
	const osg::GraphicsContext::Traits* traits = gw->getTraits();
	//camera->setClearColor(osg::Vec4(0.2, 0.2, 0.2, 1.0));
	camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
	camera->setProjectionMatrixAsPerspective(45.0f, static_cast<double>(traits->width) / static_cast<double>(traits->height), 1.0f, 10000.0f);

	_viewer->frame();
	if (gw)
	{
		QVBoxLayout* layout = new QVBoxLayout;
		layout->addWidget(gw->getGLWidget());
		layout->setContentsMargins(0, 0, 0, 1);
		setLayout(layout);

		// Send window size event for initializing
		int x, y, w, h;
		gw->getWindowRectangle(x, y, w, h);
		_viewer->getEventQueue()->windowResize(x, y, w, h);
	}

	connect(&_timer, SIGNAL(timeout()), this, SLOT(update()));
	_timer.start(10);

	//如果发现帧率过低，可以去NVIDIA控制面板检查一下
	//首选图形处理器是否选择了“高性能NVIDIA处理器”
	//或者尝试startTimer(1000.0 / 60, Qt::PreciseTimer);
}

GraphicsWindowQt* BaseSceneWidget::createGraphicsWindow(int x, int y, int w, int h, const std::string& name /*= ""*/, bool windowDecoration /*= false*/)
{
	osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
	osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
	traits->windowName = name;
	traits->windowDecoration = windowDecoration;
	traits->x = x;
	traits->y = y;
	traits->width = w;
	traits->height = h;
	traits->doubleBuffer = true;
	traits->alpha = ds->getMinimumNumAlphaBits();
	traits->stencil = ds->getMinimumNumStencilBits();
	traits->sampleBuffers = ds->getMultiSamples();
	traits->samples = 4;// ds->getNumMultiSamples();//抗锯齿
	return new GraphicsWindowQt(traits.get());
}

void BaseSceneWidget::paintEvent(QPaintEvent* event)
{
	_viewer->frame();
}

osgViewer::Viewer* BaseSceneWidget::getViewer()
{
	return _viewer;
}

osg::Group* BaseSceneWidget::getRoot()
{
	return _root;
}

void BaseSceneWidget::initScene()
{
	_root = new osg::Group;

	_viewer->setSceneData(_root);
	_viewer->setCameraManipulator(new osgGA::TrackballManipulator());

	// add the state manipulator
	_viewer->addEventHandler(new osgGA::StateSetManipulator(_viewer->getCamera()->getOrCreateStateSet()));
	// add the thread model handler
	_viewer->addEventHandler(new osgViewer::ThreadingHandler);
	// add the window size toggle handler
	_viewer->addEventHandler(new osgViewer::WindowSizeHandler);
	// add the stats handler
	_viewer->addEventHandler(new osgViewer::StatsHandler);
	// add the help handler
	_viewer->addEventHandler(new osgViewer::HelpHandler);
	// add the record camera path handler
	_viewer->addEventHandler(new osgViewer::RecordCameraPathHandler);
	// add the LOD Scale handler
	_viewer->addEventHandler(new osgViewer::LODScaleHandler);
	// add the screen capture handler
	_viewer->addEventHandler(new osgViewer::ScreenCaptureHandler);
}