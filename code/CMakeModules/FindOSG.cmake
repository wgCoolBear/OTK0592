
SET(OSG_DIR "" CACHE PATH "Set to base OpenSceneGraph install path")

FIND_PATH(OSG_INCLUDE_DIR osg/Referenced
    PATHS
	${OSG_DIR}/include
    $ENV{OSG_ROOT}/include
    $ENV{OSG_DIR}/include
    /usr/include
    /usr/local/include
)
FIND_PATH(OSG_LIB_DIR libosg.so osg.lib
    PATHS
	${OSG_DIR}/lib
    $ENV{OSG_ROOT}/lib
    $ENV{OSG_DIR}/lib
    /usr/lib
    /usr/local/lib
)
SET( OSG_FOUND "NO" )
IF( OSG_INCLUDE_DIR AND OSG_LIB_DIR )
    SET( OSG_FOUND "YES" )
ENDIF( OSG_INCLUDE_DIR AND OSG_LIB_DIR )