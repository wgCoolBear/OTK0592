#include <osgViewer/Viewer>  
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>
#include <osgGA/TrackballManipulator>
#include <osgDB/ReadFile> 
#include <osgEarth/Version>
#include <osgEarth/ModelLayer>
#include <osgEarth/GLUtils>
#include <osgEarth/Map>
#include <osgEarth/MapNode>
#include <osgEarth/Terrain>
#include <osgEarth/GeoTransform>
#include <osgEarth/TerrainEngineNode>
#include <osgEarth/Registry>
#include <osgEarth/ShaderGenerator>
#include <osgEarthAnnotation/ModelNode>
#include <osgEarthUtil/AutoClipPlaneHandler>
#include <osgEarthUtil/EarthManipulator>
#include <osgEarthUtil/Sky>
#include <osgEarthUtil/Shadowing>
#include <osgEarthUtil/Sky>
#include <osgEarthUtil/EarthManipulator>
#include <osgEarthUtil/GeodeticGraticule>
#include <osgEarthDrivers/model_simple/SimpleModelOptions>
#include <osgEarthDrivers/model_feature_geom/FeatureGeomModelOptions>
#include <osgEarthDrivers/agglite/AGGLiteOptions>
#include <osgEarthDrivers/gdal/GDALOptions>
#include <osgEarthDrivers/feature_ogr/OGRFeatureOptions>
#include <osgEarthDrivers/tms/TMSOptions>
#include <osgEarthDrivers/sky_simple/SimpleSkyOptions>
#if OSGEARTH_VERSION_GREATER_OR_EQUAL(2,10,0)
#include <osgEarthFeatures/FeatureModelLayer>
#include <osgEarthFeatures/ConvertTypeFilter>
#endif
int main()
{
	//抗锯齿
	osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
	ds->setNumMultiSamples(8);

	osgViewer::Viewer viewer;
	osg::Group* root = new osg::Group;
	// 场景图构成
	//Root
	//		-SkyNode
	//			-ShadowCaster
	//			   -MapNode
	//					-ImageLayer
	//					-ElevationLayer
	//					-ModelLayer

#define USE_EARTH_FILE 0
#if(USE_EARTH_FILE)
	// 从.earth文件加载数据
	osg::Node* configEarth = osgDB::readNodeFile("../../../data/project/gdal_tiff.earth");
	osgEarth::MapNode* mapNode = osgEarth::MapNode::findMapNode(configEarth);
	osgEarth::Map* map = mapNode->getMap();
#else
	osgEarth::Map* map = new osgEarth::Map();
	osgEarth::MapNodeOptions mapNodeOptions;
	mapNodeOptions.enableLighting() = false;
	osgEarth::MapNode* mapNode = new osgEarth::MapNode(map, mapNodeOptions);	
	
	// 影像、高程
	osgEarth::Drivers::GDALOptions basemapOpt;//GDAL
	basemapOpt.url() = "../../../data/image/world.tif";
	map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("basemap", basemapOpt)));

	//osgEarth::Drivers::TMSOptions baseImg, baseEle;//TMS
	//baseImg.url() = "../tms/readymap/ReadyMapImagery/tms.xml";//本地数据
	//baseImg.url() = "http://readymap.org/readymap/tiles/1.0.0/7/";//网络数据
	//map->addImageLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("baseImg", baseImg)));

	//baseEle.url() = "http://readymap.org/readymap/tiles/1.0.0/9/";
	//map->addElevationLayer(new osgEarth::ElevationLayer(osgEarth::ElevationLayerOptions("baseEle", baseEle)));

#endif // USE_EARTH_FILE

#if 1
	//光照
	//mapNode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	// 添加3D模型(4种方式)
	// 第一种使用GeoTransform。注意这种方式加载模型，要么在模型名字后面添加.osgearth_shadergen；要么使用
	// osgEarth::Registry::shaderGenerator()。否则使用shader的模型将不能正确显示。
	osg::ref_ptr<osg::Node> model3D = osgDB::readNodeFile("../../../data/model/cow.osgb.10.scale.osgearth_shadergen");
	//osgEarth::Registry::shaderGenerator().run(model3D);
	osgEarth::GeoTransform* geoTransform = new osgEarth::GeoTransform;
	//geoTransform->setTerrain(mapNode->getTerrain());//自动贴地
	geoTransform->addChild(model3D);
	geoTransform->setPosition(osgEarth::GeoPoint(mapNode->getMapSRS(), osg::Vec3d(116.392714, 39.9174, 30)));
	map->addLayer(new osgEarth::ModelLayer("3Dmodel layer GeoTransform", geoTransform));
	// 第二种使用SimpleModelOptions。这种方式不用考虑模型自身的shader问题。
	// 但是这种方式精度不够，因为modelOptions.location()是float精度，不推荐
	osgEarth::Drivers::SimpleModelOptions modelOptions;
	modelOptions.url() = "../../../data/model/cessna.osgb.5.scale";
	modelOptions.location() = osg::Vec3(116.392714, 39.9184, 50);
	map->addLayer(new osgEarth::ModelLayer("3Dmodel layer SimpleModelOptions", modelOptions));
	// 或者通过.earth文件配置
	/*
	<model name = "model" driver = "simple">
		<url>glider.osgt.100, 100, 100.scale< / url>
		<location>116.393714 39.9184 - 500 < / location >
	< / model>
	*/
	// 第三种通过osgEarth::Annotation::ModelNode，推荐
	// 需要注意的是这种方式意味着模型是一个Annotation，是地球的一部分，不能产生阴影
	osgEarth::Symbology::Style modelStyle;
	//modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->setModel(model3D);
	modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->url() = std::string("../../../data/model/redflag.osgb.100.scale");
	auto modelNode = new osgEarth::Annotation::ModelNode(mapNode, modelStyle);
	modelNode->setPosition(osgEarth::GeoPoint(mapNode->getMapSRS(), 116.3932, 39.918, 0));
	mapNode->addChild(modelNode);
	//或者通过.earth文件配置
	/*<annotations>
		<model name = "Auto-Scaled Model">
			<position lat = "43" long = "-100" / >
			<style>
				model: "data/model/red_flag.osg.45.scale";
				model-scale: auto;
			</style>
		</model>
	</annotations>
	*/
	//第四种直接使用osg方式，和osgearth的mapNode无关
	osg::Matrix worldMatrix;
	osgEarth::GeoPoint(mapNode->getMapSRS(), osg::Vec3(120.53, 37.82, 100)).createLocalToWorld(worldMatrix);
	osg::MatrixTransform* mt = new osg::MatrixTransform();
	mt->setMatrix(worldMatrix);
	mt->addChild(osgDB::readNodeFile("../../../data/model/cow.osgb.10000.scale"));
	root->addChild(mt);

	// 矢量
	osgEarth::Drivers::OGRFeatureOptions featureOptions;
	//featureOptions.url() = "../../../data/shp/World/world.shp";
	featureOptions.url() = "../../../data/shp/中华人民共和国/中华人民共和国.shp";
#if OSGEARTH_VERSION_GREATER_OR_EQUAL(2,10,0)
	// Make a feature source layer and add it to the Map:
	osgEarth::Features::FeatureSourceLayerOptions ogrLayer;
	ogrLayer.name() = "vector-data";
	ogrLayer.featureSource() = featureOptions;
	_map->addLayer(new osgEarth::Features::FeatureSourceLayer(ogrLayer));
	bool useDraping = true;//贴地
	bool useClamping = false;
	bool useRaster = false;
	// Define a style for the feature data. Since we are going to render the
	// vectors as lines, configure the line symbolizer:
	osgEarth::Symbology::Style style;
	osgEarth::Symbology::LineSymbol* ls = style.getOrCreateSymbol<osgEarth::Symbology::LineSymbol>();
	ls->stroke()->color() = osgEarth::Symbology::Color::Yellow;
	ls->stroke()->width() = 2.0f;
	ls->tessellationSize()->set(100, osgEarth::Symbology::Units::KILOMETERS);
	if (useDraping)
	{
		osgEarth::Symbology::AltitudeSymbol* alt = style.getOrCreate<osgEarth::Symbology::AltitudeSymbol>();
		alt->clamping() = alt->CLAMP_TO_TERRAIN;
		alt->technique() = alt->TECHNIQUE_DRAPE;
	}
	else if (useClamping)
	{
		osgEarth::Symbology::AltitudeSymbol* alt = style.getOrCreate<osgEarth::Symbology::AltitudeSymbol>();
		alt->clamping() = alt->CLAMP_TO_TERRAIN;
		alt->technique() = alt->TECHNIQUE_GPU;
		ls->tessellationSize()->set(100, osgEarth::Symbology::Units::KILOMETERS);
		osgEarth::Symbology::RenderSymbol* render = style.getOrCreate<osgEarth::Symbology::RenderSymbol>();
		render->depthOffset()->enabled() = true;
	}
	if (useRaster)
	{
		osgEarth::Drivers::AGGLiteOptions rasterOptions;
		rasterOptions.featureOptions() = featureOptions;
		rasterOptions.styles() = new osgEarth::Symbology::StyleSheet();
		rasterOptions.styles()->addStyle(style);
		_map->addLayer(new osgEarth::ImageLayer("My Features", rasterOptions));
	}
	else //if (useGeom)
	{
		//省界线
		osgEarth::Drivers::FeatureModelLayerOptions fml;
		fml.name() = "My Features";
		fml.featureSourceLayer() = "vector-data";
		fml.styles() = new osgEarth::Symbology::StyleSheet();
		fml.styles()->addStyle(style);
		fml.enableLighting() = false;
		_map->addLayer(new osgEarth::Drivers::FeatureModelLayer(fml));
		if (true)//显示文字
		{
			// set up symbology for drawing labels. We're pulling the label
			// text from the name attribute, and its draw priority from the
			// population attribute.
			osgEarth::Symbology::Style labelStyle;
			osgEarth::Symbology::TextSymbol* text = labelStyle.getOrCreateSymbol<osgEarth::Symbology::TextSymbol>();
			//world.shp
			//text->content() = osgEarth::Symbology::StringExpression("[cntry_name]");
			//text->priority() = osgEarth::Symbology::NumericExpression("[pop_cntry]");
			//中华人民共和国.shp
			text->content() = osgEarth::Symbology::StringExpression("[name]");
			//处理中文
			text->encoding() = osgEarth::Symbology::TextSymbol::ENCODING_UTF8;
			text->font() = "simsun.ttc";
			text->size() = 16.0f;
			text->alignment() = osgEarth::Symbology::TextSymbol::ALIGN_CENTER_CENTER;
			text->fill()->color() = osgEarth::Symbology::Color::White;
			text->halo()->color() = osgEarth::Symbology::Color::DarkGray;
			// and configure a model layer:
			osgEarth::Drivers::FeatureModelLayerOptions fml;
			fml.name() = "Labels";
			fml.featureSourceLayer() = "vector-data";
			fml.styles() = new osgEarth::Symbology::StyleSheet();
			fml.styles()->addStyle(labelStyle);
			_map->addLayer(new osgEarth::Drivers::FeatureModelLayer(fml));
		}
	}
#else
	osgEarth::Symbology::Style style;
	osgEarth::Symbology::LineSymbol* ls = style.getOrCreateSymbol<osgEarth::Features::LineSymbol>();
	ls->stroke()->color() = osgEarth::Symbology::Color(0.5, 0, 1, 1);//osgEarth::Symbology::Color::Red
	ls->stroke()->width() = 2.0f;
	//ls->stroke()->lineCap() = osgEarth::Symbology::Stroke::LINECAP_ROUND;//使连接处圆滑一些
	osgEarth::Symbology::AltitudeSymbol* as = style.getOrCreateSymbol<osgEarth::Symbology::AltitudeSymbol>();
	//as->verticalOffset() = 10;//距离地面高度
	//as->clamping() = osgEarth::Symbology::AltitudeSymbol::CLAMP_TO_TERRAIN;
	//as->technique() = osgEarth::Symbology::AltitudeSymbol::TECHNIQUE_GPU;//TECHNIQUE_DRAPE,TECHNIQUE_MAP
	//as->binding() = AltitudeSymbol::BINDING_CENTROID;
	//as->clampingResolution() = 0.005;
	if (false)
	{
		//附着在地面，速度慢
		osgEarth::Drivers::AGGLiteOptions rasterOptions;
		rasterOptions.featureOptions() = featureOptions;
		rasterOptions.styles() = new osgEarth::Symbology::StyleSheet();
		rasterOptions.styles()->addStyle(style);
		map->addLayer(new osgEarth::ImageLayer("AGGLite", rasterOptions));
	}
	else
	{
		//Geometry方式绘制shp，速度快，会有穿地
		osgEarth::Drivers::FeatureGeomModelOptions geomOptions;
		geomOptions.featureOptions() = featureOptions;
		geomOptions.styles() = new osgEarth::Symbology::StyleSheet();
		geomOptions.styles()->addStyle(style);
		geomOptions.enableLighting() = false;
		//geomOptions.minRange() = 0;
		//geomOptions.maxRange() = 1e7;
		map->addLayer(new osgEarth::ModelLayer("", geomOptions));
	}
#endif

	root->addChild(mapNode);
#if OSGEARTH_VERSION_LESS_THAN(2,10,0)
	// 天空
	osgEarth::Util::SkyNode *skyNode = osgEarth::Util::SkyNode::create(mapNode);
	skyNode->setDateTime(osgEarth::DateTime(2018, 12, 24, 4));
	skyNode->setMinimumAmbient(osg::Vec4(0.1,0.1,0.1,0.5));//黑夜区的颜色
	skyNode->attach(&viewer, 0);
	osgEarth::insertParent(skyNode, mapNode);
	// 阴影
	osgEarth::Util::ShadowCaster* caster = new osgEarth::Util::ShadowCaster();
	caster->setLight(skyNode->getSunLight());
	caster->getShadowCastingGroup()->addChild(mapNode->getLayerNodeGroup());
	caster->getShadowCastingGroup()->addChild(mapNode->getTerrainEngine());
	if (mapNode->getNumParents() > 0)
		osgEarth::insertGroup(caster, mapNode->getParent(0));
	else
	{
		caster->addChild(mapNode);
		root->addChild(caster);
	}
#else
	// 天空
	osgEarth::SimpleSky::SimpleSkyOptions opt;
	opt.hours() = 4;
	opt.ambient() = 0.1;
	osgEarth::Extension* e = osgEarth::Extension::create("sky_simple", opt);
	mapNode->addExtension(e);
	// Check for a View interface:
	osgEarth::ExtensionInterface<osg::View>* viewIF = osgEarth::ExtensionInterface<osg::View>::get(e);
	if (viewIF)
		viewIF->connect(&viewer);
	// 阴影
	int unit;
	if (mapNode->getTerrainEngine()->getResources()->reserveTextureImageUnit(unit, "ShadowCaster"))
	{
		osgEarth::Util::ShadowCaster* caster = new osgEarth::Util::ShadowCaster();
		caster->setTextureImageUnit(unit);
		caster->setLight(viewer.getLight());
		caster->getShadowCastingGroup()->addChild(mapNode->getLayerNodeGroup());
		caster->getShadowCastingGroup()->addChild(mapNode->getTerrainEngine());
		if (mapNode->getNumParents() > 0)
		{
			osgEarth::insertGroup(caster, mapNode->getParent(0));
		}
		else
		{
			caster->addChild(mapNode);
			root->addChild(caster);
		}
	}
#endif
#else
	root->addChild(mapNode);
#endif
	
	// 近地面自动裁剪，这样可使地面的模型不被裁剪
	viewer.getCamera()->addCullCallback(new osgEarth::Util::AutoClipPlaneCullCallback(mapNode));
	// 绘制经纬度网格
	osgEarth::Util::GeodeticGraticule* gr = new osgEarth::Util::GeodeticGraticule();
	//mapNode->getMap()->addLayer(gr);

	//----------------------在OE2.10中，需要添加以下两行，否则线或者点可能会看不见------------
	// default uniform values:
	osgEarth::GLUtils::setGlobalDefaults(viewer.getCamera()->getOrCreateStateSet());
	// disable the small-feature culling
	viewer.getCamera()->setSmallFeatureCullingPixelSize(-1.0f);

	viewer.setSceneData(root);
	// add some stock OSG handlers:
	viewer.addEventHandler(new osgViewer::StatsHandler());
	viewer.addEventHandler(new osgViewer::WindowSizeHandler());
	viewer.addEventHandler(new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()));
	//viewer.setUpViewInWindow(100, 500, 600, 400);

	osgEarth::Util::EarthManipulator* em = new osgEarth::Util::EarthManipulator();
	viewer.setCameraManipulator(em);

	viewer.realize();
	
	//初始化视点位置，要在viewer.realize()之后
	osgEarth::Viewpoint vp("", 116.393714, 39.9184, 15, -2.50, -60.0, 1e3);
	//em->setViewpoint(vp);
	//em->setHomeViewpoint(vp);

	return viewer.run();
}