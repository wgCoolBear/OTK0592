#pragma once
#include "Export.h"
#include <osg/Group>
#include "osgManipulator/Dragger"
//拖拽器
class DLL_EXPORT SimpleDragger:public osg::Group
{
public:
	SimpleDragger(osg::Node* model);

	// 拖拽器容器，可以控制拖拽器在屏幕上的大小固定
	class DraggerContainer : public osg::Group
	{
	public:
		DraggerContainer();

		void setDragger(osgManipulator::Dragger* dragger);

		void setDraggerSize(float size) { _draggerSize = size; }
		float getDraggerSize() const { return _draggerSize; }

		void traverse(osg::NodeVisitor& nv);

	protected:
		osg::ref_ptr<osgManipulator::Dragger> _dragger;
		float _draggerSize;
		bool _active;
	};

	// 当拖拽器移动控制点的时候，调用此回调
	class UpdateCtrlPntDraggerCallback : public osgManipulator::DraggerCallback
	{
	public:
		UpdateCtrlPntDraggerCallback(SimpleDragger* dragger);
		virtual bool receive(const osgManipulator::MotionCommand& command);

	protected:
		osg::ref_ptr<SimpleDragger> _dragger;
	};
};

