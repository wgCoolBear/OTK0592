
SET(OSGEARTH_DIR "" CACHE PATH "Set to base osgEarth install path")

FIND_PATH(OSGEARTH_INCLUDE_DIR osgEarth/Version
    PATHS
	${OSGEARTH_DIR}/include
    $ENV{OSGEARTH_ROOT}/include
    $ENV{OSGEARTH_DIR}/include
    /usr/include
    /usr/local/include
)
FIND_PATH(OSGEARTH_LIB_DIR osgEarth.lib
    PATHS
	${OSGEARTH_DIR}/lib
    $ENV{OSGEARTH_ROOT}/lib
    $ENV{OSGEARTH_DIR}/lib
    /usr/lib
    /usr/local/lib
)
SET( OSGEARTH_FOUND "NO" )
IF( OSGEARTH_LIB_DIR AND OSGEARTH_INCLUDE_DIR )
    SET( OSGEARTH_FOUND "YES" )
ENDIF( OSGEARTH_LIB_DIR AND OSGEARTH_INCLUDE_DIR )
