#include "TrackNode.h"
#include <osgEarth/GeoTransform>
#include "osg/AnimationPath"

#define SRS osgEarth::SpatialReference::get("wgs84")

TrackNode::TrackNode(osgViewer::Viewer* viewer, osg::ref_ptr<osg::Node> trackedNode)
{
	osg::AnimationPath* animationPath = new osg::AnimationPath;
	animationPath->setLoopMode(osg::AnimationPath::SWING);

	float yaw = 0.0f;
	float roll = osg::inDegrees(30.0f);
	float pitch=0.0f;
	//构造测试数据
	for (int i=0;i<10;i++)
	{
		osg::Vec3 lla(-i*10, i*5, 4000000);
		osgEarth::GeoPoint gp(SRS, lla);
		osg::Matrixd worldMat;
		gp.createLocalToWorld(worldMat);
		osg::Vec3d position=worldMat.getTrans();
		osg::Quat rotation=worldMat.getRotate();
		animationPath->insert(i*10,osg::AnimationPath::ControlPoint(position));
	}
	//使用AnimationPathCallback使模型沿着测试路径飞行
	this->addUpdateCallback(new osg::AnimationPathCallback(animationPath, 0.0, 1.0));
	
	this->addChild(trackedNode);

	osgEarth::Viewpoint vp;
	vp.setNode(this);
	//如果不绑定节点，也可以指定位置
	//vp.focalPoint()=osgEarth::GeoPoint(SRS,114,40,100000);
	vp.range()=10e5;
	//vp.setHeading(0);
	//vp.setPitch(-90);
	osg::ref_ptr<osgEarth::Util::EarthManipulator> em = new osgEarth::Util::EarthManipulator();
	viewer->setCameraManipulator(em);
	//设置绑定节点模式
	//em->getSettings()->setTetherMode(osgEarth::Util::EarthManipulator::TETHER_CENTER);
	em->setViewpoint(vp);
	
	//获取当前viewpoint
	//osgEarth::Viewpoint currentVP=em->getViewpoint();
	//解除绑定节点
	//em->clearViewpoint();
	
	//修改EM默认控制。左键控制旋转，中键控制平移，右键控制缩放
	osg::ref_ptr<osgEarth::Util::EarthManipulator::Settings> settings = em->getSettings();
	settings->bindMouse(osgEarth::Util::EarthManipulator::ACTION_ROTATE,
		osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON);
	settings->bindMouse(osgEarth::Util::EarthManipulator::ACTION_PAN,
		osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON);
	//禁用方向键平移
	settings->bindKey(osgEarth::Util::EarthManipulator::ACTION_NULL, osgGA::GUIEventAdapter::KEY_Up);
	settings->bindKey(osgEarth::Util::EarthManipulator::ACTION_NULL, osgGA::GUIEventAdapter::KEY_Down);
	settings->bindKey(osgEarth::Util::EarthManipulator::ACTION_NULL, osgGA::GUIEventAdapter::KEY_Left);
	settings->bindKey(osgEarth::Util::EarthManipulator::ACTION_NULL, osgGA::GUIEventAdapter::KEY_Right);
	em->applySettings(settings);
}