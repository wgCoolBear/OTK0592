#include "MousePick.h"

bool MousePick::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	osgViewer::Viewer* viewer = dynamic_cast<osgViewer::Viewer*>(&aa);
	if (!viewer)return false;
	osg::Node* scene = viewer->getSceneData();
	if (!scene) return false;

	switch (ea.getEventType())
	{
	//鼠标
	case osgGA::GUIEventAdapter::PUSH:
		if (ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON)
		{
			
			osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
			if (view)
			{
				std::cout << "使用view->computeIntersections拾取结果:";
				pick_method1(view, ea);
			}
			std::cout << "\n使用osgUtil::IntersectionVisitor拾取结果:";
			pick_method2(viewer, ea);
		}
		break;
	default:
		break;
	}
	return false;
}

void MousePick::pick_method1(osgViewer::View* view, const osgGA::GUIEventAdapter& ea)
{
	osgUtil::LineSegmentIntersector::Intersections intersections;

	std::string gdlist = "";

	if (view->computeIntersections(ea, intersections))
	{
		for (osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
			hitr != intersections.end();
			++hitr)
		{
			std::ostringstream os;
			if (!hitr->nodePath.empty() && !(hitr->nodePath.back()->getName().empty()))
			{
				// the geodes are identified by name.
				os << "Object \"" << hitr->nodePath.back()->getName() << "\"" << std::endl;
			}
			else if (hitr->drawable.valid())
			{
				os << "Object \"" << hitr->drawable->className() << "\"" << std::endl;
			}

			os << "        local coords vertex(" << hitr->getLocalIntersectPoint() << ")" << "  normal(" << hitr->getLocalIntersectNormal() << ")" << std::endl;
			os << "        world coords vertex(" << hitr->getWorldIntersectPoint() << ")" << "  normal(" << hitr->getWorldIntersectNormal() << ")" << std::endl;
			const osgUtil::LineSegmentIntersector::Intersection::IndexList& vil = hitr->indexList;
			for (unsigned int i = 0; i < vil.size(); ++i)
			{
				os << "        vertex indices [" << i << "] = " << vil[i] << std::endl;
			}

			gdlist += os.str();
		}
		std::cout << gdlist << std::endl;
	}
}

void MousePick::pick_method2(osgViewer::Viewer* viewer, const osgGA::GUIEventAdapter& ea)
{
	osg::Node* scene = viewer->getSceneData();
	if (!scene) return;

	osg::notify(osg::NOTICE) << std::endl;

	osg::Node* node = 0;
	osg::Group* parent = 0;
	bool usePolytopeIntersector = false;//是否使用多面体求交
	bool useWindowCoordinates = false;//是否在窗口坐标系求交
	if (usePolytopeIntersector)
	{
		osgUtil::PolytopeIntersector* picker;
		if (useWindowCoordinates)
		{
			// use window coordinates
			// remap the mouse x,y into viewport coordinates.
			osg::Viewport* viewport = viewer->getCamera()->getViewport();
			double mx = viewport->x() + (int)((double)viewport->width()*(ea.getXnormalized()*0.5 + 0.5));
			double my = viewport->y() + (int)((double)viewport->height()*(ea.getYnormalized()*0.5 + 0.5));

			// half width, height.
			double w = 5.0f;
			double h = 5.0f;
			picker = new osgUtil::PolytopeIntersector(osgUtil::Intersector::WINDOW, mx - w, my - h, mx + w, my + h);
		}
		else {
			double mx = ea.getXnormalized();
			double my = ea.getYnormalized();
			double w = 0.05;
			double h = 0.05;
			picker = new osgUtil::PolytopeIntersector(osgUtil::Intersector::PROJECTION, mx - w, my - h, mx + w, my + h);
		}
		osgUtil::IntersectionVisitor iv(picker);

		viewer->getCamera()->accept(iv);

		if (picker->containsIntersections())
		{
			osgUtil::PolytopeIntersector::Intersection intersection = picker->getFirstIntersection();

			osg::notify(osg::NOTICE) << "Picked " << intersection.localIntersectionPoint << std::endl
				<< "  Distance to ref. plane " << intersection.distance
				<< ", max. dist " << intersection.maxDistance
				<< ", primitive index " << intersection.primitiveIndex
				<< ", numIntersectionPoints "
				<< intersection.numIntersectionPoints
				<< std::endl;

			osg::NodePath& nodePath = intersection.nodePath;
			node = (nodePath.size() >= 1) ? nodePath[nodePath.size() - 1] : 0;
			parent = (nodePath.size() >= 2) ? dynamic_cast<osg::Group*>(nodePath[nodePath.size() - 2]) : 0;

			if (node) std::cout << "  Hits " << node->className() << " nodePath size " << nodePath.size() << std::endl;
		}

	}
	else
	{
		osgUtil::LineSegmentIntersector* picker;
		if (!useWindowCoordinates)
		{
			// use non dimensional coordinates - in projection/clip space
			picker = new osgUtil::LineSegmentIntersector(osgUtil::Intersector::PROJECTION, ea.getXnormalized(), ea.getYnormalized());
		}
		else {
			// use window coordinates
			// remap the mouse x,y into viewport coordinates.
			osg::Viewport* viewport = viewer->getCamera()->getViewport();
			float mx = viewport->x() + (int)((float)viewport->width()*(ea.getXnormalized()*0.5f + 0.5f));
			float my = viewport->y() + (int)((float)viewport->height()*(ea.getYnormalized()*0.5f + 0.5f));
			picker = new osgUtil::LineSegmentIntersector(osgUtil::Intersector::WINDOW, mx, my);
		}
		osgUtil::IntersectionVisitor iv(picker);

		viewer->getCamera()->accept(iv);

		if (picker->containsIntersections())
		{
			osgUtil::LineSegmentIntersector::Intersection intersection = picker->getFirstIntersection();
			osg::notify(osg::NOTICE) << "Picked " << intersection.localIntersectionPoint << std::endl;

			osg::NodePath& nodePath = intersection.nodePath;
			node = (nodePath.size() >= 1) ? nodePath[nodePath.size() - 1] : 0;
			parent = (nodePath.size() >= 2) ? dynamic_cast<osg::Group*>(nodePath[nodePath.size() - 2]) : 0;

			if (node) std::cout << "  Hits " << node->className() << " nodePath size" << nodePath.size() << std::endl;
		}
	}
}
