// UnknownRecord.cpp

#include "flt.h"
#include "Registry.h"
#include "UnknownRecord.h"

using namespace flt;

////////////////////////////////////////////////////////////////////
//
//                         UnknownRecord
//
////////////////////////////////////////////////////////////////////

RegisterRecordProxy<UnknownRecord> g_UnknownProxy;

UnknownRecord::UnknownRecord()
{
}


// virtual
UnknownRecord::~UnknownRecord()
{
}
