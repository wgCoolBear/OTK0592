#include "ToolKitFunctionClass.h"
#include <Windows.h>
#include <osg/Math>
#include <osg/TriangleFunctor>
#include "osg/Geode"
#include "osg/Texture2D"
#include "osg/Geometry"
#include "osgDB/ReadFile"
#include "osg/ShapeDrawable"
#include "osg/LineWidth"
#include <osg/PositionAttitudeTransform>
#include "AutoScaleCullCallback.h"
#include "osg/CullFace"
#include "osg/PolygonMode"
#if INCLUDE_OE
#include <osgEarth/Terrain>
#endif
#include "osg/Point"
#include "AutoScaleGroup.h"

/************************************************************************/
/* 常用工具函数或者类
/************************************************************************/
//osg::createGeodeForImage();
auto a = osg::sign(1);

/************************************************************************/
/* 遍历全部三角面片的顶点信息
/* osg::TriangleFunctor
/************************************************************************/
struct VertexPrint
{
	//注意osg3.4.0中需要第四个参数，而osg3.6不能
	void operator() (const osg::Vec3& v1, const osg::Vec3& v2, const osg::Vec3& v3/*, bool*/)
	{
		//变换到世界坐标
		_pos->insert(v1*mat);
		_pos->insert(v2*mat);
		_pos->insert(v3*mat);
	}
	osg::Matrix mat;
	std::set<osg::Vec3>* _pos;
};
//访问全部三角形
class TriangleVisitor : public osg::NodeVisitor
{
public:
	TriangleVisitor(std::set<osg::Vec3>& pos) : osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN),
		_pos(pos)
	{
	}
	void apply(osg::Geode& geode)
	{
		for (unsigned int i = 0; i < geode.getNumDrawables(); ++i)
		{
			//geode.getDrawable(i)->getWorldMatrices();
			osg::NodePath nodePath = this->getNodePath();
			osg::Matrix mat = osg::computeLocalToWorld(nodePath);
			osg::TriangleFunctor<VertexPrint> tf;
			tf.mat = mat;
			tf._pos = &_pos;
			geode.getDrawable(i)->accept(tf);
		}
	}
	std::set<osg::Vec3>& _pos;
};
void ToolKitFunctionClass::getAllTrianglesVertex(osg::Node* node, std::set<osg::Vec3>& pos)
{
	if (node)
	{
		TriangleVisitor tv(pos);
		node->accept(tv);
	}
}

osg::Geode* ToolKitFunctionClass::createBoxWithLinePoint(const std::vector<osg::Vec3>& corners)
{
	osg::Geode* boxGeode = new osg::Geode();
	osg::Geometry* lines = new osg::Geometry();
	boxGeode->addDrawable(lines);
	osg::Vec3Array* vertices = new osg::Vec3Array(corners.size(), (osg::Vec3*)&corners[0]);
	lines->setVertexArray(vertices);
	osg::Vec4Array* colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f));
	lines->setColorArray(colors, osg::Array::BIND_OVERALL);
	osg::Vec3Array* normals = new osg::Vec3Array;
	normals->push_back(osg::Vec3(0.0f, 0.0f, 1.0f));
	lines->setNormalArray(normals, osg::Array::BIND_OVERALL);
	osg::DrawElementsUShort* topAndBottom = new osg::DrawElementsUShort(osg::PrimitiveSet::LINES);
	topAndBottom->push_back(0);	topAndBottom->push_back(1); 
	topAndBottom->push_back(2);	topAndBottom->push_back(3);
	topAndBottom->push_back(4);	topAndBottom->push_back(5); 
	topAndBottom->push_back(6);	topAndBottom->push_back(7);
	topAndBottom->push_back(0);	topAndBottom->push_back(3);
	topAndBottom->push_back(1);	topAndBottom->push_back(2);
	topAndBottom->push_back(4);	topAndBottom->push_back(7);
	topAndBottom->push_back(5);	topAndBottom->push_back(6);
	topAndBottom->push_back(0);	topAndBottom->push_back(4);
	topAndBottom->push_back(1);	topAndBottom->push_back(5);
	topAndBottom->push_back(2);	topAndBottom->push_back(6);
	topAndBottom->push_back(3);	topAndBottom->push_back(7);
	lines->addPrimitiveSet(topAndBottom);
	lines->getOrCreateStateSet()->setAttribute(new osg::LineWidth(5.0f));
	lines->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//8个顶点
	for (unsigned int i = 0; i < corners.size(); ++i)
	{
		osg::Sphere* sphere = new osg::Sphere(corners[i], 0.1);
		osg::ShapeDrawable* d = new osg::ShapeDrawable(sphere);
		d->setColor(osg::Vec4(0, 0.5, 1, 1));
		boxGeode->addDrawable(d);
	}
	return boxGeode;
}

osg::Geometry* ToolKitFunctionClass::createTexturedQuad(const std::string& imgFile,const osg::Vec3& corner, const osg::Vec3& widthVec, const osg::Vec3& heightVec)
{
	osg::ref_ptr<osg::Texture2D> texture = new osg::Texture2D;
	osg::ref_ptr<osg::Image> image = osgDB::readImageFile(imgFile);
	texture->setImage(image.get());

	osg::Geometry* quad = osg::createTexturedQuadGeometry(corner, widthVec, heightVec);
	quad->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture.get());
	return quad;
}

osg::Geode* ToolKitFunctionClass::createQuad(float w, float h)
{
	osg::Geode* geo = new osg::Geode();
#if 0
	osg::Geometry* quad = osg::createTexturedQuadGeometry(osg::Vec3(-w / 2, h / 2, 0), 
		osg::Vec3(w, 0, 0), osg::Vec3(0, -h, 0));
	geo->addDrawable(quad);
#else
	osg::Geometry* geom = new osg::Geometry();
	geo->addDrawable(geom);
	osg::Vec4Array* colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(0.8f, 0.8f, 0.8f, 0.8f));
	geom->setColorArray(colors, osg::Array::BIND_OVERALL);
	osg::Vec3Array* normals = new osg::Vec3Array;
	normals->push_back(osg::Z_AXIS);
	geom->setNormalArray(colors, osg::Array::BIND_OVERALL);
	osg::Vec3Array* vertices = new osg::Vec3Array;
	vertices->push_back(osg::Vec3(-w / 2, h / 2, 0));
	vertices->push_back(osg::Vec3(-w / 2, -h / 2, 0));
	vertices->push_back(osg::Vec3(w / 2, -h / 2, 0));
	vertices->push_back(osg::Vec3(w / 2, h / 2, 0));
	geom->setVertexArray(vertices);
	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, vertices->size()));
#endif
	return geo;
}

osg::Geode* ToolKitFunctionClass::createPoint()
{
	//需要关闭相机的细节裁剪。有些时候例如只绘制一个点的时候会看不见，是因为细节裁剪给裁剪掉了。switch off small feature culling as this can cull out geometry that will still be large enough once perspective correction takes effect.
	//viewer.getCamera()->setCullingMode(viewer.getCamera()->getCullingMode() & ~osg::CullSettings::SMALL_FEATURE_CULLING);

	osg::Geode* geo = new osg::Geode();
	osg::Geometry* geom = new osg::Geometry();
	geo->addDrawable(geom);
	osg::Vec4Array* colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));
	geom->setColorArray(colors, osg::Array::BIND_OVERALL);
	osg::Vec3Array* normals = new osg::Vec3Array;
	normals->push_back(osg::Z_AXIS);
	geom->setNormalArray(colors, osg::Array::BIND_OVERALL);
	osg::Vec3Array* vertices = new osg::Vec3Array;
	vertices->push_back(osg::Vec3(0, 0, 0));
	geom->setVertexArray(vertices);
	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, vertices->size()));
	osg::StateSet *ss = geom->getOrCreateStateSet();
	ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	//ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	ss->setAttributeAndModes(new osg::Point(50));
	return geo;
}

osg::MatrixTransform* ToolKitFunctionClass::makeFrustumGeometry(osg::Matrixd proj, osg::Matrixd mv)
{
	// Get near and far from the Projection matrix.
	const double nearPlane = proj(3, 2) / (proj(2, 2) - 1.0);
	const double farPlane = proj(3, 2) / (1.0 + proj(2, 2));

	// Get the sides of the near plane.
	const double nLeft = nearPlane * (proj(2, 0) - 1.0) / proj(0, 0);
	const double nRight = nearPlane * (1.0 + proj(2, 0)) / proj(0, 0);
	const double nTop = nearPlane * (1.0 + proj(2, 1)) / proj(1, 1);
	const double nBottom = nearPlane * (proj(2, 1) - 1.0) / proj(1, 1);

	// Get the sides of the far plane.
	const double fLeft = farPlane * (proj(2, 0) - 1.0) / proj(0, 0);
	const double fRight = farPlane * (1.0 + proj(2, 0)) / proj(0, 0);
	const double fTop = farPlane * (1.0 + proj(2, 1)) / proj(1, 1);
	const double fBottom = farPlane * (proj(2, 1) - 1.0) / proj(1, 1);

	// Our vertex array needs only 9 vertices: The origin, and the
	// eight corners of the near and far planes.
	osg::Vec3Array* v = new osg::Vec3Array;
	v->resize(9);
	(*v)[0].set(0., 0., 0.);
	(*v)[1].set(nLeft, nBottom, -nearPlane);
	(*v)[2].set(nRight, nBottom, -nearPlane);
	(*v)[3].set(nRight, nTop, -nearPlane);
	(*v)[4].set(nLeft, nTop, -nearPlane);
	(*v)[5].set(fLeft, fBottom, -farPlane);
	(*v)[6].set(fRight, fBottom, -farPlane);
	(*v)[7].set(fRight, fTop, -farPlane);
	(*v)[8].set(fLeft, fTop, -farPlane);

	osg::Geometry* geom = new osg::Geometry;
	geom->setUseDisplayList(false);
	geom->setVertexArray(v);

	osg::Vec4Array* c = new osg::Vec4Array;
	c->push_back(osg::Vec4(1., 1., 1., 1.));
	geom->setColorArray(c, osg::Array::BIND_OVERALL);

	GLushort idxLines[8] = {
		0, 5, 0, 6, 0, 7, 0, 8 };
	GLushort idxLoops0[4] = {
		1, 2, 3, 4 };
	GLushort idxLoops1[4] = {
		5, 6, 7, 8 };
	geom->addPrimitiveSet(new osg::DrawElementsUShort(osg::PrimitiveSet::LINES, 8, idxLines));
	geom->addPrimitiveSet(new osg::DrawElementsUShort(osg::PrimitiveSet::LINE_LOOP, 4, idxLoops0));
	geom->addPrimitiveSet(new osg::DrawElementsUShort(osg::PrimitiveSet::LINE_LOOP, 4, idxLoops1));

	osg::Geode* geode = new osg::Geode;
	geode->addDrawable(geom);

	geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);


	// Create parent MatrixTransform to transform the view volume by
	// the inverse ModelView matrix.
	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->setMatrix(osg::Matrixd::inverse(mv));
	mt->addChild(geode);

	return mt;
}

template<typename T>
T ToolKitFunctionClass::as(const std::string& str, const T& default_value)
{
	T temp = default_value;
	std::istringstream strin( str );
	if ( !strin.eof() ) strin >> temp;
	return temp;
}

template<typename T>
std::string ToolKitFunctionClass::toString(const T& value)
{
	std::stringstream out;
	//out << std::setprecision(20) << std::fixed << value;
	out << std::setprecision(20) <<  value;
	std::string outStr;
	outStr = out.str();
	return outStr;
}

std::string ToolKitFunctionClass::w2s(const std::wstring &src)
{
	std::string result;
	int n = WideCharToMultiByte(CP_UTF8, 0, src.c_str(), -1, 0, 0, 0, 0);
	result.resize(n);
	::WideCharToMultiByte(CP_UTF8, 0, src.c_str(), -1, (char*)result.c_str(), result.length(), 0, 0);
	return result;
}

std::wstring ToolKitFunctionClass::s2w(const std::string &src)
{
	std::wstring result;
	int n = MultiByteToWideChar(CP_ACP, 0, src.c_str(), -1, NULL, 0);
	result.resize(n);
	::MultiByteToWideChar(CP_ACP, 0, src.c_str(), -1, (LPWSTR)result.c_str(), result.length());
	return result;
}

std::string ToolKitFunctionClass::s2utf8(const std::string &src)
{
	std::wstring strWideChar = s2w(src);
	std::string utf8 = w2s(strWideChar);
	return utf8;
}

osg::Geode* ToolKitFunctionClass::createGraticule(float R, float spanLat/*=15*/, float spanLon/*=15*/, float lenPerSegment/*=1*/, osg::Vec4 color/*=osg::Vec4(1,1,1,1) */)
{
	osg::ref_ptr<osg::Geode> graticuleGeode = new osg::Geode();
	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
	osg::Vec3Array* vertices = new osg::Vec3Array();
	//纬线圈
	for(int lat=-90; lat<90; lat+=spanLat)
	{
		int firstIdx = vertices->size();
		float latRadian = osg::inDegrees((float)lat);
		//纬线圈长度
		float latCircleLen = 2*osg::PI*(R*cos(latRadian));
		//构成纬线圈的段数
		int seg = int(latCircleLen/lenPerSegment)+1;
		//每段对应的经度
		float lonPerSeg = 360.0f/seg;
		for (int s=0;s<seg;s++)
		{
			float lon = s*lonPerSeg;
			float lonRadian = osg::inDegrees((float)lon);
			float x = R*cos(latRadian)*sin(lonRadian);
			float y = R*cos(latRadian)*cos(lonRadian);
			float z = R*sin(latRadian);
			vertices->push_back(osg::Vec3(x,y,z));
		}
		//一个纬线圈
		osg::DrawArrays* drawArrays = new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, firstIdx, seg);
		geom->addPrimitiveSet(drawArrays);
	}

	//经线圈长度
	float lonCircleLen = 2*osg::PI*R;
	//构成经线圈的段数
	int seg = int(lonCircleLen/lenPerSegment)+1;
	//每段对应的圆心角
	float anglePerSeg = 360.0f/seg;
	for (int lon=0;lon<180;lon+=spanLon)
	{
		int firstIdx = vertices->size();
		float lonRadian = osg::inDegrees((float)lon);
		for (int s=0;s<seg;s++)
		{
			float angle = s*anglePerSeg;
			float latRadian = osg::inDegrees(angle);
			float x = R*cos(latRadian)*sin(lonRadian);
			float y = R*cos(latRadian)*cos(lonRadian);
			float z = R*sin(latRadian);
			vertices->push_back(osg::Vec3(x,y,z));
		}
		//一个经线圈
		osg::DrawArrays* drawArrays = new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, firstIdx, seg);
		geom->addPrimitiveSet(drawArrays);
	}
	geom->setVertexArray(vertices);
	osg::Vec4Array* colors = new osg::Vec4Array();
	colors->push_back(color);
	geom->setColorArray(colors, osg::Array::BIND_OVERALL);

	graticuleGeode->addDrawable(geom);
	graticuleGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	return graticuleGeode.release();
}

osg::Group* ToolKitFunctionClass::createSphere(float R, bool withWire /*= true*/, float spanLat /*= 15*/, float spanLon /*= 15*/, osg::Vec4 color /*= osg::Vec4(0, 0.5, 1, 0.3)*/)
{
	osg::Group* group = new osg::Group();
	//使用前后两个面，是为了消除自遮挡带来的重叠问题。见TransparentBall
	osg::Geode* geodeFront = new osg::Geode();
	osg::Geode* geodeBack = new osg::Geode();
	group->addChild(geodeFront);
	group->addChild(geodeBack);

	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
	//纬线条数
	int latLineCount = osg::round(180 / spanLat) + 1;
	//经线条数（半圆）
	int lonLineCount = osg::round(360 / spanLon);
	osg::Vec3Array* vertices = new osg::Vec3Array();
	osg::Vec3Array* normals = new osg::Vec3Array();
	osg::DrawElementsUInt* drawElements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUAD_STRIP);
	for (int latIndex = 0; latIndex <= latLineCount; latIndex++)
	{
		for (int lonIndex = 0; lonIndex < lonLineCount; lonIndex++)
		{
			float lat = latIndex*spanLat - 90;
			float lon = lonIndex*spanLon - 180;
			if (lat > 90)
				lat = 90;
			float latRadian = osg::inDegrees((float)lat);
			float lonRadian = osg::inDegrees((float)lon);
			float x = R*cos(latRadian)*sin(lonRadian);
			float y = R*cos(latRadian)*cos(lonRadian);
			float z = R*sin(latRadian);
			osg::Vec3 pos(x,y,z);
			vertices->push_back(pos);
			osg::Vec3 normal = pos; normal.normalize();
			normals->push_back(normal);

			if (latIndex < latLineCount)
			{
				drawElements->push_back(latIndex*lonLineCount + lonIndex);
				drawElements->push_back((latIndex + 1)*lonLineCount + lonIndex);
			}
		}
		//连接当前纬线圈头部
		if (latIndex < latLineCount)
		{
			drawElements->push_back(latIndex*lonLineCount + 0);
			drawElements->push_back((latIndex + 1)*lonLineCount + 0);
			//这样能保证与下一行对接上
			drawElements->push_back((latIndex + 1)*lonLineCount + 0);
			drawElements->push_back((latIndex + 1)*lonLineCount + 0);
		}
	}
	geom->setVertexArray(vertices);
	geom->setNormalArray(normals, osg::Array::BIND_PER_VERTEX);
	osg::Vec4Array* colors = new osg::Vec4Array();
	colors->push_back(color);
	geom->setColorArray(colors, osg::Array::BIND_OVERALL);
	geom->addPrimitiveSet(drawElements);

	geodeFront->addDrawable(geom);
	geodeBack->addDrawable(geom);
	{
		osg::StateSet* ss = geodeFront->getOrCreateStateSet();
		osg::CullFace* cullFace = new osg::CullFace(osg::CullFace::FRONT);
		ss->setAttributeAndModes(cullFace, osg::StateAttribute::ON);
		//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
		ss->setMode(GL_BLEND, osg::StateAttribute::ON);
		ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	}
	{
		osg::StateSet* ss = geodeBack->getOrCreateStateSet();
		osg::CullFace* cullFace = new osg::CullFace(osg::CullFace::BACK);
		ss->setAttributeAndModes(cullFace, osg::StateAttribute::ON);
		//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
		ss->setMode(GL_BLEND, osg::StateAttribute::ON);
		ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	}

	if (withWire)
	{
		osg::Geode* wireGeode = new osg::Geode;
		group->addChild(wireGeode);
		wireGeode->addChild(geom);
		osg::StateSet* ss = wireGeode->getOrCreateStateSet();
		ss->setAttributeAndModes(new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE), osg::StateAttribute::ON);
		//ss->setAttributeAndModes(new osg::PolygonOffset(-1, -1));
		osg::LineWidth* lw = new osg::LineWidth(1);
		ss->setAttributeAndModes(lw, osg::StateAttribute::ON);
		//使用一个简单的shader设置网格颜色
		osg::Program* program = new osg::Program;
		const char *microshaderVertSource = {
			"#version 120\n"
			"// microshader - colors a fragment based on its position\n"
			"varying vec4 color;\n"
			"void main(void)\n"
			"{\n"
			"    color = gl_Vertex;\n"
			"    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n"
			"}\n"
		};
		const char *microshaderFragSource = {
			"#version 120\n"
			"varying vec4 color;\n"
			"uniform bool test1=false;\n"
			"void main(void)\n"
			"{\n"
			"	if(test1)\n"
			"		gl_FragColor = clamp( color, 0.0, 1.0 );\n"
			"	else\n"
			"		gl_FragColor = vec4( 1.0, 1.0, 0.0, 1.0 );\n"
			"}\n"
		};
		program->addShader(new osg::Shader(osg::Shader::VERTEX, microshaderVertSource));
		program->addShader(new osg::Shader(osg::Shader::FRAGMENT, microshaderFragSource));
		ss->setAttributeAndModes(program, osg::StateAttribute::ON);
	}

	return group;
}

osg::Geode* ToolKitFunctionClass::createFan(float R, float angle, osg::Vec4 color /*= osg::Vec4(0.3, 0.5, 0, 0.5)*/)
{
	osg::Geode* geode = new osg::Geode();
	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
	osg::Vec3Array* vertices = new osg::Vec3Array();
	//圆心
	vertices->push_back(osg::Vec3(0, 0, 0));
	//构成圆弧的段数
	int seg = 10;
	//每段对应的圆心角
	float anglePerSeg = angle / seg;
	for (int s = 0; s <= seg; s++)
	{
		float angle = s*anglePerSeg;
		float latRadian = osg::inDegrees(angle);
		float x = R*cos(latRadian);
		float y = 0;
		float z = R*sin(latRadian);
		vertices->push_back(osg::Vec3(x, y, z));
	}
	osg::DrawArrays* drawArrays = new osg::DrawArrays(osg::PrimitiveSet::TRIANGLE_FAN, 0, vertices->size());
	geom->addPrimitiveSet(drawArrays);
	geom->setVertexArray(vertices);
	osg::Vec4Array* colors = new osg::Vec4Array();
	colors->push_back(color);
	geom->setColorArray(colors, osg::Array::BIND_OVERALL);
	osg::Vec3Array* normals = new osg::Vec3Array();
	normals->push_back(osg::Vec3(0,-1,0));
	geom->setNormalArray(normals, osg::Array::BIND_OVERALL);

	geode->addDrawable(geom);
	osg::StateSet* ss = geode->getOrCreateStateSet();
	ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	return geode;
}

osg::Group* ToolKitFunctionClass::createAutoScaleModel(osg::Node* model)
{
	osg::Group* grp = new osg::Group();
#if 1
	osg::MatrixTransform* mt = new osg::MatrixTransform();
	mt->addChild(model);
	AutoScaleGroup* asMt = new AutoScaleGroup(mt);
	grp->addChild(asMt);
	grp->addChild(mt);
#else//功能尚未正确实现，不能使用
	osg::PositionAttitudeTransform* pat = new osg::PositionAttitudeTransform();
	pat->addChild(model);
	AutoScaleCullCallback* cb = new AutoScaleCullCallback();
	pat->addCullCallback(cb);
	pat->setCullingActive(false);
	grp->addChild(pat);
#endif
	return grp;
}
#if INCLUDE_OE
double ToolKitFunctionClass::getElevation(osgEarth::MapNode* mapNode, osgEarth::GeoPoint lonlat)
{
	double elevation = 0.0;
	//第一种方法
	{
		auto query = new osgEarth::ElevationQuery(mapNode->getMap());
		elevation = query->getElevation(lonlat);
	}
	//第2种方法
	{
		mapNode->getTerrain()->getHeight(mapNode->getMapSRS(), lonlat.x(), lonlat.y(), &elevation);
	}
	
	return elevation;
}

osg::Vec3d ToolKitFunctionClass::LLA2XYZ(osgEarth::GeoPoint lla)
{
	osg::Vec3d worldPos;
	lla.toWorld(worldPos);
	return worldPos;
}

osgEarth::GeoPoint ToolKitFunctionClass::XYZ2LLA(osg::Vec3d xyz, osgEarth::SpatialReference* srs /*= osgEarth::SpatialReference::get("wgs84")*/)
{
	osgEarth::GeoPoint lla;
	lla.fromWorld(srs, xyz);
	return lla;
}

#endif