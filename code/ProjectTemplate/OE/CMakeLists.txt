﻿cmake_minimum_required(VERSION 3.0)

SET(PRODUCT_NAME OE)

PROJECT(${PRODUCT_NAME})

# 指定自定义cmake脚本，在CMakeModules文件夹下
SET(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/CMakeModules;${CMAKE_MODULE_PATH}")

# 查找OSG库
FIND_PACKAGE(OSG)
INCLUDE_DIRECTORIES(${OSG_INCLUDE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})
LINK_DIRECTORIES(${OSG_LIB_DIR})

# 查找OSGEARTH库
FIND_PACKAGE(OSGEARTH)
INCLUDE_DIRECTORIES(${OSGEARTH_INCLUDE_DIR})
LINK_DIRECTORIES(${OSGEARTH_LIB_DIR})

# 查找第三方依赖库
FIND_PACKAGE(3rdParty)
IF(3rd_FOUND)
	SET(3rd_INCLUDE_DIR ${3rd_DIR}/include)
	SET(3rd_LIB_DIR ${3rd_DIR}/lib)
	INCLUDE_DIRECTORIES(${3rd_INCLUDE_DIR})
	LINK_DIRECTORIES(${3rd_LIB_DIR})
ENDIF()

# 设置Debug版本和Release版本下库文件的后缀名
SET(CMAKE_DEBUG_POSTFIX "d")
SET(CMAKE_RELEASE_POSTFIX "")

SET(PRODUCT_HEADER_FILES
)

SET(PRODUCT_FILES
    src/main.cpp
    ${PRODUCT_HEADER_FILES}
)

#SET(PRODUCT_DATA_FILES
    # data/axes.osgt
#)

SET(WITH_CONSOLE 1)
IF(${WITH_CONSOLE})
    ADD_EXECUTABLE(${PRODUCT_NAME} ${PRODUCT_FILES})
ELSE(${WITH_CONSOLE})
    ADD_EXECUTABLE(${PRODUCT_NAME} WIN32 ${PRODUCT_FILES})
ENDIF(${WITH_CONSOLE})

SET_TARGET_PROPERTIES(${PRODUCT_NAME} PROPERTIES DEBUG_POSTFIX "${CMAKE_DEBUG_POSTFIX}")

TARGET_LINK_LIBRARIES(${PRODUCT_NAME}
    debug osgd          optimized osg
    # debug osgAnimationd optimized osgAnimation
    # debug osgParticled  optimized osgParticle
    debug osgDBd        optimized osgDB
    debug osgGAd        optimized osgGA
    debug osgTextd      optimized osgText
    # debug osgTerraind   optimized osgTerrain
    debug osgUtild      optimized osgUtil
    debug osgViewerd    optimized osgViewer
    debug OpenThreadsd  optimized OpenThreads
	debug osgEarthd               optimized osgEarth
    debug osgEarthFeaturesd       optimized osgEarthFeatures
    debug osgEarthUtild           optimized osgEarthUtil
    debug osgEarthSymbologyd      optimized osgEarthSymbology
    debug osgEarthAnnotationd     optimized osgEarthAnnotation
    ${OPENGL_LIBRARIES} ${THIRD_PARTY_LIBRARIES} 
	# ${EXTERNAL_LIBRARIES}
)

SET_TARGET_PROPERTIES(${PRODUCT_NAME} PROPERTIES 
    ARCHIVE_OUTPUT_DIRECTORY_DEBUG "${PROJECT_BINARY_DIR}/lib"
    ARCHIVE_OUTPUT_DIRECTORY_RELEASE "${PROJECT_BINARY_DIR}/lib"
    LIBRARY_OUTPUT_DIRECTORY_DEBUG "${PROJECT_BINARY_DIR}/lib"
    LIBRARY_OUTPUT_DIRECTORY_RELEASE "${PROJECT_BINARY_DIR}/lib"
    RUNTIME_OUTPUT_DIRECTORY_DEBUG "${PROJECT_BINARY_DIR}/bin"
    RUNTIME_OUTPUT_DIRECTORY_RELEASE "${PROJECT_BINARY_DIR}/bin"
	DEBUG_POSTFIX "d"
	RELEASE_POSTFIX "")

# 设置工作目录
SET_TARGET_PROPERTIES(${PRODUCT_NAME} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}")
	
# 设置工程调试环境变量PATH目录(最终影响项目属性-配置属性-调试-环境)，这样不会影响系统的环境变量PATH，只对当前项目起作用(VS_DEBUGGER_ENVIRONMENT需要CMake最低3.13)
SET(DLL_PATH "PATH=%PATH%" "${OSG_DIR}/bin;${OSGEARTH_DIR}/bin;${3rd_BIN_DIR}")
SET_TARGET_PROPERTIES(${PRODUCT_NAME} PROPERTIES VS_DEBUGGER_ENVIRONMENT "${DLL_PATH}")

INSTALL(TARGETS ${PRODUCT_NAME} RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
#INSTALL(FILES ${PRODUCT_DATA_FILES} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/data)