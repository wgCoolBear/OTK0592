#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <osg/Group>
#include <osgViewer/Viewer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
	void on_action_quit_triggered();	//退出,on_action_*_triggered语法中，"quit"和mainwindow.ui中的action对应，这样就不用显示的声明connect了
private:
private:
    Ui::MainWindow *ui;
	osg::ref_ptr<osgViewer::Viewer> _viewer;
	osg::ref_ptr<osg::Camera> _mainCamera;
	osg::ref_ptr<osg::Group> _root;
};

#endif // MAINWINDOW_H
