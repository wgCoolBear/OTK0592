#include "OESceneWidget.h"
#include <osgEarthUtil/AutoClipPlaneHandler>
#include <osgEarthUtil/EarthManipulator>
#include <osgearth/TerrainEngineNode>
#include <osgEarthAnnotation/ModelNode>
#include <osgEarth/GLUtils>
#include <osgEarthAnnotation/ModelNode>
#include <osgEarthDrivers/gdal/GDALOptions>
#include <osgEarthDrivers/xyz/XYZOptions>
#include <osgEarthDrivers/tms/TMSOptions>
#include <osgEarthDrivers/model_feature_geom/FeatureGeomModelOptions>
#include <osgEarthDrivers/cache_filesystem/FileSystemCache>
#include <osgEarth/ModelLayer>
#include <osgEarth/ImageLayer>
#include <osgEarthDrivers/feature_ogr/OGRFeatureOptions>
#include <osgEarthDrivers/agglite/AGGLiteOptions>

OESceneWidget::OESceneWidget(QWidget* parent /*= 0*/, Qt::WindowFlags f /*= 0*/) 
	: BaseSceneWidget(parent, f)
{
	osg::Camera* camera = _viewer->getCamera();
	//----------------------在OE2.10中，需要添加以下两行，否则线或者点可能会看不见------------
	// default uniform values:
	osgEarth::GLUtils::setGlobalDefaults(camera->getOrCreateStateSet());
	// disable the small-feature culling
	camera->setSmallFeatureCullingPixelSize(-1.0f);

	_map = new osgEarth::Map();
	_mapNode = new osgEarth::MapNode(_map);

	osgEarth::Drivers::GDALOptions basemapOpt;
	basemapOpt.url() = "../../../data/image/world.tif";
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("basemap", basemapOpt)));

	osgEarth::Symbology::Style modelStyle;
	osg::ref_ptr<osg::Node> model3D = osgDB::readRefNodeFile("axes.osgt.1000000.scale");
	modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->setModel(model3D);
	//modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->url() = std::string("data/model/red_flag.osg.100000.scale");
	auto modelNode = new osgEarth::Annotation::ModelNode(_mapNode, modelStyle);
	modelNode->setPosition(osgEarth::GeoPoint(_mapNode->getMapSRS(), 118.13, 45));
	_mapNode->addChild(modelNode);

	_root->addChild(_mapNode);

	// 近地面自动裁剪，这样可使地面的模型不被裁剪
	camera->addCullCallback(new osgEarth::Util::AutoClipPlaneCullCallback(_mapNode));

	osgEarth::Util::EarthManipulator* em = new osgEarth::Util::EarthManipulator();
	_viewer->setCameraManipulator(em);
	osgEarth::Viewpoint viewpoint("", 116.393714, 39.9184, 15, -2.50, -60.0, 1.5e7);
	em->setViewpoint(viewpoint);
	em->setHomeViewpoint(viewpoint);

	_viewer->home();
}

osgEarth::Map* OESceneWidget::getMap()
{
	return _map;
}

osgEarth::MapNode* OESceneWidget::getMapNode()
{
	return _mapNode;
}