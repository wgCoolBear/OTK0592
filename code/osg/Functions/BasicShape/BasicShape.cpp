#include "BasicShape.h"
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/Point>
#include <osg/LineWidth>
#include <osg/Shape>
#include <osg/ShapeDrawable>
#include "osgText/Text"
#include "osg/AutoTransform"
#include "osg/PositionAttitudeTransform"
#include "3rd/ToolKitFunctionClass/ToolKitFunctionClass.h"
#include "osgDB/ReadFile"

BasicShape::BasicShape()
{
	{
		//自动缩放模型。有些问题，例如初始不可见，法向量变形问题
		//osg::Group* g = ToolKitFunctionClass::createAutoScaleModel(osgDB::readNodeFile("data/model/axes.osgb"));
		//addChild(g);
	}
	osg::Geode* geode = new osg::Geode();
	this->addChild(geode);
	osg::Vec3 pos[] =
	{
		osg::Vec3(-1, 0, 0),
		osg::Vec3(1, 0, 0),
		osg::Vec3(1, 0, 1),
		osg::Vec3(0, 0, 2),
		osg::Vec3(-1, 0, 1)
	};

	//点线
	{
		osg::Geometry* geom = new osg::Geometry();
		int vertexNum = sizeof(pos) / sizeof(osg::Vec3);
		osg::Vec3Array* vertices = new osg::Vec3Array(vertexNum, pos);
		geom->setVertexArray(vertices);
		osg::Vec4Array* colors = new osg::Vec4Array;
		colors->push_back(osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f));
		geom->setColorArray(colors, osg::Array::BIND_OVERALL);
		osg::Vec3Array* normals = new osg::Vec3Array;
		normals->push_back(osg::Vec3(0.0f, 0.0f, 1.0f));
		geom->setNormalArray(normals, osg::Array::BIND_OVERALL);
		geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, vertexNum));
		geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, 0, vertexNum));
		osg::DrawElementsUShort* elements = new osg::DrawElementsUShort(osg::PrimitiveSet::TRIANGLES);
		elements->push_back(0);elements->push_back(1);elements->push_back(2);
		elements->push_back(2);elements->push_back(3);elements->push_back(4);
		geom->addPrimitiveSet(elements);
		geom->getOrCreateStateSet()->setAttribute(new osg::Point(10.0f));
		geom->getOrCreateStateSet()->setAttribute(new osg::LineWidth(2.0f));
		geom->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

		geode->addDrawable(geom);
	}

	//立方体和球
	{
		osg::Shape* box = new osg::Box(pos[4], 0.1);
		osg::ShapeDrawable* boxDrawable = new osg::ShapeDrawable(box);
		boxDrawable->setColor(osg::Vec4(1, 0, 0, 1));
		geode->addDrawable(boxDrawable);

		osg::Sphere* sphere = new osg::Sphere(pos[2],0.1);
		osg::ShapeDrawable* sphereDrawable = new osg::ShapeDrawable(sphere);
		sphereDrawable->setColor(osg::Vec4(0, 1, 0, 1));
		geode->addDrawable(sphereDrawable);
	}
	
	//文字
	{
		osg::ref_ptr<osgText::Text> text=new osgText::Text();
		//在文字内容频繁更新时，低配置机器可以设置此分辨率20*20以保证正常显示.默认是32*32
		//text->setFontResolution(20,20);
		//text->setDataVariance(osg::Object::DYNAMIC);
		text->setFont("simsun.ttc");
		text->setAlignment(osgText::Text::LEFT_CENTER);
		//text->setAutoRotateToScreen(true);
		//text->setAxisAlignment(osgText::TextBase::XZ_PLANE);
		text->setPosition(osg::Vec3(0,0,0));
		text->setText(ToolKitFunctionClass::s2utf8("osgText中文汉字"), osgText::String::ENCODING_UTF8);
		text->setCharacterSize(40);
		text->setColor(osg::Vec4(1,1,1,1));
		osg::ref_ptr<osg::Geode> textGeode=new osg::Geode();
		textGeode->addDrawable(text.get());
		textGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::PROTECTED);
		//关闭深度测试，可以使文字不被遮挡
		textGeode->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF); 
		osg::ref_ptr<osg::AutoTransform> autoTransform=new osg::AutoTransform();
		autoTransform->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_SCREEN);
		autoTransform->setAutoScaleToScreen(true);
		autoTransform->addChild(textGeode);
		autoTransform->setPosition(osg::Vec3(1,0,0.5));
		addChild(autoTransform);
	}

	//自定义经纬线网格
	{
		osg::PositionAttitudeTransform* pat = new osg::PositionAttitudeTransform();
		pat->setPosition(osg::Vec3(0,0,-0.5));
		//pat->setAttitude(osg::Quat(osg::PI_2, osg::X_AXIS));
		pat->addChild(ToolKitFunctionClass::createGraticule(0.4,15,15,0.1,osg::Vec4(1,0,1,1)));
		addChild(pat);
	}

	//带有动态纹理的金字塔
	{
		class TextureCoordUpdateCallback : public osg::NodeCallback
		{
		public:
			TextureCoordUpdateCallback(double delay = 1.0) :
			  _delay(delay),  _prevTime(0.0)
			  {
			  }
			  virtual void operator()(osg::Node* node, osg::NodeVisitor* nv)
			  {
				  if (nv->getFrameStamp())
				  {
					  double currTime = nv->getFrameStamp()->getSimulationTime();
					  if (currTime - _prevTime > _delay)
					  {
						  osg::Geode* geode = node->asGeode();
						  osg::Geometry* geom = geode->getDrawable(0)->asGeometry();
						  // 获取纹理坐标数组
						  osg::Array* tmp = geom->getTexCoordArray(0);
						  osg::Vec2Array* coorArray = (osg::Vec2Array*) tmp;
						  auto it = coorArray->begin();
						  for (; it < coorArray->end(); it++)
						  {
							  // 动起来
							  it->y() += 0.001;
						  }
						  // 更新纹理坐标数组
						  geom->setTexCoordArray(0, coorArray);
						  // record time
						  _prevTime = currTime;
					  }
				  }
			  }
		protected:
			double                          _delay;
			double                          _prevTime;
		};
		
		osg::Geode* pyramidGeode = new osg::Geode();
		osg::Geometry* pyramidGeometry = new osg::Geometry();
		pyramidGeode->setUpdateCallback(new TextureCoordUpdateCallback(0.01));
		pyramidGeode->setDataVariance(osg::Object::DYNAMIC);
		pyramidGeode->addDrawable(pyramidGeometry);
		osg::PositionAttitudeTransform* pat = new osg::PositionAttitudeTransform();
		pat->setPosition(osg::Vec3(-1,0,-0.5));
		pat->setScale(osg::Vec3(0.5,0.5,0.5));
		pat->addChild(pyramidGeode);
		addChild(pat);

		osg::Vec3Array* pyramidVertices = new osg::Vec3Array;
		pyramidVertices->push_back(osg::Vec3(0, 0, 1)); // 塔尖
		pyramidVertices->push_back(osg::Vec3(-.5, -.5, 0)); // 左前
		pyramidVertices->push_back(osg::Vec3(.5, -.5, 0)); // 右前
		pyramidVertices->push_back(osg::Vec3(.5, .5, 0)); // 右后
		pyramidVertices->push_back(osg::Vec3(-.5, .5, 0)); // 左后
		pyramidGeometry->setVertexArray(pyramidVertices);
		osg::DrawElementsUInt* pyramidBase = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		pyramidBase->push_back(4);pyramidBase->push_back(3);pyramidBase->push_back(2);pyramidBase->push_back(1);
		pyramidGeometry->addPrimitiveSet(pyramidBase);
#if 1
		osg::DrawElementsUInt* pyramidFaceOne = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
		pyramidFaceOne->push_back(1);pyramidFaceOne->push_back(2);pyramidFaceOne->push_back(0);
		pyramidGeometry->addPrimitiveSet(pyramidFaceOne);
		osg::DrawElementsUInt* pyramidFaceTwo = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
		pyramidFaceTwo->push_back(2);pyramidFaceTwo->push_back(3);pyramidFaceTwo->push_back(0);
		pyramidGeometry->addPrimitiveSet(pyramidFaceTwo);
		osg::DrawElementsUInt* pyramidFaceThree = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
		pyramidFaceThree->push_back(3);pyramidFaceThree->push_back(4);pyramidFaceThree->push_back(0);
		pyramidGeometry->addPrimitiveSet(pyramidFaceThree);
		osg::DrawElementsUInt* pyramidFaceFour = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
		pyramidFaceFour->push_back(4);pyramidFaceFour->push_back(1);pyramidFaceFour->push_back(0);
		pyramidGeometry->addPrimitiveSet(pyramidFaceFour);
#else
		//pyramidVertices->push_back(osg::Vec3(-.5, -.5, 0)); // 左前
		osg::DrawArrays* pyramidFaces = new osg::DrawArrays(osg::PrimitiveSet::TRIANGLE_FAN, 0, pyramidVertices->size());
		pyramidGeometry->addPrimitiveSet(pyramidFaces);
#endif

		osg::Vec4Array* colors = new osg::Vec4Array;
		colors->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f)); //白色
		pyramidGeometry->setColorArray(colors);
		pyramidGeometry->setColorBinding(osg::Geometry::BIND_OVERALL);

		osg::Vec3Array* normals = new osg::Vec3Array(1);
		(*normals)[0].set(0.0f, 0.0f, 1.0f);
		pyramidGeometry->setNormalArray(normals, osg::Array::BIND_OVERALL);

		osg::Vec2Array* texcoords = new osg::Vec2Array(5);
		(*texcoords)[0].set(0.50f, 1.0f);
		(*texcoords)[1].set(0.00f, 0.0f);
		(*texcoords)[2].set(0.25f, 0.0f);
		(*texcoords)[3].set(0.50f, 0.0f);
		(*texcoords)[4].set(0.75f, 0.0f);
		pyramidGeometry->setTexCoordArray(0, texcoords);

		// set up the texture state.
		osg::Texture2D* texture = new osg::Texture2D;
		texture->setDataVariance(osg::Object::DYNAMIC); // protect from being optimized away as static state.
		texture->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
		texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
		texture->setWrap(osg::Texture2D::WRAP_S, osg::Texture2D::REPEAT);
		texture->setWrap(osg::Texture2D::WRAP_T, osg::Texture2D::REPEAT);
		texture->setImage(osgDB::readImageFile("Images/road.png"));
		osg::StateSet* stateset = pyramidGeometry->getOrCreateStateSet();
		stateset->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
		stateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
		//stateset->setMode(GL_BLEND, osg::StateAttribute::ON);
		//stateset->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	}
}