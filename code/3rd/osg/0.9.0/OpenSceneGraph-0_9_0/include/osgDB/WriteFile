//C++ header - Open Scene Graph - Copyright (C) 1998-2002 Robert Osfield
//Distributed under the terms of the GNU Library General Public License (LGPL)
//as published by the Free Software Foundation.

#ifndef OSGDB_WRITEFILE
#define OSGDB_WRITEFILE 1

#include <osg/Image>
#include <osg/Node>

#include <osgDB/Export>

#include <string>

namespace osgDB {

/** Write an osg::Object to file. 
  * Return true on success,
  * return false on failure.
  * The osgDB::Registry is used to load the appropriate ReaderWriter plugin
  * for the filename extension, and this plugin then handles the request
  * to write the specified file.*/

OSGDB_EXPORT extern bool writeObjectFile(const osg::Object& object, const std::string& filename);

/** Write an osg::Image to file. 
  * Return true on success,
  * return false on failure.
  * The osgDB::Registry is used to load the appropriate ReaderWriter plugin
  * for the filename extension, and this plugin then handles the request
  * to write the specified file.*/
OSGDB_EXPORT extern bool writeImageFile(const osg::Image& image, const std::string& filename);

/** Write an osg::Node to file. 
  * Return true on success,
  * return false on failure.
  * The osgDB::Registry is used to load the appropriate ReaderWriter plugin
  * for the filename extension, and this plugin then handles the request
  * to write the specified file.*/
OSGDB_EXPORT extern bool writeNodeFile(const osg::Node& node, const std::string& filename);


}

#endif
