#include <QApplication>
#include <QFile>
#include <QTextStream>
#include <QSettings>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow win;
	//win.setGeometry(100, 100, 1024, 800);
	//win.showMaximized();
    win.show();

    return a.exec();
}
