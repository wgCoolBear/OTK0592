#include <osgGLUT/Version>

const char* osgGLUTGetVersion()
{
    return "0.9.0";
}


const char* osgGLUTGetLibraryName()
{
    return "Open Scene Graph GLUT Library";
}
