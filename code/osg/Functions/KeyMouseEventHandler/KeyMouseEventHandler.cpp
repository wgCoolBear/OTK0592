#include "KeyMouseEventHandler.h"

bool KeyMouseEventHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	osgViewer::Viewer* viewer = dynamic_cast<osgViewer::Viewer*>(&aa);
	if (!viewer)return false;
	osg::Node* scene = viewer->getSceneData();
	if (!scene) return false;

	switch (ea.getEventType())
	{
		//按键
	case osgGA::GUIEventAdapter::KEYDOWN:
	{
		//Ctrl,Alt,Shift等组合键
		std::cout << ea.getModKeyMask() << std::endl;
		if (ea.getModKeyMask()&osgGA::GUIEventAdapter::MODKEY_CTRL)
		{
			int a0 = 0;
		}

		switch (ea.getKey())
		{
		case osgGA::GUIEventAdapter::KEY_1:
			std::cout << "1" << std::endl;
		case ' ':
			std::cout << "空格" << std::endl;
			break;
		default:
			std::cout << "键盘" << char(ea.getKey()) << "按下" << std::endl;
			break;
		}
		break;
	}
	//鼠标
	case osgGA::GUIEventAdapter::PUSH:
	{
		//Ctrl,Alt,Shift等组合键
		std::cout << ea.getModKeyMask() << std::endl;
		if (ea.getModKeyMask()&osgGA::GUIEventAdapter::MODKEY_SHIFT)
		{
			int a0 = 0;
		}

		if (ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON)
		{
			std::cout << "鼠标左键按下\n";
			osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
		}
		break;
	}
	default:
		break;
	}
	return false;
}
