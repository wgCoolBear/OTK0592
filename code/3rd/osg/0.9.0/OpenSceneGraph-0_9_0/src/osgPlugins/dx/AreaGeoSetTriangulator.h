#ifndef __OSG_AREA_GEOSET_TRIANGULATOR_H
#define __OSG_AREA_GEOSET_TRIANGULATOR_H

#include <osg/GeoSet>

namespace dx {

osg::GeoSet *TriangulateAreaGeoSet( const osg::GeoSet &geoset );

};

#endif
