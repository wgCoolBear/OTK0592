
SET(osgModeling_DIR "" CACHE PATH "Set to base osgModeling install path")

FIND_PATH(osgModeling_INCLUDE_DIR osgModeling/Model
    PATHS
	${osgModeling_DIR}/include
    /usr/include
    /usr/local/include
)
SET( osgModeling_FOUND "NO" )
IF( osgModeling_INCLUDE_DIR)
    SET( osgModeling_FOUND "YES" )
ENDIF( osgModeling_INCLUDE_DIR )