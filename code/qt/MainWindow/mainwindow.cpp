#include "mainwindow.h"
#include "AutoHideMessageBox.h"
#include "Toast.h"
#include "ui_mainwindow.h"
#include "ui_MoveableWidget.h"
#include <QCompleter>
#include <QTimer>
#include <QFileDialog>
#include <QColorDialog>
#include <QMessageBox>

#define USEWebEngine 0

#if USEWebEngine
#if (QT_VERSION >= QT_VERSION_CHECK(5,6,0))
#if _DEBUG
#pragma comment(lib, "Qt5WebEngined.lib")
#pragma comment(lib, "Qt5WebEngineWidgetsd.lib")
#else
#pragma comment(lib, "Qt5WebEngine.lib")
#pragma comment(lib, "Qt5WebEngineWidgets.lib")
#endif
#include <QtWebEngineWidgets/QWebEngineView>
#include <QtWebEngineWidgets/QWebEnginePage>
#endif
#endif
#include "TableHeadViewWithCheckBox.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	
	//showFullScreen();//全屏
	//setWindowFlags(Qt::FramelessWindowHint);
	//statusBar()->hide();
	//ui->mainToolBar->hide();

	//可移动窗口
	QWidget* moveableWidget = new QWidget(this/*, Qt::FramelessWindowHint|Qt::Tool*/);
	//moveableWidget->setAttribute(Qt::WA_TranslucentBackground); 
	uiMoveableWidget = new Ui::MoveableWidgetContainer();
	uiMoveableWidget->setupUi(moveableWidget);
	moveableWidget->show();

	//输入框LineEdit添加自定义按钮
	{
		QAction *action = new QAction(ui->lineEdit_button);
		action->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton));
		QPushButton *button = new QPushButton();
		button->setStyleSheet(
			"background-color: rgba(0,125,255,100); border: 1px solid rgba(255,255,255,100)\n"
			);
		QObject::connect(button, &QPushButton::clicked, [] {
			QMessageBox::information(NULL, QStringLiteral("提示"), "search");
		});
		QHBoxLayout *layout = new QHBoxLayout();
		layout->addStretch();
		layout->addWidget(button);
		layout->setContentsMargins(0, 0, 0, 0);
		ui->lineEdit_button->addAction(action, QLineEdit::LeadingPosition); // QLineEdit::TrailingPosition 在右侧
		ui->lineEdit_button->setPlaceholderText(QStringLiteral("请输入搜索内容"));
		ui->lineEdit_button->setToolTip(QStringLiteral("搜索"));
		ui->lineEdit_button->setLayout(layout);
		//搜索提示
		QStringList list;
		list << "test" << "admin" << " hello";
		QCompleter *comp = new QCompleter(list, this);
		comp->setCaseSensitivity(Qt::CaseInsensitive);
		ui->lineEdit_button->setCompleter(comp);
	}

	//选择文件。注意使用匿名slot时，不能使用显式SIGNAL，要使用类似&QAction::triggered
	QAction* openFileAction = new QAction(this);
	//openFileAction->setIcon(QApplication::style()->standardIcon(QStyle::SP_FileIcon));
	openFileAction->setIcon(QIcon(":/image/UnFold.png"));
	ui->lineEdit_filePath->addAction(openFileAction, QLineEdit::TrailingPosition);
	connect(openFileAction, &QAction::triggered, this, [&](){
		QString filePath = QFileDialog::getOpenFileName(this,QStringLiteral("选择文件"),"",QStringLiteral("(*.txt);;所有文件(*.*)"));
		ui->lineEdit_filePath->setText(filePath);
	});
	//QObject::connect(button, &QPushButton::clicked, someFunction);
	//QObject::connect(listWidget, &QListWidget::currentRowChanged, this, [&](int row){});
	
	//保存文件
	QAction* saveFileAction = new QAction(this);
	saveFileAction->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton));
	ui->lineEdit_saveFilePath->addAction(saveFileAction, QLineEdit::TrailingPosition);
	connect(saveFileAction, &QAction::triggered, this, [&](){
		QString filePath = QFileDialog::getSaveFileName(this,QStringLiteral("保存文件"),"",QStringLiteral("(*.txt);;所有文件(*.*)"));
		ui->lineEdit_saveFilePath->setText(filePath);
	});
	//选择文件夹
	QAction* openDirAction = new QAction(this);
	openDirAction->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOpenButton));
	ui->lineEdit_dir->addAction(openDirAction, QLineEdit::TrailingPosition);
	connect(openDirAction, &QAction::triggered, this, [&](){
		QString dir = QFileDialog::getExistingDirectory(this,QStringLiteral("选择文件夹"),QStringLiteral(""));
		ui->lineEdit_dir->setText(dir);
	});

	//输入框/下拉列表自动补全
	QCompleter* completer = new QCompleter(ui->comboBox->model(), this);
	completer->setCaseSensitivity(Qt::CaseInsensitive);
	//模糊搜索
	completer->setFilterMode(Qt::MatchContains);
	QLineEdit* lineEdit = new QLineEdit(this);
	ui->comboBox->setLineEdit(lineEdit);
	ui->comboBox->setCompleter(completer);
	ui->comboBox->setEditable(true);
	//注意如果在qss中设置combobox的item高度需要
	ui->comboBox->setView(new QListView());

	//弹出右键菜单
	this->setContextMenuPolicy(Qt::CustomContextMenu);
	//connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotPopMenu(QPoint)));
	connect(this, &QWidget::customContextMenuRequested, this, [&](QPoint p){slotPopMenu(p); });

	//有时候QSS无效，可以设置WA_StyledBackground试试
	//moveableButton->setAttribute(Qt::WA_StyledBackground);
	//moveableButton->setStyleSheet("border-image:url(:/image/bg.png)");

	//IP地址（一种是使用InputMask，另一种使用正则表达式控制）
	QRegExp rx("\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b");  //IP的正则表达式
	ui->lineEdit_ip->setValidator(new QRegExpValidator(rx,this));

	//单发定时器
	QTimer::singleShot(200, this, 
		[&] 
	{
		
	});

	//web网页。需要在安装Qt时勾选Qt WebEngine模块
#if (QT_VERSION >= QT_VERSION_CHECK(5,6,0))
#if USEWebEngine
	//QWebEngineView* webView = ui->webEngineView;
	QWebEngineView* webView = new QWebEngineView(this);
	//webView->setGeometry(100, 200, 400, 300);
	ui->gridLayout->addWidget(webView, 12, 3, 1, 1);
	webView->setAttribute(Qt::WA_TranslucentBackground);
	webView->page()->setBackgroundColor(Qt::transparent);
	//webView->page()->setBackgroundColor(QColor(255,255,0,10));
	webView->load(QUrl("http://www.baidu.com/"));
	webView->show();

	//如果需要js交互，例如点击页面中的链接，需要QWebChannel和QWebEnginePage的配合

	//如果要动态的删除webView，在delete webWidget之前，一定要从layout->removeWidget(webWidget)，否则可能引起崩溃
	
	//在onLoadFinished(bool ok)，如果ok==false，可以调用webWidget->stop()隐藏浏览器自身的错误页面
#endif
#endif

	//组件屏蔽鼠标事件
	//ui->pushButton->setAttribute(Qt::WA_TransparentForMouseEvents, true);

	//表格表头第一列带有复选框
	TableHeadViewWithCheckBox* checkBoxHeader = new TableHeadViewWithCheckBox(0, Qt::Horizontal,this);
	ui->tableWidget->setHorizontalHeader(checkBoxHeader);
	connect(checkBoxHeader, &TableHeadViewWithCheckBox::checkStatusChanged, this, 
		[&](bool checked){
		auto rows = ui->tableWidget->rowCount();
		ui->tableWidget->blockSignals(true);
		for (int row = 0; row < rows;row++)
		{
			auto item = ui->tableWidget->item(row, 0);
			item->setCheckState(checked ? Qt::Checked : Qt::Unchecked);
		}
		ui->tableWidget->blockSignals(false);
	});
	//自定义表头排序功能
	connect(checkBoxHeader, SIGNAL(sectionClicked(int)), ui->tableWidget, SLOT(sortByColumn(int)));
	//特别注意在更新(例如添加行)数据的时候，要禁止排序，否则排序结果可能混乱。可以先禁止排序，添加完数据后再启用排序
	//ui->tableWidget->setSortingEnabled(false);
	//ui->tableWidget
	//ui->tableWidget->setSortingEnabled(true);

	//lisgWidget删除item
	ui->listWidget->takeItem(6);

	//搜索树形节点
	{
		QAction* action = new QAction(this);
		action->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOpenButton));
		ui->lineEdit_searchTree->addAction(action, QLineEdit::TrailingPosition);
		//回车
		connect(ui->lineEdit_searchTree, &QLineEdit::returnPressed, this, [&](){});
		connect(action, &QAction::triggered, this, [&](){
			auto key = ui->lineEdit_searchTree->text();
			QTreeWidgetItemIterator it(ui->treeWidget);
			while (*it)
			{
				if ((*it)->text(0).contains(key))
				{
					(*it)->setHidden(false);
					QTreeWidgetItem *item = *it;
					//显示父节点
					while (item->parent())
					{
						item->parent()->setHidden(false);
						item = item->parent();
					}
				}
				else
				{
					//不满足满足条件先隐藏，它的子项目满足条件时会再次让它显示
					(*it)->setHidden(true);
				}
				++it;
			}
		});
	}

	//搜索list节点
	{
		QAction* action = new QAction(this);
		action->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOpenButton));
		ui->lineEdit_searchList->addAction(action, QLineEdit::TrailingPosition);
		//回车
		connect(ui->lineEdit_searchList, &QLineEdit::returnPressed, this, [&](){});
		connect(action, &QAction::triggered, this, [&](){
			auto key = ui->lineEdit_searchList->text();
			auto rows = ui->listWidget->count();
			for (auto r = 0; r < rows; r++)
			{
				auto item = ui->listWidget->item(r);
				auto text = item->text();
				item->setHidden(!text.contains(key, Qt::CaseInsensitive));
			}
		});
	}

	//设置treewidget水平滚动条
	ui->treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui->treeWidget->header()->setStretchLastSection(false);

	//Toast消息提示框
	connect(ui->pushButton_toast, &QPushButton::clicked,
		this, [&](){
		Toast::showTip(QStringLiteral("Toast消息提示框"), this);
	});

	//自动隐藏消息框
	connect(ui->pushButton_autoHideMessageBox, &QPushButton::clicked, 
		this, [&](){
		AutoHideMessageBox msg(NULL, QStringLiteral("AutoHideMessageBox"), QStringLiteral("AutoHideMessageBox"), true, 3 * 1000);
		msg.exec();
	});

}

MainWindow::~MainWindow()
{
    delete ui;
}
//双击表格单元弹出ComboBox
void MainWindow::on_tableWidget_cellDoubleClicked(int row, int col)
{
	QTableWidget* table = ui->tableWidget;
	QComboBox* combo = new QComboBox(table);
	combo->addItem("ABC123");
	combo->addItem("abcdeFG234");
	connect(combo,SIGNAL(activated(int)),this,SLOT(slotOnComboBox(int)));
	table->setCellWidget(row,col,combo);
}

void MainWindow::on_tableWidget_currentCellChanged(int newRow, int newCol, int oldRow, int oldCol)
{
	QTableWidget* table = ui->tableWidget;
	QWidget* cellWidget = table->cellWidget(oldRow,oldCol);
	QComboBox* combo = dynamic_cast<QComboBox*>(cellWidget);
	if(combo)
	{
		QString text = combo->currentText();
		table->setItem(oldRow,oldCol,new QTableWidgetItem(text));
		table->removeCellWidget(oldRow,oldCol);
	}
}

void MainWindow::slotOnComboBox(int idx)
{
	QObject* sender = this->sender();
	QComboBox* combo = dynamic_cast<QComboBox*>(sender);
	if (combo)
	{
		QTableWidget* table = ui->tableWidget;
		int curRow = table->currentRow();
		int curCol = table->currentColumn();
		on_tableWidget_currentCellChanged(0,0,curRow,curCol);
	}
}
//弹出右键菜单
void MainWindow::slotPopMenu(QPoint p)
{
	QMenu* menu = new QMenu(this);
	menu->setWindowFlags(menu->windowFlags()|Qt::FramelessWindowHint);
	menu->setAttribute(Qt::WA_TranslucentBackground);//背景透明
	menu->setStyleSheet(
		"QMenu {background-color: rgba(0,125,255,100); border: 1px solid rgba(255,255,255,100)}\n"
		"QMenu::item { font-size:10pt; background-color: transparent; padding:4px 4px; margin:2px 2px;}\n"
		"QMenu::item:selected { background-color: #2dabf9;}"
		);
	QAction* openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
	openAct->setShortcuts(QKeySequence::Open);
	openAct->setStatusTip(tr("Open an existing file"));
	//connect(openAct, SIGNAL(triggered()), this, SLOT(slotOpen()));
	menu->addAction(openAct);
	menu->addAction(new QAction(QStringLiteral("菜单2"),this));
	//menu->exec(QCursor::pos());//模态阻塞线程
	menu->popup(QCursor::pos());//不阻塞线程
}
//颜色选择按钮
void MainWindow::on_pushButton_color_clicked()
{
	QString styleSheet = ui->pushButton_color->styleSheet();
	styleSheet=styleSheet.mid(styleSheet.indexOf('(')+1, styleSheet.indexOf(')')-styleSheet.indexOf('(')-1);
	QStringList rgba = styleSheet.split(',');
	QColor initColor(rgba[0].toInt(), rgba[1].toInt(), rgba[2].toInt(), rgba[3].toInt());
	QColor color = QColorDialog::getColor(initColor, this, QStringLiteral("选择颜色"),QColorDialog::ShowAlphaChannel);
	if (color.isValid())
	{
		QString ss =QString("background-color:rgba(%1,%2,%3,%4)").arg(color.red()).arg(color.green()).arg(color.blue()).arg(color.alpha());
		ui->pushButton_color->setStyleSheet(ss);
	}
}
