#pragma once
#if USE_QOpenGLWidget
#include "3rd/osgQOpenGL/BaseSceneWidget.h"
#else
#include "3rd/osgQGL/BaseSceneWidget.h"
#endif
#include "osgEarth/MapNode"
#include "osgEarthUtil/EarthManipulator"

class SceneWidget : public BaseSceneWidget
{
	Q_OBJECT
public:
	SceneWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
	osgEarth::Map* getMap();
	osgEarth::MapNode* getMapNode();
#ifdef USE_QOpenGLWidget
	virtual void initializedFromOsgQOpenGLWidget() override;
#endif
	//��������
	void jingHuFly();
	//̫��
	void taiHu();
signals:
	void initialized();
protected:
	void init(osgViewer::Viewer* viewer);
protected:
	osgEarth::Map* _map;
	osgEarth::MapNode* _mapNode;
	osgEarth::Util::EarthManipulator* _em;
};