#include "KeyValueConfig.h"

void readConfigFile(const char * cfgfilepath, std::map<std::string,std::string>& key_value)
{
	std::fstream cfgFile;
	cfgFile.open(cfgfilepath);//打开文件	
	if (!cfgFile.is_open())
	{
		std::cout << "can not open cfg file!" << std::endl;
		return;
	}
	char tmp[1000];
	while (!cfgFile.eof())//循环读取每一行
	{
		cfgFile.getline(tmp, 1000);//每行读取前1000个字符，1000个应该足够了
		std::string line(tmp);
		size_t pos = line.find('=');//找到每行的“=”号位置，之前是key之后是value
		if (pos == std::string::npos) 
			continue;
		std::string key = line.substr(0, pos);//取=号之前
		std::string value = line.substr(pos + 1);//取=号之后
		key_value[key] = value;
		std::cout << key << "=" << value<<std::endl;
	}
	cfgFile.close();
}

void readConfigFile(const char * cfgfilepath, std::map<std::string, std::vector<std::string>>& key_value)
{
	std::fstream cfgFile;
	cfgFile.open(cfgfilepath);//打开文件	
	if (!cfgFile.is_open())
	{
		std::cout << "can not open cfg file!" << std::endl;
		return;
	}
	char tmp[1000];
	while (!cfgFile.eof())//循环读取每一行
	{
		cfgFile.getline(tmp, 1000);//每行读取前1000个字符，1000个应该足够了
		std::string line(tmp);
		size_t pos = line.find('=');//找到每行的“=”号位置，之前是key之后是value
		if (pos == std::string::npos)
			continue;
		std::string key = line.substr(0, pos);//取=号之前
		std::string value = line.substr(pos + 1);//取=号之后
		key_value[key].push_back(value);
		//std::cout << key << "=" << value << std::endl;
	}
	cfgFile.close();
}
