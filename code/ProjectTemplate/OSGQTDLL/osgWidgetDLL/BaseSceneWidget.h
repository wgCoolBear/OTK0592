#pragma once
#include "Export.h"
#include <QWidget>
#include <QGridLayout>
#include <QTimer>
#include "osgViewer\Viewer"
#include "osgDB\ReadFile"
#include "GraphicsWindowQt.h"

class DLL_EXPORT BaseSceneWidget : public QWidget
{
public:
	BaseSceneWidget(QWidget* parent = 0, Qt::WindowFlags f = 0, osgViewer::ViewerBase::ThreadingModel threadingModel = osgViewer::Viewer::SingleThreaded);

	GraphicsWindowQt* createGraphicsWindow(int x, int y, int w, int h, const std::string& name = "", bool windowDecoration = false);

	virtual void paintEvent(QPaintEvent* event) override;

	osgViewer::Viewer* getViewer();
	osg::Group* getRoot();
protected:
	void initScene();
protected:
	osgViewer::Viewer* _viewer;
	osg::Group* _root;
	QTimer _timer;
};