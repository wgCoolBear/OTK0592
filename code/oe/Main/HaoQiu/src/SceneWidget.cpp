#include "SceneWidget.h"
#include <osgEarthUtil/AutoClipPlaneHandler>
#include <osgEarthUtil/EarthManipulator>
#include <osgEarthUtil/GeodeticGraticule>
#include <osgearth/TerrainEngineNode>
#include <osgEarthAnnotation/ModelNode>
#include <osgEarth/GLUtils>
#include <osgEarthAnnotation/ModelNode>
#include <osgEarthDrivers/gdal/GDALOptions>
#include <osgEarthDrivers/xyz/XYZOptions>
#include <osgEarthDrivers/tms/TMSOptions>
#include <osgEarthDrivers/model_feature_geom/FeatureGeomModelOptions>
#include <osgEarthDrivers/cache_filesystem/FileSystemCache>
#include <osgEarth/ModelLayer>
#include <osgEarth/ImageLayer>
#include <osgEarthDrivers/feature_ogr/OGRFeatureOptions>
#include <osgEarthDrivers/agglite/AGGLiteOptions>

#define USE_GAODE_IMAGE 0
#define USE_GOOGLE_IMAGE 0
#define USE_BEIJING 1
#define USE_TEST_TIF 0

SceneWidget::SceneWidget(QWidget* parent /*= 0*/, Qt::WindowFlags f /*= 0*/) 
	: BaseSceneWidget(parent, f)
{
#ifndef USE_QOpenGLWidget
	init(_viewer);
#endif
}

osgEarth::Map* SceneWidget::getMap()
{
	return _map;
}

osgEarth::MapNode* SceneWidget::getMapNode()
{
	return _mapNode;
}

#ifdef USE_QOpenGLWidget
void SceneWidget::initializedFromOsgQOpenGLWidget()
{
	BaseSceneWidget::initializedFromOsgQOpenGLWidget();

	init(getViewer());

	emit initialized();
}
#endif

void SceneWidget::init(osgViewer::Viewer* viewer)
{
	osg::Camera* camera = viewer->getCamera();
	//----------------------在OE2.10中，需要添加以下两行，否则线或者点可能会看不见------------
	// default uniform values:
	osgEarth::GLUtils::setGlobalDefaults(camera->getOrCreateStateSet());
	// disable the small-feature culling
	camera->setSmallFeatureCullingPixelSize(-1.0f);

	osgEarth::MapOptions mapOptions;
	osgEarth::Drivers::FileSystemCacheOptions cacheOptions;
	cacheOptions.rootPath() = "data/tileCache";
	mapOptions.cache() = cacheOptions;
	//mapOptions.profile() = osgEarth::ProfileOptions("plate-carre");//平面模式
	/*  平面模式projected下经纬度高程转世界坐标
	//wgs84坐标
	float lon=118;
	float lat=40;
	osgEarth::GeoPoint gp(osgEarth::SpatialReference::get("wgs84"),
	lon, lat, alt);
	//转换到平面投影坐标
	gp.transformInPlace(osgEarth::SpatialReference::get("plate-carre"));
	gp.toWorld(worldPos);
	*/
	_map = new osgEarth::Map(mapOptions);

	osgEarth::MapNodeOptions mapNodeOptions;
	//mapNodeOptions.enableLighting() = false;
	osgEarth::TerrainOptions terrainOpt;
	//terrainOpt.setDriver("mp");
	mapNodeOptions.setTerrainOptions(terrainOpt);
	_mapNode = new osgEarth::MapNode(_map, mapNodeOptions);

	osgEarth::Drivers::GDALOptions basemapOpt;
	basemapOpt.url() = "data/image/world.tif";
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("basemap", basemapOpt)));

#if USE_GAODE_IMAGE
	osgEarth::Drivers::XYZOptions gaodeOpt;
	gaodeOpt.url() = "http://webst04.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}";
	gaodeOpt.profile() = osgEarth::ProfileOptions("spherical-mercator");
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("高德影像", gaodeOpt)));
	/*<image name="mapbox_satellite" driver="xyz">
	<url>http://webst04.is.autonavi.com/appmaptile?style=6&amp;x={x}&amp;y={y}&amp;z={z}</url>
	<profile>spherical-mercator</profile>
	</image>*/
#endif

#if USE_GOOGLE_IMAGE
	osgEarth::Drivers::XYZOptions googleOpt;
	googleOpt.url() = "http://mt2.google.cn/vt/lyrs=s&hl=zh-CN&gl=cn&x={x}&y={y}&z={z}";
	googleOpt.profile() = osgEarth::ProfileOptions("spherical-mercator");
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("google影像", googleOpt)));
#endif

#if USE_BEIJING
	osgEarth::Drivers::TMSOptions bjImg, bjEle;
	bjImg.url() = "data/tms/beijing/BeiJingImagery/tms.xml";
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("bjImg", bjImg)));
	bjEle.url() = "data/tms/beijing/BeiJingElevation/tms.xml";
	_map->addLayer(new osgEarth::ElevationLayer(osgEarth::ElevationLayerOptions("bjEle", bjEle)));
#endif

#if USE_TEST_TIF
	osgEarth::Drivers::GDALOptions ele;
	ele.url() = "data/dem/ASTGTMV003_N39E116_dem.tif";
	_map->addLayer(new osgEarth::ElevationLayer(osgEarth::ElevationLayerOptions("ele", ele)));
#endif

	osgEarth::Symbology::Style modelStyle;
	osg::ref_ptr<osg::Node> model3D = osgDB::readRefNodeFile("data/model/axes.osgb.1000000.scale");
	modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->setModel(model3D);
	//modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->url() = std::string("data/model/red_flag.osg.100000.scale");
	auto modelNode = new osgEarth::Annotation::ModelNode(_mapNode, modelStyle);
	modelNode->setPosition(osgEarth::GeoPoint(_mapNode->getMapSRS(), 118.13, 45));
	//_mapNode->addChild(modelNode);

	_root->addChild(_mapNode);

	//近地面自动裁剪，这样可使地面的模型不被裁剪。但是会裁剪包围盒大于地球的东西例如卫星轨道
	//所以动态使用AutoClipPlaneCullCallback是个好的选择
	//camera->addCullCallback(new osgEarth::Util::AutoClipPlaneCullCallback(_mapNode));

	// 绘制经纬度网格
	osgEarth::Util::GeodeticGraticule* gr = new osgEarth::Util::GeodeticGraticule();
	//_mapNode->getMap()->addLayer(gr);

	osgEarth::Util::EarthManipulator* em = new osgEarth::Util::EarthManipulator();
	viewer->setCameraManipulator(em);
	osgEarth::Viewpoint viewpoint("", 116.393714, 39.9184, 15, -2.50, -90.0, 1.5e7);
	em->setViewpoint(viewpoint);
	em->setHomeViewpoint(viewpoint);

	viewer->home();
}
