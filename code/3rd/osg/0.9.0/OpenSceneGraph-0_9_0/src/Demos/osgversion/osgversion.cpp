#include <stdio.h>
#include <osg/Version>


int main( int, char **)
{
    printf( "%s\n", osgGetVersion() );
    return 0;
}
