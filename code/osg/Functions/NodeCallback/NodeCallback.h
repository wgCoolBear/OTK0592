#include "Export.h"
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osg/Camera>

//实现旋转功能的回调
/************************************************************************/
/* 	osg::MatrixTransform* rotMT = new osg::MatrixTransform();
	rotMT->addChild(model);
	rotMT->addUpdateCallback(new RotCallback());
/************************************************************************/
class DLL_EXPORT RotCallback : public osg::NodeCallback
{
public:
	RotCallback();
	// node节点是当前回调所挂载的节点
	virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);
private:
	double startTime;
};

//相机绘制回调
class DLL_EXPORT ReadFrameBufferCameraDrawCallback : public osg::Camera::DrawCallback
{
public:
	ReadFrameBufferCameraDrawCallback(osg::Image* img);
	virtual void operator()(const osg::Camera& camera) const;
private:
	osg::Image* _img;
};

/*
https://blog.csdn.net/csxiaoshui/article/details/78136677
osg::Drawable在3.4版本之前继承自osg::Object，在3.4版本以后继承自osg::Node，
在osg3.4版本之前，osg::Drawable使用的回调和osg::Node使用的回调是不同的，
在3.4版本之后，由于osg::Drawable继承自osg::Node，因此回调函数将二者统一起来了。
具体来说，在3.4版本之前，编写回调的方式：
Node节点的回调 (更新、事件、筛选回调:三种都是使用NodeCallback）
class MyNodeCallback : public osg::NodeCallback
{
	//必须实现的成员
	virtual void  operator() (Node *node, NodeVisitor *nv)；
}
//设置回调的函数：
void  setUpdateCallback(NodeCallback *nc);
void  setEventCallback(NodeCallback *nc);
void  setCullCallback(NodeCallback *nc);
//-------------------------------------------------------------

//Drawable节点的回调（更新回调）
class MyDrawableUpdateCallback : public osg::Drawable::UpdateCallback
{
	//必须实现的成员
	virtual void  update(osg::NodeVisitor *, osg::Drawable *);
}
//Drawable节点的回调（事件回调）
class MyDrawableEventCallback ： public osg::Drawable::EventCallback
{
	//必须实现的成员
	virtual void  event(osg::NodeVisitor *, osg::Drawable *);
}
//Drawable节点的回调（筛选回调）
class MyDrawableCullCallback : public osg::Drawable::CullCallback
{
	//必须实现的成员
	virtual bool  cull(osg::NodeVisitor *nv, osg::Drawable *drawable, osg::RenderInfo *renderInfo) const;
}
//Drawable节点的回调（绘制[渲染]回调）
public MyDrawableDrawCallback : public osg::Drawable::DrawCallback
{
	//必须实现的成员
	virtual void  drawImplementation(osg::RenderInfo &, const osg::Drawable *) const;
}
可以看到在3.4版本之前的处理方式：
1. osg::Node有3种回调（对应一帧渲染过程中的3个阶段）：更新、事件、筛选
2. osg::Drawable有4种回调（相比Node多出来一个绘制，因为Drawable就是场景中可绘制的对象，自然比Node多一个渲染过程）
3. 每一个不同的回调类都需要实现特定的方法，这个方法会在回调过程中被调用。

在3.4版本之后，OSG由于将Drawable的父类修改为osg::Node，也同时调整了回调的类继承结构，
现在OSG中大部分的回调都继承自一个统一的回调类——osg::Callback，同时由于调整了Node类中加载回调的函数参数，
使得这些函数可以被Drawable使用，也就是将参数统一调整为Callback类型：
void  setUpdateCallback (Callback *nc);
void  setEventCallback (Callback *nc);
void  setCullCallback (Callback *nc) ;
这样Drawable类中不需要重新实现这些函数，同时Drawable自己的UpdateCallback由于继承自Callback，
也可以传入到这些函数中。
新的Callback类需要所有继承它的类都实现一个方法
virtual bool  run (osg::Object *object, osg::Object *data)
这个方法会在回调中被调用。这样就省去了3.1种每一个不同的回调类都有一个需要实现的方法（并且这些方法名字不同）。
在3.4版本中，原来的osg::NodeCallback被标记为Deprecated。
OSG通过修改Drawable的继承关系，同时调整了回调的类结构，让整个代码看起来更加清晰，代码的复用率更高。
*/