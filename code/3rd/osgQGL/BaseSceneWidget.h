#pragma once
#include "Export.h"
#include <QWidget>
#include <QGridLayout>
#include <QTimer>
#include "osgViewer\Viewer"
#include "osgDB\ReadFile"
#include "GraphicsWindowQt.h"
/************************************************************************/
/* 如果想在Qt中【不】使用OSG的osgViewer::ViewerBase::SingleThreaded单线程模式
/* 可以参考：https://blog.csdn.net/hankern/article/details/81734709?utm_source=blogxgwz5
	连接中的工程代码在doc\Qt5QOpenglContext.zip
/* qt5+osg多线程的解决方案
	问题描述：
	Cannot make QOpenGLContext current in a different thread
	解决思路：
	在主线程中将qt窗体中的QOpenglContext moveToThread到窗体线程中，这样窗体线程在执行makecurrent时就不会报错。
	moveToThread必须在QOpenglContext原来所在的线程中执行，否则会失败，提示以上问题信息。
	以下工程文件中还解决了单线程模式中出现的QOpenGLContext::swapBuffers() called with non-exposed window, behavior is undefined告警，
	这主要是因为osg进入帧循环后，相应的窗体还没有显示出来。
	补充说明：
	开发环境为win7x64 qt5.4 osg3.3 vs2010
	工程文件：https://pan.baidu.com/s/1DZpn9bahTqiuAc88w5HThw
	————————————————
	版权声明：本文为CSDN博主「hankern」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
	原文链接：https://blog.csdn.net/hankern/article/details/81734709
/************************************************************************/
class DLL_EXPORT BaseSceneWidget : public QWidget
{
public:
	BaseSceneWidget(QWidget* parent = 0, Qt::WindowFlags f = 0, osgViewer::ViewerBase::ThreadingModel threadingModel = osgViewer::Viewer::SingleThreaded);

	GraphicsWindowQt* createGraphicsWindow(int x, int y, int w, int h, const std::string& name = "", bool windowDecoration = false);

	virtual void paintEvent(QPaintEvent* event) override;

	osgViewer::Viewer* getViewer();
	osg::Group* getRoot();
protected:
	void initScene();
protected:
	osgViewer::Viewer* _viewer;
	osg::Group* _root;
	QTimer _timer;
};