#include <osgDB/ReadFile>
#include <osgDB/FileUtils>

#include <osg/ArgumentParser>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>


int main(int argc, char **argv)
{
	//抗锯齿
	osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
	ds->setNumMultiSamples(8);
	
    // use an ArgumentParser object to manage the program arguments.
    osg::ArgumentParser arguments(&argc, argv);

    // set up the usage document, in case we need to print out how to use this program.
    arguments.getApplicationUsage()->setApplicationName(arguments.getApplicationName());
    arguments.getApplicationUsage()->setDescription(arguments.getApplicationName() + " is an OpenSceneGraph example.");
    arguments.getApplicationUsage()->setCommandLineUsage(arguments.getApplicationName() + " data path.");
    arguments.getApplicationUsage()->addCommandLineOption("-h or --help", "Display this information");
    //arguments.getApplicationUsage()->addCommandLineOption("-arg1", "arg1");

    // construct the viewer.
    osgViewer::Viewer viewer;

    // if user request help write it out to cout.
    if (arguments.read("-h") || arguments.read("--help"))
    {
        arguments.getApplicationUsage()->write(std::cout);
        return 1;
    }

    //double arg1 = 0.25;
    //arguments.read("-arg1", arg1);

    osg::ref_ptr<osg::Group> root = new osg::Group;

    // read the scene from the list of file specified command line args.
    osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFiles(arguments);
    // if not loaded assume no arguments passed in, try use default mode instead.
	if (!loadedModel) loadedModel = osgDB::readNodeFile("../../../data/model/cow.osgb");
    // if no model has been successfully loaded report failure.
    if (!loadedModel)
    {
        std::cout << arguments.getApplicationName() << ": No data loaded" << std::endl;
	}
	root->addChild(loadedModel);

    // pass the loaded scene graph to the viewer.
    viewer.setSceneData(root.get());

    // add the state manipulator
    viewer.addEventHandler(new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()));
    // add the thread model handler
    viewer.addEventHandler(new osgViewer::ThreadingHandler);
    // add the window size toggle handler
    viewer.addEventHandler(new osgViewer::WindowSizeHandler);
    // add the stats handler
    viewer.addEventHandler(new osgViewer::StatsHandler);
    // add the help handler
    viewer.addEventHandler(new osgViewer::HelpHandler(arguments.getApplicationUsage()));
    // add the record camera path handler
    viewer.addEventHandler(new osgViewer::RecordCameraPathHandler);
    // add the LOD Scale handler
    viewer.addEventHandler(new osgViewer::LODScaleHandler);
    // add the screen capture handler
    viewer.addEventHandler(new osgViewer::ScreenCaptureHandler);

    // create the windows and run the threads.
    viewer.realize();

    return viewer.run();
}