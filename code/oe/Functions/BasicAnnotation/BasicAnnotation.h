#pragma once
#include "Export.h"
#include <osg/Group>
#include "osgEarth/MapNode"
//绘制基础的点、线、圆圈、图文标签、模型等，可参考osgearth_annotation.cpp
class DLL_EXPORT BasicAnnotation:public osg::Group
{
public:
	BasicAnnotation(osgEarth::MapNode* mapNode);
	~BasicAnnotation();
};

