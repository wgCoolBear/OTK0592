#include <osgUtil/Version>

const char* osgUtilGetVersion()
{
    return "0.9.0";
}


const char* osgUtilGetLibraryName()
{
    return "Open Scene Graph Utility Library";
}
