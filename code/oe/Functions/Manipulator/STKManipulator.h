#pragma once
#include "Export.h"
#include <osgGA/CameraManipulator>

/************************************************************************/
/* 模仿STK操纵器
/* Shift键：以当前视点位置为球心旋转
/* Ctrl键：限制在垂直方向旋转
/* Alt键：限制在水平方向旋转
/************************************************************************/
class DLL_EXPORT STKManipulator:public osgGA::CameraManipulator
{
public:
	STKManipulator();
	virtual ~STKManipulator();

	virtual void setByMatrix(const osg::Matrixd& matrix) override;

	virtual void setByInverseMatrix(const osg::Matrixd& matrix) override;

	virtual osg::Matrixd getMatrix() const override;

	virtual osg::Matrixd getInverseMatrix() const override;

	virtual void home( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us ) override;

	virtual void init(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& us) override;

	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us) override;
protected:
	void addMouseEvent( const osgGA::GUIEventAdapter& ea );
	void flushMouseEventStack();
	// internal event stack comprising last two mouse events.
	osg::ref_ptr< const osgGA::GUIEventAdapter > _ga_t1;
	osg::ref_ptr< const osgGA::GUIEventAdapter > _ga_t0;

	osg::Vec3d _eye;
	osg::Vec3d _center;
	osg::Vec3d _up;

	//视线与球面交点
	double _lon;
	double _lat;
};