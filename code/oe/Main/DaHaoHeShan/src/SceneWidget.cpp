#include "SceneWidget.h"
#include <osgEarthUtil/AutoClipPlaneHandler>
#include <osgEarthUtil/GeodeticGraticule>
#include <osgearth/TerrainEngineNode>
#include <osgEarthAnnotation/ModelNode>
#include <osgEarth/GLUtils>
#include <osgEarthAnnotation/ModelNode>
#include <osgEarthDrivers/gdal/GDALOptions>
#include <osgEarthDrivers/xyz/XYZOptions>
#include <osgEarthDrivers/tms/TMSOptions>
#include <osgEarthDrivers/model_feature_geom/FeatureGeomModelOptions>
#include <osgEarthDrivers/cache_filesystem/FileSystemCache>
#include <osgEarth/ModelLayer>
#include <osgEarth/ImageLayer>
#include <osgEarthDrivers/feature_ogr/OGRFeatureOptions>
#include <osgEarthDrivers/agglite/AGGLiteOptions>
#include "osgEarth/Version"
#if OSGEARTH_VERSION_GREATER_OR_EQUAL(2,10,0)
#include <osgEarthFeatures/FeatureModelLayer>
#include <osgEarthFeatures/ConvertTypeFilter>
#endif

#define USE_GAODE_IMAGE 0
#define USE_GOOGLE_IMAGE 0
#define USE_BEIJING 0
#define USE_TEST_TIF 0

SceneWidget::SceneWidget(QWidget* parent /*= 0*/, Qt::WindowFlags f /*= 0*/) 
	: BaseSceneWidget(parent, f)
{
#ifndef USE_QOpenGLWidget
	init(_viewer);
#endif
}

osgEarth::Map* SceneWidget::getMap()
{
	return _map;
}

osgEarth::MapNode* SceneWidget::getMapNode()
{
	return _mapNode;
}

#ifdef USE_QOpenGLWidget
void SceneWidget::initializedFromOsgQOpenGLWidget()
{
	BaseSceneWidget::initializedFromOsgQOpenGLWidget();

	init(getViewer());

	emit initialized();
}

#endif

void SceneWidget::jingHuFly()
{

}

void SceneWidget::taiHu()
{
	osgEarth::Viewpoint viewpoint("", 120.3, 31.2, 15, -2.50, -90.0, 1e5);
	_em->setViewpoint(viewpoint, 1);
}

void SceneWidget::init(osgViewer::Viewer* viewer)
{
	osg::Camera* camera = viewer->getCamera();
	//----------------------在OE2.10中，需要添加以下两行，否则线或者点可能会看不见------------
	// default uniform values:
	osgEarth::GLUtils::setGlobalDefaults(camera->getOrCreateStateSet());
	// disable the small-feature culling
	camera->setSmallFeatureCullingPixelSize(-1.0f);

	osgEarth::MapOptions mapOptions;
	osgEarth::Drivers::FileSystemCacheOptions cacheOptions;
	cacheOptions.rootPath() = "data/tileCache";
	mapOptions.cache() = cacheOptions;
	//mapOptions.profile() = osgEarth::ProfileOptions("plate-carre");//平面模式
	/*  平面模式projected下经纬度高程转世界坐标
	//wgs84坐标
	float lon=118;
	float lat=40;
	osgEarth::GeoPoint gp(osgEarth::SpatialReference::get("wgs84"),
	lon, lat, alt);
	//转换到平面投影坐标
	gp.transformInPlace(osgEarth::SpatialReference::get("plate-carre"));
	gp.toWorld(worldPos);
	*/
	_map = new osgEarth::Map(mapOptions);

	osgEarth::MapNodeOptions mapNodeOptions;
	//mapNodeOptions.enableLighting() = false;
	osgEarth::TerrainOptions terrainOpt;
	//terrainOpt.setDriver("mp");
	mapNodeOptions.setTerrainOptions(terrainOpt);
	_mapNode = new osgEarth::MapNode(_map, mapNodeOptions);

	osgEarth::Drivers::GDALOptions basemapOpt;
	basemapOpt.url() = "data/image/world.tif";
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("basemap", basemapOpt)));

#if USE_GAODE_IMAGE
	osgEarth::Drivers::XYZOptions gaodeOpt;
	gaodeOpt.url() = "http://webst04.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}";
	gaodeOpt.profile() = osgEarth::ProfileOptions("spherical-mercator");
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("高德影像", gaodeOpt)));
	/*<image name="mapbox_satellite" driver="xyz">
	<url>http://webst04.is.autonavi.com/appmaptile?style=6&amp;x={x}&amp;y={y}&amp;z={z}</url>
	<profile>spherical-mercator</profile>
	</image>*/
#endif

#if USE_GOOGLE_IMAGE
	osgEarth::Drivers::XYZOptions googleOpt;
	googleOpt.url() = "http://mt2.google.cn/vt/lyrs=s&hl=zh-CN&gl=cn&x={x}&y={y}&z={z}";
	googleOpt.profile() = osgEarth::ProfileOptions("spherical-mercator");
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("google影像", googleOpt)));
#endif

#if USE_BEIJING
	osgEarth::Drivers::TMSOptions bjImg, bjEle;
	bjImg.url() = "data/tms/beijing/BeiJingImagery/tms.xml";
	_map->addLayer(new osgEarth::ImageLayer(osgEarth::ImageLayerOptions("bjImg", bjImg)));
	bjEle.url() = "data/tms/beijing/BeiJingElevation/tms.xml";
	_map->addLayer(new osgEarth::ElevationLayer(osgEarth::ElevationLayerOptions("bjEle", bjEle)));
#endif

#if USE_TEST_TIF
	osgEarth::Drivers::GDALOptions ele;
	ele.url() = "data/dem/ASTGTMV003_N39E116_dem.tif";
	_map->addLayer(new osgEarth::ElevationLayer(osgEarth::ElevationLayerOptions("ele", ele)));
#endif

	osgEarth::Symbology::Style modelStyle;
	osg::ref_ptr<osg::Node> model3D = osgDB::readRefNodeFile("data/model/axes.osgb.1000000.scale");
	modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->setModel(model3D);
	//modelStyle.getOrCreate<osgEarth::Symbology::ModelSymbol>()->url() = std::string("data/model/red_flag.osg.100000.scale");
	auto modelNode = new osgEarth::Annotation::ModelNode(_mapNode, modelStyle);
	modelNode->setPosition(osgEarth::GeoPoint(_mapNode->getMapSRS(), 118.13, 45));
	//_mapNode->addChild(modelNode);

	osgEarth::Drivers::OGRFeatureOptions featureOptions;
	//featureOptions.url() = "data/shp/World/world.shp";
	//featureOptions.url() = "data/shp/中华人民共和国/中华人民共和国.shp";
	featureOptions.url() = "data/shp/公路/road.shp";
	featureOptions.url() = "data/shp/铁路/railway.shp";
	featureOptions.url() = "data/shp/河湖/river.shp";
	featureOptions.url() = "data/shp/河湖/lake.shp";
#if OSGEARTH_VERSION_GREATER_OR_EQUAL(2,10,0)
	// Make a feature source layer and add it to the Map:
	osgEarth::Features::FeatureSourceLayerOptions ogrLayer;
	ogrLayer.name() = "vector-data";
	ogrLayer.featureSource() = featureOptions;
	_map->addLayer(new osgEarth::Features::FeatureSourceLayer(ogrLayer));
	bool useDraping = true;//贴地
	bool useClamping = false;
	bool useRaster = false;
	// Define a style for the feature data. Since we are going to render the
	// vectors as lines, configure the line symbolizer:
	osgEarth::Symbology::Style style;
	osgEarth::Symbology::LineSymbol* ls = style.getOrCreateSymbol<osgEarth::Symbology::LineSymbol>();
	ls->stroke()->color() = osgEarth::Symbology::Color::Yellow;
	ls->stroke()->width() = 2.0f;
	ls->tessellationSize()->set(100, osgEarth::Symbology::Units::KILOMETERS);
	if (useDraping)
	{
		osgEarth::Symbology::AltitudeSymbol* alt = style.getOrCreate<osgEarth::Symbology::AltitudeSymbol>();
		alt->clamping() = alt->CLAMP_TO_TERRAIN;
		alt->technique() = alt->TECHNIQUE_DRAPE;
	}
	else if (useClamping)
	{
		osgEarth::Symbology::AltitudeSymbol* alt = style.getOrCreate<osgEarth::Symbology::AltitudeSymbol>();
		alt->clamping() = alt->CLAMP_TO_TERRAIN;
		alt->technique() = alt->TECHNIQUE_GPU;
		ls->tessellationSize()->set(100, osgEarth::Symbology::Units::KILOMETERS);
		osgEarth::Symbology::RenderSymbol* render = style.getOrCreate<osgEarth::Symbology::RenderSymbol>();
		render->depthOffset()->enabled() = true;
	}
	if (useRaster)
	{
		osgEarth::Drivers::AGGLiteOptions rasterOptions;
		rasterOptions.featureOptions() = featureOptions;
		rasterOptions.styles() = new osgEarth::Symbology::StyleSheet();
		rasterOptions.styles()->addStyle(style);
		_map->addLayer(new osgEarth::ImageLayer("My Features", rasterOptions));
	}
	else //if (useGeom)
	{
		//省界线
		osgEarth::Drivers::FeatureModelLayerOptions fml;
		fml.name() = "My Features";
		fml.featureSourceLayer() = "vector-data";
		fml.styles() = new osgEarth::Symbology::StyleSheet();
		fml.styles()->addStyle(style);
		fml.enableLighting() = false;
		_map->addLayer(new osgEarth::Drivers::FeatureModelLayer(fml));
		if (true)//显示文字，注意如果text->content()字段填写错误会导致程序崩溃
		{
			// set up symbology for drawing labels. We're pulling the label
			// text from the name attribute, and its draw priority from the
			// population attribute.
			osgEarth::Symbology::Style labelStyle;
			osgEarth::Symbology::TextSymbol* text = labelStyle.getOrCreateSymbol<osgEarth::Symbology::TextSymbol>();
			//world.shp
			//text->content() = osgEarth::Symbology::StringExpression("[cntry_name]");
			//text->priority() = osgEarth::Symbology::NumericExpression("[pop_cntry]");
			//文字字段
			text->content() = osgEarth::Symbology::StringExpression("[NAME]");
			//注意shp的Encoding要设置为UTF8格式的才能正确处理中文
			text->encoding() = osgEarth::Symbology::TextSymbol::ENCODING_UTF8;
			text->font() = "simsun.ttc";
			text->size() = 16.0f;
			text->alignment() = osgEarth::Symbology::TextSymbol::ALIGN_CENTER_CENTER;
			text->fill()->color() = osgEarth::Symbology::Color::White;
			text->halo()->color() = osgEarth::Symbology::Color::DarkGray;
			// and configure a model layer:
			osgEarth::Drivers::FeatureModelLayerOptions fml;
			fml.name() = "Labels";
			fml.featureSourceLayer() = "vector-data";
			fml.styles() = new osgEarth::Symbology::StyleSheet();
			fml.styles()->addStyle(labelStyle);
			_map->addLayer(new osgEarth::Drivers::FeatureModelLayer(fml));
		}
	}
#endif

	_root->addChild(_mapNode);

	//近地面自动裁剪，这样可使地面的模型不被裁剪。但是会裁剪包围盒大于地球的东西例如卫星轨道
	//所以动态使用AutoClipPlaneCullCallback是个好的选择
	//camera->addCullCallback(new osgEarth::Util::AutoClipPlaneCullCallback(_mapNode));

	// 绘制经纬度网格
	osgEarth::Util::GeodeticGraticule* gr = new osgEarth::Util::GeodeticGraticule();
	//_mapNode->getMap()->addLayer(gr);

	_em = new osgEarth::Util::EarthManipulator();
	viewer->setCameraManipulator(_em);
	osgEarth::Viewpoint viewpoint("", 116.393714, 39.9184, 15, -2.50, -90.0, 1.5e7);
	_em->setViewpoint(viewpoint);
	_em->setHomeViewpoint(viewpoint);

	viewer->home();
}
