#pragma once
#include <QTreeWidget>
#include <QMouseEvent>

//点击空白取消item选中
class TreeWidget : public QTreeWidget
{
public:
	TreeWidget(QWidget* parent = NULL);
protected:
	virtual void mousePressEvent(QMouseEvent *event) override;
};