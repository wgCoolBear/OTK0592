// CommentRecord.cpp

#include "flt.h"
#include "Registry.h"
#include "CommentRecord.h"

using namespace flt;

////////////////////////////////////////////////////////////////////
//
//                          CommentRecord
//
////////////////////////////////////////////////////////////////////

RegisterRecordProxy<CommentRecord> g_CommentRecordProxy;

CommentRecord::CommentRecord()
{
}


// virtual
CommentRecord::~CommentRecord()
{
}


// virtual
void CommentRecord::endian()
{
}
