#include <osgText/Version>

const char* osgTextGetVersion()
{
    return "0.9.0";
}


const char* osgTextGetLibraryName()
{
    return "Open Scene Graph Text Library";
}
