#pragma once
#include "Export.h"
#include <osgViewer/Viewer>
#include <osgGA/GUIEventHandler>
#include <osg/io_utils>
#include <iostream>

class DLL_EXPORT KeyMouseEventHandler :public osgGA::GUIEventHandler
{
public:
	KeyMouseEventHandler() {}

	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
};

