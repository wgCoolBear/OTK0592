#include "Shader.h"
#include "osgEarth\VirtualProgram"

osgEarth::VirtualProgram* vp=NULL;

void Shader::AddShaderOnEarth(osgEarth::MapNode* mapNode)
{
	char s_hazeVertShader[] =
		"#version " GLSL_VERSION_STR "\n"
		"varying vec3 v_pos; \n"
		"void setup_haze(inout vec4 VertexVIEW) \n"
		"{ \n"
		"    v_pos = vec3(VertexVIEW); \n"
		"} \n";
	char s_hazeFragShader[] =
		"#version " GLSL_VERSION_STR "\n"
		"varying vec3 v_pos; \n"
		"void apply_haze(inout vec4 color) \n"
		"{ \n"
		"    float dist = clamp( length(v_pos)/1e7, 0.0, 0.75 ); \n"
		"    color = mix(color, vec4(0.5, 0.5, 0.5, 1.0), dist); \n"
		"} \n";
	vp = new osgEarth::VirtualProgram();
	vp->setFunction( "setup_haze", s_hazeVertShader, osgEarth::ShaderComp::LOCATION_VERTEX_VIEW );
	vp->setFunction( "apply_haze", s_hazeFragShader, osgEarth::ShaderComp::LOCATION_FRAGMENT_COLORING );
	vp->setShaderLogging(true, "shaders.txt");
	mapNode->getOrCreateStateSet()->setAttributeAndModes( vp, osg::StateAttribute::ON );
}

void Shader::RemoveShader(osgEarth::MapNode* mapNode)
{
	mapNode->getOrCreateStateSet()->removeAttribute( vp );
}
