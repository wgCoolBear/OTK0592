#include "AnimationPath.h"
#include "osg/AnimationPath"
#include "osg/MatrixTransform"

AnimationPath::AnimationPath(osg::Node* model)
{
	osg::AnimationPath* animationPath = new osg::AnimationPath;
	animationPath->setLoopMode(osg::AnimationPath::LOOP);

	osg::Vec3 center;
	float radius=2;
	float looptime=5;
	int numSamples = 40;
	float yaw = 0.0f;
	float yaw_delta = 2.0f*osg::PI/((float)numSamples-1.0f);
	float roll = osg::inDegrees(30.0f);
	double time=0.0f;
	double time_delta = looptime/(double)numSamples;
	for(int i=0;i<numSamples;++i)
	{
		osg::Vec3 position(center+osg::Vec3(sinf(yaw)*radius,cosf(yaw)*radius,0.0f));
		osg::Quat rotation(osg::Quat(roll,osg::Vec3(0.0,1.0,0.0))*osg::Quat(-(yaw+osg::inDegrees(180.0f)),osg::Vec3(0.0,0.0,1.0)));

		animationPath->insert(time,osg::AnimationPath::ControlPoint(position,rotation));

		yaw += yaw_delta;
		time += time_delta;
	}

	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->setUpdateCallback(new osg::AnimationPathCallback(animationPath, 0.0, 1.0));
	mt->addChild(model);

	addChild(mt);
}

AnimationPath::~AnimationPath()
{

}
