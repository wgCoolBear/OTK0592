#include "UpdateGeometry.h"
#include "osg\Geode"
#include "osg\Geometry"

UpdateGeometry::UpdateGeometry()
{
	osg::Geode* geode = new osg::Geode();
	this->addChild(geode);
	osg::Geometry* linesGeom = new osg::Geometry();
	osg::Vec3 myCoords[] =
	{
		osg::Vec3(-1, 0, 0),
		osg::Vec3(1, 0, 0),
		osg::Vec3(0, 0, 1),
	};
	int numCoords = sizeof(myCoords) / sizeof(osg::Vec3);
	osg::Vec3Array* vertices = new osg::Vec3Array(numCoords, myCoords);
	linesGeom->setVertexArray(vertices);
	osg::Vec4Array* colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f));
	linesGeom->setColorArray(colors, osg::Array::BIND_OVERALL);
	osg::Vec3Array* normals = new osg::Vec3Array;
	normals->push_back(osg::Vec3(0.0f, -1.0f, 0.0f));
	linesGeom->setNormalArray(normals, osg::Array::BIND_OVERALL);
	linesGeom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, 0, numCoords));
	geode->addDrawable(linesGeom);
	geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	//如果几何体数据经常变动，选择使用VBO绘制
	linesGeom->setDataVariance(osg::Object::DYNAMIC);
	linesGeom->setUseVertexBufferObjects(true);
}

UpdateGeometry::~UpdateGeometry()
{

}

void UpdateGeometry::changeColor(osg::Vec4 newColor)
{
	osg::Geode* geode = this->getChild(0)->asGeode();
	osg::Geometry *geometry = geode->getDrawable(0)->asGeometry();
	//顶点数据
	osg::Vec3Array* vertices = dynamic_cast<osg::Vec3Array*>(geometry->getVertexArray());
	//法向量
	osg::Vec3Array* normals = dynamic_cast<osg::Vec3Array*>(geometry->getNormalArray());
	//颜色
	osg::Vec4Array* color = dynamic_cast<osg::Vec4Array*>(geometry->getColorArray());
	(*color)[0] = newColor;
	//如果不使用VBO而是显示列表，那么更新显示列表
	//geometry->dirtyDisplayList();
	//如果使用VBO来更新几何对象，需要使用Vec3Array::dirty()来将修改结果通知给VBO对象
	color->dirty();

	//如果要清空全部顶点的话
	//vertices->clear();
	//vertices->dirty();
	//_drawArrays->set(sog::PrimitiveSet::POINTS, 0, 0);
}
