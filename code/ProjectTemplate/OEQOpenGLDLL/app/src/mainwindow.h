#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
	void on_action_quit_triggered();	//退出,on_action_*_triggered语法中，"quit"和mainwindow.ui中的action对应，这样就不用显示的声明connect了
private:
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
