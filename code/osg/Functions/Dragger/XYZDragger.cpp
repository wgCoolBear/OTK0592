
#include "XYZDragger.h"
#include <osgManipulator/Command>

#include <osg/ShapeDrawable>
#include <osg/Geometry>
#include <osg/LineWidth>
#include <osg/Material>
#include <osgGA/EventVisitor>
#include <osgViewer/View>

using namespace osgManipulator;

// 重写pi是否包含一个node
bool contains(PointerInfo pi, const osg::Node* node)
{
	for (osgManipulator::PointerInfo::IntersectionList::iterator itrHit = pi._hitList.begin();
	itrHit != pi._hitList.end();		++itrHit)
	{
		if (std::find((*itrHit).first.begin(), (*itrHit).first.end(), node) != (*itrHit).first.end())
			return true;
	}

	return false;
}


OneDDragger::OneDDragger() : Translate1DDragger()
{
    _projector = new LineProjector;
    setColor(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
    setPickColor(osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f));
}

OneDDragger::OneDDragger(const osg::Vec3d& s, const osg::Vec3d& e) : Translate1DDragger()
{
    _projector = new LineProjector(s,e);
    setColor(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
    setPickColor(osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f));
}

OneDDragger::~OneDDragger()
{
}

bool OneDDragger::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	if (ea.getHandled()) return false;

	osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
	if (!view) return false;

	bool handled = false;

	bool activationPermitted = true;
	if (_activationModKeyMask != 0 || _activationMouseButtonMask != 0 || _activationKeyEvent != 0)
	{
		_activationPermittedByModKeyMask = (_activationModKeyMask != 0) ?
			((ea.getModKeyMask() & _activationModKeyMask) != 0) :
			false;

		_activationPermittedByMouseButtonMask = (_activationMouseButtonMask != 0) ?
			((ea.getButtonMask() & _activationMouseButtonMask) != 0) :
			false;

		if (_activationKeyEvent != 0)
		{
			switch (ea.getEventType())
			{
			case osgGA::GUIEventAdapter::KEYDOWN:
			{
				if (ea.getKey() == _activationKeyEvent) _activationPermittedByKeyEvent = true;
				break;
			}
			case osgGA::GUIEventAdapter::KEYUP:
			{
				if (ea.getKey() == _activationKeyEvent) _activationPermittedByKeyEvent = false;
				break;
			}
			default:
				break;
			}
		}

		activationPermitted = _activationPermittedByModKeyMask || _activationPermittedByMouseButtonMask || _activationPermittedByKeyEvent;

	}

	if (activationPermitted || _draggerActive)
	{
		switch (ea.getEventType())
		{
		case osgGA::GUIEventAdapter::PUSH:
		{
			osgUtil::LineSegmentIntersector::Intersections intersections;

			_pointer.reset();

			if (view->computeIntersections(ea, intersections, _intersectionMask))
			{
				for (osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
				hitr != intersections.end();
					++hitr)
				{
					_pointer.addIntersection(hitr->nodePath, hitr->getLocalIntersectPoint());
				}
				//需要遍历全部的_hitList，不能只检测_hitList.front()，在实际测试中，拖拽器有时候不是存在于第一个intersection
				//如果只检测第一个intersection，会检测不到拖拽器被选中
				for (osgManipulator::PointerInfo::IntersectionList::iterator itrHit = _pointer._hitList.begin();
				itrHit != _pointer._hitList.end();
					++itrHit)
					for (osg::NodePath::iterator itr = (*itrHit).first.begin();
				itr != (*itrHit).first.end();
					++itr)
				{
					osgManipulator::Dragger* dragger = dynamic_cast<osgManipulator::Dragger*>(*itr);
					if (dragger)
					{
						if (dragger == this)
						{
							osg::Camera *rootCamera = view->getCamera();
							osg::NodePath nodePath = (*itrHit).first;//_pointer._hitList.front().first;
							osg::NodePath::reverse_iterator ritr;
							for (ritr = nodePath.rbegin();
							ritr != nodePath.rend();
								++ritr)
							{
								osg::Camera* camera = dynamic_cast<osg::Camera*>(*ritr);
								if (camera && (camera->getReferenceFrame() != osg::Transform::RELATIVE_RF || camera->getParents().empty()))
								{
									rootCamera = camera;
									break;
								}
							}

							_pointer.setCamera(rootCamera);
							_pointer.setMousePosition(ea.getX(), ea.getY());

							if (dragger->handle(_pointer, ea, aa))
							{
								dragger->setDraggerActive(true);
								handled = true;
							}
						}
					}
				}
			}
			break;
		}
		case osgGA::GUIEventAdapter::DRAG:
		case osgGA::GUIEventAdapter::RELEASE:
		{
			if (_draggerActive)
			{
				_pointer._hitIter = _pointer._hitList.begin();
				//                    _pointer.setCamera(view->getCamera());
				_pointer.setMousePosition(ea.getX(), ea.getY());

				if (handle(_pointer, ea, aa))
				{
					handled = true;
				}
			}
			break;
		}
		default:
			break;
		}

		if (_draggerActive && ea.getEventType() == osgGA::GUIEventAdapter::RELEASE)
		{
			setDraggerActive(false);
			_pointer.reset();
		}
	}

	return handled;
}

bool OneDDragger::handle(const PointerInfo& pointer, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	//return Translate1DDragger::handle(pi, ea, us);

	// Check if the dragger node is in the nodepath.
	if (_checkForNodeInNodePath)
	{
		//if (!pointer.contains(this)) return false;
		if (!contains(pointer,this)) return false;
	}

	switch (ea.getEventType())
	{
		// Pick start.
	case (osgGA::GUIEventAdapter::PUSH) :
	{
		// Get the LocalToWorld matrix for this node and set it for the projector.
		osg::NodePath nodePathToRoot;
		computeNodePathToRoot(*this, nodePathToRoot);
		osg::Matrix localToWorld = osg::computeLocalToWorld(nodePathToRoot);
		_projector->setLocalToWorld(localToWorld);

		if (_projector->project(pointer, _startProjectedPoint))
		{
			// Generate the motion command.
			osg::ref_ptr<TranslateInLineCommand> cmd = new TranslateInLineCommand(_projector->getLineStart(),
				_projector->getLineEnd());
			cmd->setStage(MotionCommand::START);
			cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(), _projector->getWorldToLocal());

			// Dispatch command.
			dispatch(*cmd);

			// Set color to pick color.
			setMaterialColor(_pickColor, *this);

			aa.requestRedraw();
		}
		return true;
	}

										// Pick move.
	case (osgGA::GUIEventAdapter::DRAG) :
	{
		osg::Vec3d projectedPoint;
		if (_projector->project(pointer, projectedPoint))
		{
			// Generate the motion command.
			osg::ref_ptr<TranslateInLineCommand> cmd = new TranslateInLineCommand(_projector->getLineStart(),
				_projector->getLineEnd());
			cmd->setStage(MotionCommand::MOVE);
			cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(), _projector->getWorldToLocal());
			cmd->setTranslation(projectedPoint - _startProjectedPoint);

			// Dispatch command.
			dispatch(*cmd);

			aa.requestRedraw();
		}
		return true;
	}

										// Pick finish.
	case (osgGA::GUIEventAdapter::RELEASE) :
	{
		osg::Vec3d projectedPoint;
		if (_projector->project(pointer, projectedPoint))
		{
			osg::ref_ptr<TranslateInLineCommand> cmd = new TranslateInLineCommand(_projector->getLineStart(),
				_projector->getLineEnd());

			cmd->setStage(MotionCommand::FINISH);
			cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(), _projector->getWorldToLocal());

			// Dispatch command.
			dispatch(*cmd);

			// Reset color.
			setMaterialColor(_color, *this);

			aa.requestRedraw();
		}

		return true;
	}
	default:
		return false;
	}
}

void OneDDragger::setupDefaultGeometry()
{
    // Get the line length and direction.
    osg::Vec3 lineDir = _projector->getLineEnd()-_projector->getLineStart();
    float lineLength = lineDir.length();
    lineDir.normalize();

    osg::Geode* geode = new osg::Geode;
    // Create a left cone.
    {
        osg::Cone* cone = new osg::Cone (_projector->getLineStart(), 0.025f * lineLength, 0.10f * lineLength);
        osg::Quat rotation;
        rotation.makeRotate(lineDir, osg::Vec3(0.0f, 0.0f, 1.0f));
        cone->setRotation(rotation);

        geode->addDrawable(new osg::ShapeDrawable(cone));
    }

    // Create a right cone.
    {
        osg::Cone* cone = new osg::Cone (_projector->getLineEnd(), 0.025f * lineLength, 0.10f * lineLength);
        osg::Quat rotation;
        rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), lineDir);
        cone->setRotation(rotation);

        geode->addDrawable(new osg::ShapeDrawable(cone));
    }

    // Create an invisible cylinder for picking the line.
    {
        osg::Cylinder* cylinder = new osg::Cylinder ((_projector->getLineStart()+_projector->getLineEnd())/2, 0.015f * lineLength, lineLength);
        osg::Quat rotation;
        rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), lineDir);
        cylinder->setRotation(rotation);
        osg::Drawable* cylinderGeom = new osg::ShapeDrawable(cylinder);

        //setDrawableToAlwaysCull(*cylinderGeom);

        geode->addDrawable(cylinderGeom);
    }

    osg::Geode* lineGeode = new osg::Geode;
    // Create a line.
    {
        osg::Geometry* geometry = new osg::Geometry();

        osg::Vec3Array* vertices = new osg::Vec3Array(2);
        (*vertices)[0] = _projector->getLineStart();
        (*vertices)[1] = _projector->getLineEnd();

        geometry->setVertexArray(vertices);
        geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES,0,2));

        lineGeode->addDrawable(geometry);
    }

    // Turn of lighting for line and set line width.
    lineGeode->getOrCreateStateSet()->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
    osg::LineWidth* linewidth = new osg::LineWidth();
    linewidth->setWidth(2.0f);
    lineGeode->getOrCreateStateSet()->setAttributeAndModes(linewidth, osg::StateAttribute::ON);

    // Add line and cones to the scene.
    addChild(lineGeode);
    addChild(geode);
}

XYZDragger::XYZDragger()
{
	_xDragger = new OneDDragger(osg::Vec3(0.0, 0.0, 0.0), osg::Vec3(0.0, 0.0, 1.0));
	addChild(_xDragger.get());
	addDragger(_xDragger.get());

	_yDragger = new OneDDragger(osg::Vec3(0.0, 0.0, 0.0), osg::Vec3(0.0, 0.0, 1.0));
	addChild(_yDragger.get());
	addDragger(_yDragger.get());

	_zDragger = new OneDDragger(osg::Vec3(0.0, 0.0, 0.0), osg::Vec3(0.0, 0.0, 1.0));
	addChild(_zDragger.get());
	addDragger(_zDragger.get());

	_axisLineWidth = 2.0f;
	_pickCylinderRadius = 0.015f;
	_coneHeight = 0.1f;

	setParentDragger(getParentDragger());
}

XYZDragger::~XYZDragger()
{
}

bool XYZDragger::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	if (ea.getHandled()) return false;

	osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
	if (!view) return false;

	bool handled = false;

	bool activationPermitted = true;
	if (_activationModKeyMask != 0 || _activationMouseButtonMask != 0 || _activationKeyEvent != 0)
	{
		_activationPermittedByModKeyMask = (_activationModKeyMask != 0) ?
			((ea.getModKeyMask() & _activationModKeyMask) != 0) :
			false;

		_activationPermittedByMouseButtonMask = (_activationMouseButtonMask != 0) ?
			((ea.getButtonMask() & _activationMouseButtonMask) != 0) :
			false;

		if (_activationKeyEvent != 0)
		{
			switch (ea.getEventType())
			{
			case osgGA::GUIEventAdapter::KEYDOWN:
			{
				if (ea.getKey() == _activationKeyEvent) _activationPermittedByKeyEvent = true;
				break;
			}
			case osgGA::GUIEventAdapter::KEYUP:
			{
				if (ea.getKey() == _activationKeyEvent) _activationPermittedByKeyEvent = false;
				break;
			}
			default:
				break;
			}
		}

		activationPermitted = _activationPermittedByModKeyMask || _activationPermittedByMouseButtonMask || _activationPermittedByKeyEvent;

	}

	if (activationPermitted || _draggerActive)
	{
		switch (ea.getEventType())
		{
		case osgGA::GUIEventAdapter::PUSH:
		{
			osgUtil::LineSegmentIntersector::Intersections intersections;

			_pointer.reset();

			if (view->computeIntersections(ea, intersections, _intersectionMask))
			{
				for (osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
				hitr != intersections.end();
					++hitr)
				{
					_pointer.addIntersection(hitr->nodePath, hitr->getLocalIntersectPoint());
				}
				//需要遍历全部的_hitList，不能只检测_hitList.front()，在实际测试中，拖拽器有时候不是存在于第一个intersection
				//如果只检测第一个intersection，会检测不到拖拽器被选中
				for (osgManipulator::PointerInfo::IntersectionList::iterator itrHit = _pointer._hitList.begin();
				itrHit != _pointer._hitList.end();
					++itrHit)
					for (osg::NodePath::iterator itr = (*itrHit).first.begin();
				itr != (*itrHit).first.end();
					++itr)
				{
					osgManipulator::Dragger* dragger = dynamic_cast<osgManipulator::Dragger*>(*itr);
					if (dragger)
					{
						if (dragger == this)
						{
							osg::Camera *rootCamera = view->getCamera();
							osg::NodePath nodePath = (*itrHit).first;// _pointer._hitList.front().first;
							osg::NodePath::reverse_iterator ritr;
							for (ritr = nodePath.rbegin();
							ritr != nodePath.rend();
								++ritr)
							{
								osg::Camera* camera = dynamic_cast<osg::Camera*>(*ritr);
								if (camera && (camera->getReferenceFrame() != osg::Transform::RELATIVE_RF || camera->getParents().empty()))
								{
									rootCamera = camera;
									break;
								}
							}

							_pointer.setCamera(rootCamera);
							_pointer.setMousePosition(ea.getX(), ea.getY());

							if (dragger->handle(_pointer, ea, aa))
							{
								dragger->setDraggerActive(true);
								handled = true;
							}
						}
					}
				}
			}
			break;
		}
		case osgGA::GUIEventAdapter::DRAG:
		case osgGA::GUIEventAdapter::RELEASE:
		{
			if (_draggerActive)
			{
				_pointer._hitIter = _pointer._hitList.begin();
				//                    _pointer.setCamera(view->getCamera());
				_pointer.setMousePosition(ea.getX(), ea.getY());

				if (handle(_pointer, ea, aa))
				{
					handled = true;
				}
			}
			break;
		}
		default:
			break;
		}

		if (_draggerActive && ea.getEventType() == osgGA::GUIEventAdapter::RELEASE)
		{
			setDraggerActive(false);
			_pointer.reset();
		}
	}

	return handled;
}

bool XYZDragger::handle(const PointerInfo& pi, const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	// Check if the dragger node is in the nodepath.
	//if (!pi.contains(this)) return false;
	if (!contains(pi, this)) return false;

	for (DraggerList::iterator itr = _draggerList.begin(); itr != _draggerList.end(); ++itr)
	{
		if ((*itr)->handle(pi, ea, aa))
			return true;
	}
	return false;
}

void XYZDragger::setupDefaultGeometry()
{
	// 添加一个shape作为坐标轴原点
	osg::Geode* originGeode = new osg::Geode;
#if !defined(OSG_GLES2_AVAILABLE)
	originGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
#endif
	osg::ref_ptr<osg::ShapeDrawable> shape = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(), 0.05));
	shape->setColor(osg::Vec4(1, 1, 0, 1));
	originGeode->addDrawable(shape);
	originGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	addChild(originGeode);

	osg::Geode* geode = new osg::Geode;
	// 轴线
	{
		_lineCylinder = new osg::Cylinder(osg::Vec3(0.0f, 0.0f, 0.5f), _pickCylinderRadius, 1.0f);
		osg::ShapeDrawable* cylinderDrawable = new osg::ShapeDrawable(_lineCylinder.get());
		geode->addDrawable(cylinderDrawable);
	}

	// Create a cone.
	{
		_cone = new osg::Cone(osg::Vec3(0.0f, 0.0f, 1.0f), _coneHeight * 0.25f, _coneHeight);
		osg::ShapeDrawable* coneDrawable = new osg::ShapeDrawable(_cone.get());
		// coneDrawable->setColor(osg::Vec4(0.0f,0.0f,1.0f,1.0f));
		geode->addDrawable(coneDrawable);

		// This ensures correct lighting for scaled draggers.
#if !defined(OSG_GLES2_AVAILABLE)
		geode->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
#endif
	}

	// Create an invisible cylinder for picking the line.
	{
		_cylinder = new osg::Cylinder(osg::Vec3(0.0f, 0.0f, 0.5f), _pickCylinderRadius, 1.0f);
		osg::Drawable* geometry = new osg::ShapeDrawable(_cylinder.get());
		setDrawableToAlwaysCull(*geometry);
		geode->addDrawable(geometry);
	}

	// Add geode to all 1D draggers.
	_xDragger->addChild(geode);
	_yDragger->addChild(geode);
	_zDragger->addChild(geode);

	// Rotate X-axis dragger appropriately.
	{
		osg::Quat rotation; rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), osg::Vec3(1.0f, 0.0f, 0.0f));
		_xDragger->setMatrix(osg::Matrix(rotation));
	}

	// Rotate Y-axis dragger appropriately.
	{
		osg::Quat rotation; rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), osg::Vec3(0.0f, 1.0f, 0.0f));
		_yDragger->setMatrix(osg::Matrix(rotation));
	}

	// Send different colors for each dragger.
	_xDragger->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	_yDragger->setColor(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
	_zDragger->setColor(osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f));
}

void XYZDragger::setAxisLineWidth(float linePixelWidth)
{
	_axisLineWidth = linePixelWidth;
	if (_lineWidth.valid())
		_lineWidth->setWidth(linePixelWidth);
}
void XYZDragger::setPickCylinderRadius(float pickCylinderRadius)
{
	_pickCylinderRadius = pickCylinderRadius;
	if (_cylinder.valid())
		_cylinder->setRadius(pickCylinderRadius);
}

void XYZDragger::setConeHeight(float height)
{
	_coneHeight = height;
	if (_cone.valid())
	{
		_cone->setRadius(0.25f * height);
		_cone->setHeight(height);
	}
}
