echo osgversion
osgversion
more memleaks.log

echo sgv osgcool.osg
sgv osgcool.osg
more memleaks.log

echo sgv cow.osg
sgv cow.osg
more memleaks.log

echo sgv -stereo cessna.osg
sgv -stereo cessna.osg
more memleaks.log

echo sgv cessnafire.osg
sgv cessnafire.osg
more memleaks.log

echo sgv spaceship.osg
sgv spaceship.osg
more memleaks.log

echo sgv cube_mapped_torus.osg
sgv cube_mapped_torus.osg
more memleaks.log

echo sgv Town.osg
sgv Town.osg
more memleaks.log

echo osghangglide
osghangglide
more memleaks.log

echo osggeometry 
osggeometry
more memleaks.log

echo osgprerender dumptruck.osg
osgprerender dumptruck.osg
more memleaks.log

echo osgparticle
osgparticle
more memleaks.log

echo osgbillboard lz.rgb
osgbillboard lz.rgb
more memleaks.log

echo osgcube
osgcube
more memleaks.log

echo osgclip
osgclip cow.osg
more memleaks.log


echo osghud dumptruck.osg
osghud dumptruck.osg
more memleaks.log

echo osglight glider.osg
osglight glider.osg
more memleaks.log

echo osgimpostor Town.osg
osgimpostor Town.osg
more memleaks.log

echo osgmultitexture cessnafire.osg
osmultitexture cessnafire.osg
more memleaks.log

echo osgreflect cessna.osg
osgreflect cessna.osg
more memleaks.log

echo osgscribe cow.osg
osgscribe cow.osg
more memleaks.log

echo osgstereoimage dog_left_eye.jpg dog_right_eye.jpg
osgstereoimage dog_left_eye.jpg dog_right_eye.jpg
more memleaks.log

echo osgtext
osgtext
more memleaks.log

echo osgtexture reflect.rgb lz.rgb
osgtexture reflect.rgb lz.rgb
more memleaks.log

echo osgviews cow.osg
osgviews cow.osg
more memleaks.log

echo osgoccluder glider.osg
osgoccluder glider.osg
more memleaks.log
