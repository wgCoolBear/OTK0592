#include "AutoScaleGroup.h"
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/Point>
#include <osg/LineWidth>
#include <osg/Shape>
#include <osg/ShapeDrawable>
#include "osgText/Text"
#include "osg/AutoTransform"
#include "osg/PositionAttitudeTransform"
#include "osgDB/ReadFile"
#include "osgUtil/CullVisitor"

AutoScaleGroup::AutoScaleGroup(osg::MatrixTransform* mt, float size/*=240.0f*/):
	_mt(mt),
	_size(size),
	_active(true)
{
	
}

void AutoScaleGroup::traverse(osg::NodeVisitor& nv)
{
	if (_active && nv.getVisitorType() == osg::NodeVisitor::CULL_VISITOR)
	{
		osgUtil::CullVisitor* cv = static_cast<osgUtil::CullVisitor*>(&nv);
		//这个0.48f是个魔数
		float pixelSize = cv->pixelSize(_mt->getBound().center(), 0.48f);
		if (pixelSize != _size)
		{
			float pixelScale = pixelSize > 0.0f ? _size / pixelSize : 1.0f;
			osg::Vec3d scaleFactor(pixelScale, pixelScale, pixelScale);

			osg::Vec3 trans = _mt->getMatrix().getTrans();
			_mt->setMatrix(osg::Matrix::scale(scaleFactor) * osg::Matrix::translate(trans));
		}
	}
	osg::Group::traverse(nv);
}

void AutoScaleGroup::setSize(float size)
{
	_size = size;
}
