//C++ header - Open Scene Graph - Copyright (C) 1998-2002 Robert Osfield
//Distributed under the terms of the GNU Library General Public License (LGPL)
//as published by the Free Software Foundation.

#include <osg/Matrix>
#include <osg/Drawable>
#include <osg/StateSet>
#include <osg/State>
#include <osg/Light>

#include <osgUtil/RenderLeaf>

#include <set>
#include <vector>
#include <algorithm>

class RenderGraph : public osg::Referenced
{
    public:
    

        typedef std::map< const osg::StateSet*, osg::ref_ptr<RenderGraph> >   ChildList;
        typedef std::vector< osg::ref_ptr<RenderLeaf> >                 LeafList;

        RenderGraph*                        _parent;
        osg::ref_ptr<const osg::StateSet>   _stateset;

        int                                 _depth;
        ChildList                           _children;
        LeafList                            _leaves;
        
        mutable float                       _averageDistance;
        
        osg::ref_ptr<osg::Referenced>       _userData;


        RenderGraph():
            _parent(NULL), 
            _stateset(NULL), 
            _depth(0),
            _averageDistance(0)
        {
        }

        RenderGraph(RenderGraph* parent,const osg::StateSet* stateset):
            _parent(parent), 
            _stateset(stateset)
        {
            if (_parent) _depth = _parent->_depth + 1;
            else _depth = 0;
        }
            
        ~RenderGraph() {}
        
        RenderGraph* cloneType() const { return new RenderGraph; }
        
        void setUserData(osg::Referenced* obj) { _userData = obj; }
        osg::Referenced* getUserData() { return _userData.get(); }
        const osg::Referenced* getUserData() const { return _userData.get(); }
        

        /** return true if all of drawables, lights and children are empty.*/
        inline const bool empty() const
        {
            return _leaves.empty() && _children.empty();
        }
        
        
        
    private:

        /// disallow copy construction.
        RenderGraph(const RenderGraph&):osg::Referenced() {}
        /// disallow copy operator.
        RenderGraph& operator = (const RenderGraph&) { return *this; }

};

