//C++ header - Open Scene Graph - Copyright (C) 1998-2002 Robert Osfield
//Distributed under the terms of the GNU Library General Public License (LGPL)
//as published by the Free Software Foundation.

#ifndef OSG_GEODE
#define OSG_GEODE 1

#include <osg/Node>
#include <osg/NodeVisitor>
#include <osg/Drawable>

namespace osg {

/** Leaf Node for grouping Drawables.*/
class SG_EXPORT Geode : public Node
{
    public:

        typedef std::vector< ref_ptr<Drawable> > DrawableList;

        Geode();

        /** Copy constructor using CopyOp to manage deep vs shallow copy.*/
        Geode(const Geode&,const CopyOp& copyop=CopyOp::SHALLOW_COPY);

        META_Node(osg, Geode);

        /** Add Drawable to Geode.
         *  If gset is not NULL and is not contained in Geode then increment its  
         *  reference count, add it to the drawables list and dirty the bounding 
         *  sphere to force it to recompute on next getBound() and return true for success.
         *  Otherwise return false.
         */
        virtual const bool addDrawable( Drawable *drawable );

        /** Remove Drawable from Geode.
         *  If gset is contained in Geode then remove it from the geoset
         *  list and decrement its reference count, and dirty the 
         *  bounding sphere to force it to recompute on next getBound() and
         *  return true for success.  If gset is not found then return false
         *  and do not change the reference count of gset.
         */
        virtual const bool removeDrawable( Drawable *drawable );

        /** Replace specified Drawable with another Drawable.
         *  Decrement the reference count origGSet and increments the
         *  reference count of newGset, and dirty the bounding sphere
         *  to force it to recompute on next getBound() and returns true.
         *  If origDrawable is not found then return false and do not 
         *  add newGset.  If newGset is NULL then return false and do
         *  not remove origGset.
         */
        virtual const bool replaceDrawable( Drawable *origDraw, Drawable *newDraw );


        /** return the number of geoset's.*/
        inline const unsigned int getNumDrawables() const { return _drawables.size(); }

        /** return geoset at position i.*/
        inline Drawable* getDrawable( const unsigned int i ) { return _drawables[i].get(); }

        /** return geoset at position i.*/
        inline const Drawable* getDrawable( const unsigned int i ) const { return _drawables[i].get(); }

        /** return true if geoset is contained within Geode.*/
        inline const bool containsDrawable(const Drawable* gset) const
        {
            
            for (DrawableList::const_iterator itr=_drawables.begin();
                 itr!=_drawables.end();
                 ++itr)
            {
                if (itr->get()==gset) return true;
            }
            return false;
        }

        /** return the iterator position for specified Drawable.
         *  return _geoset.end() if gset not is contained in Geode.
         */
        inline DrawableList::iterator findDrawable(const Drawable* gset)
        {
            
            for (DrawableList::iterator itr=_drawables.begin();
                 itr!=_drawables.end();
                 ++itr)
            {
                if (itr->get()==gset) return itr;
            }
            return _drawables.end();
        }

        /** return the const_iterator position for specified Drawable.
         *  return _geoset.end() if gset not is contained in Geode.
         */
        inline DrawableList::const_iterator findDrawable(const Drawable* gset) const
        {
            
            for (DrawableList::const_iterator itr=_drawables.begin();
                 itr!=_drawables.end();
                 ++itr)
            {
                if (itr->get()==gset) return itr;
            }
            return _drawables.end();
        }

        /** compile OpenGL Display List for each geoset.*/
        void compileDrawables(State& state);
        
    protected:

        virtual ~Geode();

        virtual const bool computeBound() const;

        DrawableList                    _drawables;

};

}

#endif
