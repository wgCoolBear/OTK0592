#include <osgParticle/Version>

const char* osgParticleGetVersion()
{
    return "0.9.0";
}


const char* osgParticleGetLibraryName()
{
    return "Open Scene Graph Particle Library";
}
