#include "SceneWidget.h"
#include "osgViewer\ViewerEventHandlers"
#include "osgGA\TrackballManipulator"
#include "osg\ShapeDrawable"
#include "osgGA\StateSetManipulator"

SceneWidget::SceneWidget(QWidget* parent /*= 0*/, Qt::WindowFlags f /*= 0*/) 
	: BaseSceneWidget(parent, f)
{
	_root->addChild(osgDB::readNodeFile("../../../data/model/axes.osgb"));
	_viewer->home();
}