#include"Arrangement_Combination.h"
#include <iostream>

void Arrangement(std::vector<int> A)
{
	//std::vector<int> A{ 1,2,3,4,5 };
	while (next_permutation(A.begin(), A.end()))
	{
		//std::cout << A[0] << A[1] << A[2] << A[3] << A[4] << std::endl;
	}
}


void combine_inner(const std::vector<int> &data, int start, int n, int m, int depth, 
	std::vector<int> temp, std::vector<std::vector<int> > &result)
{
	if (depth == m - 1)
	{
		//最内层循环将temp加入result
		for (int i = start; i < n - (m - depth - 1); ++i)
		{
			temp[depth] = data[i];
			result.push_back(temp);
		}
	}
	else
		for (int i = start; i < n - (m - depth - 1); ++i)
		{
			temp[depth] = data[i];//每层输出一个元素
			combine_inner(data, i + 1, n, m, depth + 1, temp, result);
		}
}

// 从输入数组in中，输出m个元素为一组的组合，按照顺序放到out
void Combination(const std::vector<int> &in, std::vector<int> &out, int m)
{
	std::vector<std::vector<int> > result;
	std::vector<int> temp(m, 0);
	combine_inner(in, 0, in.size(), m, 0, temp, result);
	for (size_t i=0;i<result.size();i++)
	{
		for (size_t j=0;j<result[i].size();j++)
		{
			out.push_back(result[i][j]);
		}
	}
}
