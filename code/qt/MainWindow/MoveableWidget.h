#pragma once
#include <QWidget>
#include <QMouseEvent>

//���ƶ��Ĵ���
class MoveableWidget : public QWidget
{
public:
	MoveableWidget(QWidget* parent = NULL);
protected:
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;

	QWidget* _parentWidget;
	bool _dragFlag;
	QPoint _dragStartPos;
};