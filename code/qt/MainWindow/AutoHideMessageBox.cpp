#include "AutoHideMessageBox.h"
#include <QPropertyAnimation>


AutoHideMessageBox::AutoHideMessageBox(QWidget* parent, const QString &title, const QString &text, bool autoHide/* = true*/, int showTime/* = 2000*/, QDialogButtonBox::StandardButtons buttons/* = QDialogButtonBox::Ok*/)
	:QDialog(parent, Qt::FramelessWindowHint)
{
	setupUi(this);
	setAttribute(Qt::WA_TranslucentBackground);
	label_title->setText(title);
	label_content->setText(text);
	if (autoHide)
	{
		//geometry属性可以做组件大小变形动画
		//QPropertyAnimation* animation = new QPropertyAnimation(this, "geometry");
		QPropertyAnimation* animation = new QPropertyAnimation(this, "windowOpacity");
		animation->setDuration(showTime);
		animation->setStartValue(1);
		animation->setEndValue(0);
		animation->start();
		connect(animation, SIGNAL(finished()), this, SLOT(close()));
		//自动隐藏不需要关闭按钮和确定按钮
		buttonBox->hide();
		pushButton_exit->hide();
	}
	else
	{
		buttonBox->setStandardButtons(buttons);
		buttonBox->button(QDialogButtonBox::Ok)->setText(QStringLiteral("确定"));
		connect(pushButton_exit, SIGNAL(clicked()), this, SLOT(hide()));
	}
}


AutoHideMessageBox::~AutoHideMessageBox()
{
}
