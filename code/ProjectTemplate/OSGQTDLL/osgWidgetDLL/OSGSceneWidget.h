#pragma once
#include "BaseSceneWidget.h"

class DLL_EXPORT OSGSceneWidget : public BaseSceneWidget
{
public:
	OSGSceneWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
};