echo osgversion
osgversion

echo sgv osgcool.osg
sgv osgcool.osg

echo sgv cow.osg
sgv cow.osg

echo sgv -stereo cessna.osg
sgv -stereo cessna.osg

echo sgv cessnafire.osg
sgv cessnafire.osg

echo sgv spaceship.osg
sgv spaceship.osg

echo sgv cube_mapped_torus.osg
sgv cube_mapped_torus.osg

echo sgv Town.osg
sgv Town.osg

echo osghangglide
osghangglide

echo osggeometry 
osggeometry

echo osgprerender dumptruck.osg
osgprerender dumptruck.osg

echo osgparticle
osgparticle

echo osgbillboard lz.rgb
osgbillboard lz.rgb

echo osgcube
osgcube

echo osgclip
osgclip cow.osg

echo osghud dumptruck.osg
osghud dumptruck.osg

echo osglight glider.osg
osglight glider.osg

echo osgimpostor Town.osg
osgimpostor Town.osg

echo osgmultitexture cessnafire.osg
osgmultitexture cessnafire.osg

echo osgreflect cessna.osg
osgreflect cessna.osg

echo osgscribe cow.osg
osgscribe cow.osg

echo osgstereoimage dog_left_eye.jpg dog_right_eye.jpg
osgstereoimage dog_left_eye.jpg dog_right_eye.jpg

echo osgtext
osgtext

echo osgtexture reflect.rgb lz.rgb
osgtexture reflect.rgb lz.rgb

echo osgviews cow.osg
osgviews cow.osg

echo osgoccluder glider.osg
osgoccluder glider.osg
