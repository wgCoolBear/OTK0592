#include "TxtReaderWriter.h"

void readTxtFile(const char * txtFilePath)
{
	std::ifstream fs;
	fs.open(txtFilePath);//打开文件	
	if (!fs.is_open())
	{
		std::cout << "can not open txt file!" << std::endl;
		return;
	}
	std::string line;
	while (getline(fs, line))//循环读取每一行
	{
		std::cout << line << std::endl;
	}
	fs.close();
}

void writeTxtFile(const char * txtFilePath)
{
	std::ofstream outfile(txtFilePath);
	outfile.setf(std::ios::fixed);
	outfile.precision(6);
	outfile << 120.0 << " " << 2500.0 << " " << 120.0 << std::endl;
	outfile.close();
}
