#pragma once
#include "Export.h"
#include <osgViewer/Viewer>
#include <osgGA/GUIEventHandler>
#include <osg/io_utils>
#include <iostream>
/************************************************************************/
/* osgEarth中可参照MouseCoordsTool中的MouseCoordsTool::handle
/* _mapNode->getTerrain()->getWorldCoordsUnderMouse
/************************************************************************/
class DLL_EXPORT MousePick :public osgGA::GUIEventHandler
{
public:
	MousePick() {}

	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
	//鼠标拾取。使用view->computeIntersections获取拾取信息
	void pick_method1(osgViewer::View* view, const osgGA::GUIEventAdapter& ea);
	//使用osgUtil::IntersectionVisitor获取拾取信息，可以指定求交器以及坐标系
	void pick_method2(osgViewer::Viewer* viewer, const osgGA::GUIEventAdapter& ea);
};

