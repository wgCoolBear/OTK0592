#pragma once
#include<vector>
#include<algorithm>
/************************************************************************/
/*
//A(n,m)＝n(n-1)(n-2)....(n-m+1)＝n!/(n-m)!
//C(n,m)=A(n,m)/A(m,m)=A(n,m)/m!
std::vector<int> A{ 1,2,3,4,5 };
//Arrangement(A);
std::vector<int> B;
Combination(A, B, 3);*/
/************************************************************************/
// 排列
void Arrangement(std::vector<int> in);
// 从输入数组in中，输出m个元素为一组的组合，按照顺序放到out
void Combination(const std::vector<int> &in, std::vector<int> &out, int m);