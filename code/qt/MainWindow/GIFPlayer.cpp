#include "GIFPlayer.h"
#include <QBitMap>

GIFPlayer::GIFPlayer(QWidget* parent/*=NULL*/)
	:QLabel(parent)
{

}

GIFPlayer::GIFPlayer(QString gifPath, QWidget* parent, bool play /*= false*/)
	:QLabel(parent, Qt::FramelessWindowHint|Qt::Tool)
{
	init(gifPath, play);
	_width = _movie->currentImage().width();
	_height = _movie->currentImage().height();
	setGeom(0,0,_width,_height);
}

GIFPlayer::GIFPlayer(QString gifPath, QWidget* parent, QSize targetSize, bool play /*= false*/)
{
	init(gifPath, play);
	_movie->setScaledSize(targetSize);
	_width = targetSize.width();
	_height = targetSize.height();
	setGeom(0,0,_width,_height);
}

void GIFPlayer::init(QString gifPath, bool play)
{
	//检测APNG格式的gif，支持apng需要编译qapng.dll插件，放在qt安装目录的plugins\imageformats目录下
	if (gifPath.endsWith(".png", Qt::CaseInsensitive))
		_movie = new QMovie(gifPath, "apng");
	else
		_movie = new QMovie(gifPath);
	setMovie(_movie);
	_movie->jumpToNextFrame();
	setAttribute(Qt::WA_TranslucentBackground);
	//屏蔽鼠标事件，即使gif在按钮上方也不是影像按钮点击
	setAttribute(Qt::WA_TransparentForMouseEvents, true);
	hide();
	connect(_movie, SIGNAL(finished()),this,SLOT(onFinished()));
	if(play)
	{
		this->play();
	}
}

void GIFPlayer::play()
{
	if (_movie)
	{
		show();
		if (_movie->state()==QMovie::Running)
		{
			_movie->stop();
		}
		_movie->start();
	}
}

void GIFPlayer::stop()
{
	if (_movie)
	{
		hide();
		_movie->stop();
	}
}

void GIFPlayer::setPos(int x, int y)
{
	move(x - _width / 2, y - _height / 2);
}

void GIFPlayer::setGeom(QRect rect)
{
	if (_width!=rect.width()||_height!=rect.height())
	{
		_movie->setScaledSize(rect.size());
		_width = rect.width();
		_height = rect.height();
	}
	setGeometry(rect);
}

void GIFPlayer::setGeom(int x, int y, int width, int height)
{
	if (_width != width || _height != height)
	{
		_movie->setScaledSize(QSize(width, height));
		_width = width;
		_height = height;
	}
	setGeometry(x, y, width, height);
}

void GIFPlayer::onFinished()
{
	hide();
}
