#include <QApplication>
#include <QFile>
#include <QTextStream>
#include <QSettings>
#include <QSurfaceFormat>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
	/*QSurfaceFormat format = QSurfaceFormat::defaultFormat();

#ifdef OSG_GL3_AVAILABLE
	format.setVersion(3, 2);
	format.setProfile(QSurfaceFormat::CoreProfile);
	format.setRenderableType(QSurfaceFormat::OpenGL);
	format.setOption(QSurfaceFormat::DebugContext);
#else
	format.setVersion(2, 0);
	format.setProfile(QSurfaceFormat::CompatibilityProfile);
	format.setRenderableType(QSurfaceFormat::OpenGL);
	format.setOption(QSurfaceFormat::DebugContext);
#endif
	format.setDepthBufferSize(24);
	//format.setAlphaBufferSize(8);
	format.setSamples(8);
	format.setStencilBufferSize(8);
	format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
	QSurfaceFormat::setDefaultFormat(format);*/

    QApplication a(argc, argv);

    MainWindow win;
	//win.setGeometry(100, 100, 1024, 800);
	win.showMaximized();
    //win.show();

    return a.exec();
}
